from setuptools import setup, find_packages
from os.path import join, dirname
import cyborg

setup(
    name='cyborg',
    version=cyborg.__version__,
    packages=find_packages(),
    long_description=open(join(dirname(__file__), 'README.md')).read(),
    install_requires=[
        'tornado==4.4.2',
        'PyYAML==3.12',
	'orderbookd>=0.1.0',
    ]
)
