from collections import defaultdict
import logging
from threading import Lock

from orderbookd.util import int2float
import orderbookd.db as db

_logger = logging.getLogger('arbitrage')

class arbitrage_t(object):
    def __init__(self, chain):
        self.ts_start = None
        self.ts_end = None

        self.chain = chain

        self.rate_start = None
        self.amount_start = None

        self.is_open = False

    def copy(self):
        dup = arbitrage_t(self.chain)
        dup.ts_start = self.ts_start
        dup.ts_end = self.ts_end
        dup.rate_start = self.rate_start
        dup.amount_start = self.amount_start
        dup.is_open = self.is_open
        return dup

    def __str__(self):
        return "chain '{}': is_open = {}, ts_start = {}, "\
               "ts_end = {}, rate_start = {}, amount_start = {}".format( '->'.join(self.chain),
                                                                         self.is_open,
                                                                         self.ts_start,
                                                                         self.ts_end,
                                                                         self.rate_start,
                                                                         self.amount_start,
                                                                       )


def _read_chains(filename, start_with='*', max_exchanges=0):
    """
    Читает цепочки обмена из файла формата:
    ```
    BTC->LSK->ETH->BTC
    USDT->XMR->DASH->BTC->USDT
    ...
    ```
    """
    chains = set()
    with open(filename, 'r') as fin:
        for line in fin:
            line = line.strip()
            if not line:
                continue
            chain = tuple(line.split('->'))
            if start_with != '*' and chain[0] != start_with:
                continue
            if max_exchanges > 0 and len(chain) - 1 > max_exchanges:
                continue
            chains.add(tuple(line.split('->')))
    return chains


class arbitrage_processor_t(object):
    def __init__(self, config, prices_holder):
        self.config = config
        self.prices_holder = prices_holder
        self.lock = Lock()
        self.conn = None
        self.fee = config['arbitrage']['fee']
        self.log_to_db = config['arbitrage']['log_to_db']
        self.min_rate = config['arbitrage']['min_rate']
        self.precision = config['precision']
        self.pairs = set(config['pairs'])
        self.pair2chains = defaultdict(list)
        self.chain2pairs = defaultdict(list)
        self.chains = _read_chains( config['arbitrage']['chains_file'],
                                    config['arbitrage']['start_with'],
                                    config['arbitrage']['max_exchanges'],
                                  )
        self.arbitrages = {}
        for chain in self.chains:
            if len(chain) < 3:
                raise ValueError("Wrong arbitrage chain '{}'".format('->'.join(chain)))
            pairs_tmp = []
            for i in range(1, len(chain)):
                pair1, pair2 = '{}_{}'.format(chain[i - 1], chain[i]), '{}_{}'.format(chain[i], chain[i - 1])
                found = 0
                if pair1 in self.pairs:
                    pairs_tmp.append(pair1)
                    found += 1

                if pair2 in self.pairs:
                    pairs_tmp.append(pair2)
                    found += 1

                if found > 1:
                    raise ValueError("There are equivalent pairs: '{}' and '{}'".format(pair1, pair2))
                if found == 0:
                    # нет пары для цепочки
                    pairs_tmp = []
                    _logger.warning("Can't process chain '{}' because there are no information about pair '{}' or '{}'".format('->'.join(chain), pair1, pair2))
                    break
            for pair in pairs_tmp:
                self.pair2chains[pair].append(chain)
                self.chain2pairs[chain].append(pair)

            self.arbitrages[chain] = arbitrage_t(chain)
        _logger.info('Process {} chains'.format(len(self.chain2pairs)))

    def check_arbitrages(self, pair):
        found_arbitrages = []
        with self.lock:
            chains = self.pair2chains[pair]
            needed_pairs = set()
            for chain in chains:
                needed_pairs.update(self.chain2pairs[chain])

            # собираем данные по интересующим парам
            storage = {}
            for pair in needed_pairs:
                if pair not in self.prices_holder.keys():
                    continue
                with self.prices_holder.get(pair) as p:
                    price_dict = dict( ts=p.ts,
                                       seq=p.seq,
                                       ask_rate=int2float(p.ask_rate, precision_decode=self.precision),
                                       ask_amount=int2float(p.ask_amount, precision_decode=self.precision),
                                       bid_rate=int2float(p.bid_rate, precision_decode=self.precision),
                                       bid_amount=int2float(p.bid_amount, precision_decode=self.precision),
                                     )
                    storage[pair] = price_dict

            for chain in chains:
                if chain not in self.chain2pairs:
                    continue
                rate, amount, ts, success = arbitrage_processor_t.calc_profit(chain, storage, fee_func=lambda a: self.sub_fee(a))
                if not success:
                    # _logger.warning("Can't calc profit for chain '{}'".format('->'.join(chain)))
                    continue
                # _logger.debug("calc_profit '{}' (rate = {}, amount = {})".format('->'.join(chain), rate, amount))
                arbitrage = self.arbitrages[chain]
                if arbitrage.is_open and rate <= 1.0:
                    # закрываем арбитраж
                    arbitrage.is_open = False
                    arbitrage.ts_end = ts
                    _logger.debug("Close arbitrage '{}' (rate = {}, amount = {}, "
                                  "duration = {:.2f})".format( '->'.join(chain),
                                                               arbitrage.rate_start,
                                                               arbitrage.amount_start,
                                                               arbitrage.ts_end - arbitrage.ts_start,
                                                             ))
                    if self.log_to_db:
                        conn = self.get_db_connection()
                        db.insert_arbitrage(conn, arbitrage)
                elif not arbitrage.is_open and rate > self.min_rate:
                    # открываем арбитраж
                    arbitrage.is_open = True
                    arbitrage.ts_start = ts
                    arbitrage.ts_end = False
                    arbitrage.rate_start = rate
                    arbitrage.amount_start = amount
                    _logger.debug('Open arbitrage: {}'.format(arbitrage))
                    found_arbitrages.append(arbitrage.copy())
        return found_arbitrages

    def sub_fee(self, amount, count=1):
        return amount * (1 - self.fee) ** count

    @staticmethod
    def calc_profit(circle, storage, fee_func):
        rate = 1
        # не спортивно.. но пока так. Речь о том, что мы якобы имеем неограниченное кол-во валюты
        # для обмена
        amount = 10**20
        ts = 0
        for i in range(len(circle) - 1):
            cur_val = circle[i]
            next_val = circle[i + 1]

            pair = cur_val + '_' + next_val
            reverse_pair = next_val + '_' + cur_val

            if pair in storage:
                if storage[pair]['ask_amount'] == 0:
                    # нет информации о лучшей цене пары
                    return 0, 0, 0, False
                rate = fee_func(rate / storage[pair]['ask_rate'])
                amount = fee_func(min(amount / storage[pair]['ask_rate'], storage[pair]['ask_amount']))
                ts = max(ts, storage[pair]['ts'])
            elif reverse_pair in storage:
                if storage[reverse_pair]['bid_amount'] == 0:
                    # нет информации о лучшей цене пары
                    return 0, 0, 0, False
                rate = fee_func(rate * storage[reverse_pair]['bid_rate'])
                amount = fee_func(min(amount * storage[reverse_pair]['bid_rate'], storage[reverse_pair]['bid_rate'] * storage[reverse_pair]['bid_amount']))
                ts = max(ts, storage[reverse_pair]['ts'])
            else:
                return 0, 0, 0, False
        return rate, amount, ts, True

    def get_db_connection(self):
        if self.conn is None:
            self.conn = db.get_connection(self.config['db'])
            self.conn.autocommit = True
            return self.conn

        try:
            cur = self.conn.cursor()
            return self.conn
        except db.OperationalError as e:
            _logger.warning('Got db.OperationalError: {}'.format(str(e)))
            _logger.info('Trying to reconnect')
            self.conn = db.get_connection(self.config['db'])
            self.conn.autocommit = True
            return self.conn
