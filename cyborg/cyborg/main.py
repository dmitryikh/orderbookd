import argparse
import logging
from threading import Thread
import yaml
import websocket
import time
import json
import math
import queue


from .poloniex import poloniex_trader_t, limits_t
from .balance import balance_t
from .arbitrage import arbitrage_processor_t
from orderbookd.util import concurrent_map_t, strnum2int, int2strnum
from orderbookd.price import price_inc_t, price_t

logging.basicConfig( format="%(asctime)s - %(name)s - %(levelname)s - %(message)s",
                     level=logging.DEBUG
                   )

logger = logging.getLogger('main')


def parse_args():
    parser = argparse.ArgumentParser(description='auto trade bot for poloniex',
                                     formatter_class=argparse.ArgumentDefaultsHelpFormatter)
    parser.add_argument('-c', '--config', type=str, required=True,
                        help='config file')
    return parser.parse_args()


class chain_executor_t(Thread):
    """
    Принимает по очереди(каналу) арбитражи и реализовывает их на бирже
    """
    def __init__(self, prices_holder, trader, config):
        super(chain_executor_t, self).__init__()
        self.queue = queue.LifoQueue()
        self.prices_holder = prices_holder
        self.trader = trader
        self.config = config
        self.precision = config['precision']
        self.logger = logging.getLogger('cyborg.chain_executor')

    def clear_queue(self):
        n_dropped = 0
        while not self.queue.empty():
            try:
                self.queue.get(False)
                n_dropped += 1
            except Empty:
                continue
        if n_dropped:
            self.logger.info('Drop {} old arbitrages'.format(n_dropped))

    def stop(self):
        self.stop_flag = True

    def run(self):
        self.logger.info('Start')
        self.stop_flag = False
        while not self.stop_flag:
            # Так как у нас Lifo очередь, то мы берем самый свежий арбитраж, остальные в очереди
            # выкидываем
            try:
                arbitrage = self.queue.get(block=True, timeout=0.1)
                self.logger.info('Starting execution: {}'.format(arbitrage))
                self.clear_queue()
                self.execute_chain(arbitrage)
                self.logger.info('Finish execution: {}'.format(arbitrage))
                self.trader.refresh_balance()
                self.logger.info('{}'.format(self.trader.balance))
            except queue.Empty:
                pass
            except Exception:
                self.logger.exception('')
                pass
        self.logger.info('Stop')

    def execute_chain(self, arbitrage):
        chain = arbitrage.chain
        for i in range(len(chain) - 1):
            cur_val = chain[i]
            next_val = chain[i + 1]
            if i == 0:
                amount_coef = min(0.2, arbitrage.amount_start / self.trader.amount(cur_val) * 10**self.precision)  # TODO: 0.2
            else:
                amount_coef = 1.0
            ret = self.execute_chain_step([cur_val, next_val], amount_coef, retry=(i != 0))
            if not ret:
                self.logger.error("Can't execute chain '{}', stop exchange in step {}".format('->'.join(chain), i))
                break

    def execute_chain_step(self, subchain, amount_coef=1.0, retry=False):
        assert(len(subchain) == 2)
        pair = subchain[0] + '_' + subchain[1]
        reverse_pair = subchain[1] + '_' + subchain[0]
        if pair in self.prices_holder.keys():
            # прямой обмен (по ask_rate, ask_amount)
            limit = self.trader.amount(subchain[0])
            limit = math.ceil(limit * amount_coef)  # тратим не более amount_coef бюджета в начальнйо валюте, TODO:
            if limit == 0:
                self.logger.warning("Can't do exchange {} ({}) because "\
                                    "I have {} = {}".format('->'.join(subchain), pair, subchain[0], limit))
                return False
            with self.prices_holder.get(pair) as price:
                ask_amount = price.ask_amount
                ask_rate = price.ask_rate
            can_ask_amount = math.ceil((limit - 1) / ask_rate * 10**self.precision)  # сколько можем купить второй валюты
            ret = self.trader.buy(pair, ask_rate, min(can_ask_amount, ask_amount))
        elif reverse_pair in self.prices_holder.keys():
            # обратный обмен (по bid_rate, bid_amount)
            limit = self.trader.amount(subchain[0])
            limit = math.ceil((limit - 1) * amount_coef)  # тратим не более amount_coef бюджета в начальнйо валюте, TODO:

            if limit == 0:
                self.logger.warning("Can't do exchange {} ({}) because "\
                                    "I have {} = {}".format('->'.join(subchain), reverse_pair, subchain[0], limit))
                return False
            with self.prices_holder.get(reverse_pair) as price:
                bid_amount = price.bid_amount
                bid_rate = price.bid_rate
            can_bid_amount = limit # сколько можем продать второй валюты
            ret = self.trader.sell(reverse_pair, bid_rate, min(can_bid_amount, bid_amount))
        else:
            self.logger.error("No information on exchange '{}'".format('->'.join(subchain)))
            return False
        if not ret:
            # не удачная операция, если это первый обмен в цепочке то и хер с ним, если
            # промежуточная - то пробуем еще раз
            if not retry:
                return False
            else:
                self.trader.refresh_balance()
                self.logger.info('Refreshed balance: {}'.format(self.trader.balance))
                return self.execute_chain_step(subchain, amount_coef, retry=False)
        else:
            return True


class prices_loader_t(Thread):
    def __init__(self, prices_holder, arbitrage_processor, executor_queue, config):
        super(prices_loader_t, self).__init__()
        self.prices_holder = prices_holder
        self.arbitrage_processor = arbitrage_processor
        self.executor_queue = executor_queue
        self.config = config
        self.url = config['orderbookd']['url']
        self.logger = logging.getLogger('cyborg.prices_loader')

    def run(self):
        self.logger.info('Start')
        self.stop_flag = False
        self.fail_counter = 0
        while not self.stop_flag:
            if self.fail_counter > 0:
                time.sleep(1.0)
            self.logger.info('WebSoket Start')
            self.ws = websocket.WebSocketApp( self.url,
                                              on_message = self.ws_on_message,
                                              on_error = self.ws_on_error,
                                              on_close = self.ws_on_close
                                            )
            self.ws.run_forever()
            self.logger.info('WebSoket Stop')
            self.fail_counter += 1
        self.logger.info('Stop')

    def stop(self):
        self.stop_flag = True
        self.ws.close()

    def ws_on_error(self, ws, error):
        self.logger.error('ERROR: {}'.format(error))
        self.ws.close()

    def ws_on_close(self, ws):
        self.logger.info('ws_on_close')

    def ws_on_message(self, ws, message):
        try:
            self.process_message(message)
        except Exception:
            self.logger.exception('Exception while processing message:')
            self.ws.close()

    def process_message(self, message):
        msgs = json.loads(message)
        for msg in msgs:
            action, data = msg
            if action == 'price_inc':
                exchange, pair, inc = price_inc_t.from_dict(data)
                if exchange == 'poloniex':  # TODO: пока поддерживаем только обновления с poloniex
                    with self.prices_holder.get(pair) as price:
                        price.apply(inc)
                    # self.logger.debug('{}: get {}'.format(pair, inc))
                    self.check_arbitrages(pair)
            elif action == 'price':
                exchange, pair, price = price_t.from_dict(data)
                if exchange == 'poloniex':  # TODO: пока поддерживаем только обновления с poloniex
                    # если такой пары еще не было - заводим
                    self.prices_holder.reg_or_update(pair, price)
                    # self.logger.debug('{}: get {}'.format(pair, price))
                    self.check_arbitrages(pair)
            else:
                self.logger.error('Unknown action: {}'.format(action))

    def check_arbitrages(self, pair):
        found_arbitrages = self.arbitrage_processor.check_arbitrages(pair)
        found_arbitrages = [arb for arb in found_arbitrages if arb.is_open == True]
        # TODO: это только для цепочек, начинающихся с USDT!
        for arbitrage in found_arbitrages:
            assert arbitrage.chain[0] == 'USDT'
        found_arbitrages = [arb for arb in found_arbitrages if arb.amount_start >= 2.0]
        if found_arbitrages:
            # Ищем лучший арбитраж - у которого наибольший rate
            best_idx = 0
            rates = [arb.rate_start for arb in found_arbitrages]
            best_idx = max(enumerate(rates), key=lambda t: t[1])
            try:
                self.executor_queue.put(found_arbitrages[best_idx[0]], block=True, timeout=0.1)
            except queue.Full:
                self.logger.warn("Timeout on putting message to executor_queue")


def main():
    settings = parse_args()
    with open(settings.config, 'r') as fin:
        config = yaml.load(fin)
    precision = config['precision']
    trader = poloniex_trader_t(config)
    exchange_limits = limits_t(config)
    prices_holder = concurrent_map_t()
    arbitrage_processor = arbitrage_processor_t(config, prices_holder)
    chain_executor = chain_executor_t(prices_holder, trader, config)
    logger.info('Rise cyborg')
    logger.info('{}'.format(trader.balance))

    prices_loader = prices_loader_t(prices_holder, arbitrage_processor, chain_executor.queue, config)
    prices_loader.start()
    chain_executor.start()

    try:
        while True:
            time.sleep(1)
    except:
        pass

    prices_loader.stop()
    chain_executor.stop()
    prices_loader.join()
    chain_executor.join()

    logger.info('Turn off cyborg')
