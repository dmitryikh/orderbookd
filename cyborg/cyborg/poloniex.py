import urllib.parse
import logging
import requests
from threading import Lock
import time
import hmac, hashlib

from orderbookd.util import strnum2int, int2strnum
from .balance import balance_t


class BadResponse(RuntimeError):
    pass


class poloniex_trader_t(object):
    def __init__(self, config, name=None):
        self.config = config
        self.precision = config['precision']
        self.APIKey = config['poloniex']['api_key']
        self.secret = bytes(open(config['poloniex']['secret_file'], 'r').read().strip(), encoding='ascii')
        self.trading_url = config['poloniex']['trading_api_url']

        self.session = requests.Session()
        self.session.headers.update({'Key': self.APIKey,
                                    'Content-Type': 'application/x-www-form-urlencoded',
                                   })
        self.lock = Lock()
        self.name = name if name else self.__class__.__name__
        self.logger = logging.getLogger(self.name)
        self.balance = balance_t(self.return_balances, config)

    def int2strnum(self, val):
        return int2strnum(val, precision_decode=self.precision, precision=self.precision)

    def amount(self, cur):
        return self.balance.amount(cur)

    def refresh_balance(self):
        self.balance.refresh()

    def _process_response(self, r):
        try:
            d = r.json()
        except Exception as e:
            raise BadResponse(str(e))

        if 'error' in d:
            # Варианты сообщений:
            #     Not enough BTC
            #     Total must be at least 0.0001
            #     Amount must be at least 0.000001
            raise BadResponse(d['error'])

        return d

    def _api_query(self, command, req={}):
        # NOTE: нужно помнить о 6 запросах в секунду..
        req['command'] = command
        req['nonce'] = int(time.time()*1000)
        post_data = bytes(urllib.parse.urlencode(req), encoding='ascii')

        headers = {
            'Sign': hmac.new(self.secret, post_data, hashlib.sha512).hexdigest(),
            'Key': self.APIKey,
        }
        with self.lock:
            res = self.session.post(self.trading_url, data=post_data, headers=headers)
            return self._process_response(res)


    # Returns all of your balances.
    # Outputs:
    # {"BTC":"0.59098578","LTC":"3.31117268", ... }
    def return_balances(self):
        self.logger.debug('Get balances')
        return {k: strnum2int(v, precision=self.precision) for k, v in self._api_query('returnBalances').items()}


    # Returns your trade history for a given market, specified by the "currencyPair" POST parameter
    # Inputs:
    # currencyPair  The currency pair e.g. "BTC_XCP"
    # Outputs:
    # date          Date in the form: "2014-02-19 03:44:59"
    # rate          Price the order is selling or buying at
    # amount        Quantity of order
    # total         Total value of order (price * quantity)
    # type          sell or buy
    def trade_history(self, pair):
        return self._api_query('returnTradeHistory', {"currencyPair": pair})

    # Пример ответа:
    #   {'orderNumber': '10931400664', 'resultingTrades': [{'amount': '223.00000000', 'date': '2018-07-10 17:38:07',
    #      'rate': '0.00000045', 'total': '0.00010035', 'tradeID': '2881809', 'type': 'buy'}], 'amountUnfilled': '0.00000000'}
    #
    #   {'orderNumber': '10931774290', 'resultingTrades': [], 'amountUnfilled': '223.00000000'}
    def buy(self, pair, rate, amount):
        rate = self.int2strnum(rate)
        amount = self.int2strnum(amount)
        self.logger.info('send buy {}, rate = {}, amount = {}'.format(pair, rate, amount))
        try:
            ret = self._api_query('buy', {"currencyPair": pair, "rate": rate, "amount": amount, "immediateOrCancel": 1})
        except Exception:
            self.logger.exception('')
            return False
        self.logger.debug(repr(ret))
        if strnum2int(ret['amountUnfilled'], precision=self.precision) != 0:
            self.logger.warning("sell failed: 'amountUnfilled' should be zero, got {}".format(ret['amountUnfilled']))
            return False
        else:
            res = ret['resultingTrades'][0]
            total = strnum2int(res['total'], precision=self.precision)
            amount = strnum2int(res['amount'], precision=self.precision)
            c1, c2 = pair.split('_')
            self.balance.sub(c1, total)
            self.balance.add(c2, amount, with_fee=True)

        return True

    def sell(self, pair, rate, amount):
        rate = self.int2strnum(rate)
        amount = self.int2strnum(amount)
        self.logger.info('sell {}, rate = {}, amount = {}'.format(pair, rate, amount))
        try:
            ret = self._api_query('sell', {"currencyPair": pair, "rate": rate, "amount": amount, "immediateOrCancel": 1})
        except Exception:
            self.logger.exception('')
            return False
        self.logger.debug(repr(ret))
        if strnum2int(ret['amountUnfilled'], precision=self.precision) != 0:
            self.logger.warning("sell failed: 'amountUnfilled' should be zero, got {}".format(ret['amountUnfilled']))
            return False
        else:
            res = ret['resultingTrades'][0]
            total = strnum2int(res['total'], precision=self.precision)  # сколько выручили до комиссии
            amount = strnum2int(res['amount'], precision=self.precision)  # сколько продали
            c1, c2 = pair.split('_')
            self.balance.add(c1, total, with_fee=True)
            self.balance.sub(c2, amount)

        return True


class limits_t(object):
    # На самом деле есть total limit и amount limit.
    # Для BTC_BCN:
    #     Total must be at least 0.0001
    #     Amount must be at least 0.000001
    # Для BTC_BCH:
    #     Total must be at least 0.0001
    # Для USDT_BTC:
    #     Amount must be at least 0.000001
    #     Total must be at least 1.
    # Для USDT_BCH:
    #     Total must be at least 1.
    def __init__(self, config):
        self.config = config
        self.precision = config['precision']
        self.buy_min_limits = {}
        for currency, min_amount in config['exchange']['min_amount'].items():
            if not isinstance(min_amount, str):
                raise ValueError('exchange::min_amount::{} should be string, use "'.format(currency))
            self.buy_min_limits[currency] = strnum2int(min_amount, precision=self.precision)

    def buy_min_limit(self, pair):
        return self.buy_min_limits.get(pair.split('_')[0], 0)

    def sell_min_limit(self, pair):
        """
        Вроде лимиты на покупку и продажу совпадают
        """
        return self.buy_min_limits.get(pair.split('_')[0], 0)
