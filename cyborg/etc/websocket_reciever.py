import argparse
import websocket
import time
import json
import logging
import random
import queue
import yaml

logging.basicConfig( format="%(asctime)s - %(name)s - %(levelname)s - %(message)s",
                     level=logging.DEBUG
                   )
logger = logging.getLogger('main')

def ws_on_open(ws):
    logger.info('ws_on_open')
    ws.send('lalalend')

def ws_on_error(ws, error):
    logger.error('ERROR: {}'.format(error))
    ws.close()

def ws_on_close(ws):
    logger.info('ws_on_close')

def ws_on_message(ws, message):
    try:
        ts = time.time()
        try:
            dt = ts - json.loads(message)[0][1]['ts']
        except:
            dt = 0.0
        print("'{}' (dt = {:.2f})".format(message, dt))
    except Exception:
        logger.exception('Exception while processing message:')
        ws.close()


ws = websocket.WebSocketApp( "ws://localhost:8080/prices/ws",
                                  on_message = ws_on_message,
                                  on_error = ws_on_error,
                                  on_close = ws_on_close
                                )
ws.on_open = ws_on_open
ws.run_forever()
