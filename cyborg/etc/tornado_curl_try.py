import asyncio
import argparse
import logging
import yaml

from tornado.httpclient import AsyncHTTPClient
from tornado import gen
from tornado.ioloop import IOLoop
AsyncHTTPClient.configure('tornado.curl_httpclient.CurlAsyncHTTPClient')

__logger = logging.getLogger('tornado.curl_httpclient')
# __logger.setLevel(logging.INFO)

from .poloniex import poloniex_trader_t

io = IOLoop.instance()

logging.basicConfig( format="%(asctime)s - %(name)s - %(levelname)s - %(message)s",
                     level=logging.DEBUG
                   )

logger = logging.getLogger('main')


def parse_args():
    parser = argparse.ArgumentParser(description='auto trade bot for poloniex',
                                     formatter_class=argparse.ArgumentDefaultsHelpFormatter)
    parser.add_argument('-c', '--config', type=str, required=True,
                        help='config file')
    return parser.parse_args()

@gen.coroutine
def do_requests(config):
    trader = poloniex_trader_t(config)
    # for i in range(5):
    logger.info('Start get balances')
    yield [trader.return_balances() for i in range(2)]
    # balances = await [trader.return_balances(), trader.return_balances(), trader.return_balances(), trader.return_balances(), trader.return_balances()]
    logger.info('Finish get balances')

def main():
    settings = parse_args()
    with open(settings.config, 'r') as fin:
        config = yaml.load(fin)
    io.run_sync(lambda: do_requests(config))
    # logger.info('Balance = \n{}'.format(balance))

