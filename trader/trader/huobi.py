import datetime
import base64
import urllib.parse
import logging
import requests
from threading import Lock
import time
import hmac, hashlib
import json

from .balance import balance_t


class BadResponse(RuntimeError):
    pass


class huobi_trader_t(object):
    def __init__(self, config, name=None):
        self.config = config
        # self.precision = config['precision']
        self.access_key = config['huobi']['access_key']
        self.secret = bytes(open(config['huobi']['secret_file'], 'r').read().strip(), encoding='ascii')
        self.trading_url = config['huobi']['trading_api_url']

        self.session = requests.Session()
        self.session.headers.update({ 'User-Agent': 'Mozilla/5.0 (Windows NT 6.1; WOW64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/39.0.2171.71 Safari/537.36',
                                      'Content-Type': 'application/x-www-form-urlencoded',
                                    })
        self.lock = Lock()
        self.name = name if name else self.__class__.__name__
        self.logger = logging.getLogger(self.name)
        accounts = self.get_accounts()
        self.logger.info(f'accounts: {accounts}')
        if len(accounts) > 1:
            self.logger.warning(f'Have more than one accounts ({accounts}). Pick first one')
        self.account = accounts[0]['id']


        self.balance = balance_t(self.get_balance, config)
        self.logger.info(f'{self.balance}')

    def amount(self, cur):
        return self.balance.amount(cur)

    def refresh_balance(self):
        self.balance.refresh()

    def return_balance(self, refresh=True):
        self.refresh_balance()
        return self.balance.balances

    @staticmethod
    def _create_sign(pParams, method, host_url, request_path, secret_key):
        sorted_params = sorted(pParams.items(), key=lambda d: d[0], reverse=False)
        encode_params = urllib.parse.urlencode(sorted_params)
        payload = [method, host_url, request_path, encode_params]
        payload = '\n'.join(payload)
        payload = payload.encode(encoding='UTF8')
        # secret_key = secret_key.encode(encoding='UTF8')

        digest = hmac.new(secret_key, payload, digestmod=hashlib.sha256).digest()
        signature = base64.b64encode(digest)
        signature = signature.decode()
        return signature

    def _api_key_get(self, params, request_path):
        method = 'GET'
        params.update({ 'AccessKeyId': self.access_key,
                        'SignatureMethod': 'HmacSHA256',
                        'SignatureVersion': '2',
                        'Timestamp': datetime.datetime.utcnow().strftime('%Y-%m-%dT%H:%M:%S'),
                      })

        host_name = urllib.parse.urlparse(self.trading_url).hostname.lower()
        params['Signature'] = self._create_sign(params, method, host_name, request_path, self.secret)

        url = self.trading_url + request_path
        return self._http_get_request(url, params)

    def _api_key_post(self, params, request_path):
        method = 'POST'
        params_to_sign ={ 'AccessKeyId': self.access_key,
                          'SignatureMethod': 'HmacSHA256',
                          'SignatureVersion': '2',
                          'Timestamp': datetime.datetime.utcnow().strftime('%Y-%m-%dT%H:%M:%S'),
                        }

        host_name = urllib.parse.urlparse(self.trading_url).hostname.lower()
        params_to_sign['Signature'] = self._create_sign(params_to_sign, method, host_name, request_path, self.secret)
        url = self.trading_url + request_path + '?' + urllib.parse.urlencode(params_to_sign)
        return self._http_post_request(url, params)

    def _http_get_request(self, url, params, headers={}):
        postdata = urllib.parse.urlencode(params)
        with self.lock:
            response = self.session.get(url, params=postdata, headers=headers, timeout=5) 
            try:
                if response.status_code == 200:
                    return response.json()
                else:
                    return
            except BaseException as e:
                self.logger.info("httpGet failed:%s,%s" %(response.text,e))
                raise BadResponse(str(e))

    def _http_post_request(self, url, params, add_to_headers=None):
        headers = {
            "Accept": "application/json",
            'Content-Type': 'application/json'
        }
        if add_to_headers:
            headers.update(add_to_headers)
        postdata = json.dumps(params)
        with self.lock:
            response = self.session.post(url, postdata, headers=headers, timeout=10)
            try:
                if response.status_code == 200:
                    return response.json()
                else:
                    return
            except BaseException as e:
                print("httpPost failed, detail is:%s,%s" %(response.text,e))
                raise BadResponse(str(e))
    # 
    # {'status': 'ok', 'data': [{'id': 4471518, 'type': 'spot', 'subtype': '', 'state': 'working'}]}
    def get_accounts(self):
        """
        :return: 
        """
        path = "/v1/account/accounts"
        params = {}
        return self._api_key_get(params, path)['data']

    def get_balance(self):
        url = "/v1/account/accounts/{0}/balance".format(self.account)
        params = {"account-id": self.account}
        return {d['currency'].upper(): float(d['balance']) for d in self._api_key_get(params, url)['data']['list']
                    if d['type'] == 'trade'} # and d['balance'] != '0'}

    def buy(self, pair, rate, amount):
        source = 'api'
        assert '_' in pair
        # BTC_USDT -> btcusdt
        symbol = ''.join(pair.split('_')).lower()
        # _type = 'buy-limit-maker'
        _type = 'buy-ioc'

        self.logger.info('send buy {}, rate = {}, amount = {}'.format(pair, rate, amount))
        params = {'account-id': self.account,
                  'amount': amount,
                  'symbol': symbol,
                  'type': _type,
                  'source': source,
                  'price': rate,
                 }

        url = '/v1/order/orders/place'
        ret = self._api_key_post(params, url)
        self.logger.debug(f'get response: {ret}')
        if ret['status'] == 'error' or 'data' not in ret:
            raise BadResponse(str(ret))
        order_id = ret['data']

        oret = self.order_info(order_id)
        # вырученные деньги равно: field-amount - field-fees
        # потрачено field-cash-amount
        if oret['data']['state'] != 'filled':
            raise BadResponse(str(oret))
        
    # Ответ:
    #  {'status': 'ok', 'data': '9899768776'}
    def sell(self, pair, rate, amount):
        source = 'api'
        assert '_' in pair
        # BTC_USDT -> btcusdt
        symbol = ''.join(pair.split('_')).lower()
        # sell-ioc продает по лучше цене, которая не меньше, чем rate
        _type = 'sell-ioc'

        self.logger.info('send sell {}, rate = {}, amount = {}'.format(pair, rate, amount))
        params = {'account-id': self.account,
                  'amount': amount,
                  'symbol': symbol,
                  'type': _type,
                  'source': source,
                  'price': rate,
                 }

        url = '/v1/order/orders/place'
        ret = self._api_key_post(params, url)
        self.logger.debug(f'get response: {ret}')
        if ret['status'] == 'error' or 'data' not in ret:
            raise BadResponse(str(ret))
        order_id = ret['data']

        oret = self.order_info(order_id)
        # вырученные деньги равно: field-cash-amount - field-fees
        if oret['data']['state'] != 'filled':
            raise BadResponse(str(oret))

    # {'status': 'ok', 'data': {'id': 9900423294, 'symbol': 'btcusdt',
    # 'account-id': 4471518, 'amount': '0.000100000000000000', 'price':
    # '5800.400000000000000000', 'created-at': 1534015839106, 'type': 'sell-ioc',
    # 'field-amount': '0.000100000000000000', 'field-cash-amount':
    # '0.637116000000000000', 'field-fees': '0.001274232000000000', 'finished-at':
    # 1534015839394, 'source': 'api', 'state': 'filled', 'canceled-at': 0}}
    def order_info(self, order_id):
        params = {}
        self.logger.debug(f'send order_info: order_id = {order_id}')
        url = "/v1/order/orders/{0}".format(order_id)
        ret = self._api_key_get(params, url)
        self.logger.debug(f'get response: {ret}')
        if ret['status'] != 'ok' or 'data' not in ret:
            raise BadResponse(str(ret))
        return ret