import json
import logging
import queue
from threading import Thread

class request_t():
    """
    Класс, хранящий в себе запрос и ответ
    """
    def __init__(self, req_str):
        self.req_str = req_str  # исходное строковое представление
        self.parse_req_str()

    @staticmethod
    def default():
        return request_t(json.dumps({'id': 0, 'action': ''}))

    def parse_req_str(self):
        self.req = json.loads(self.req_str)
        self.req_id = self.req['id']
        self.res = {'id': self.req_id}
        self.action = self.req['action']


class answer_sender_t(Thread):
    def __init__(self):
        super(answer_sender_t, self).__init__()
        self.q = queue.Queue()
        self.logger = logging.getLogger('answer_sender')

    def stop(self):
        self.stop_flag = True

    def get_queue(self):
        return self.q

    def run(self):
        self.logger.info('Start')
        self.stop_flag = False
        while not self.stop_flag:
            try:
                request = self.q.get(True, 1.0)
            except queue.Empty:
                    continue
            try:
                if not hasattr(request, 'send_action'):
                    raise ValueError(f"Got request without 'send_action' atrtribute: {request}")
                if not hasattr(request, 'res') or not isinstance(request.res, dict):
                    raise ValueError(f"Got request without 'res' atrtribute (or wrong type): {request}")
                request.send_action(json.dumps(request.res))
            except Exception:
                self.logger.exception('')
        self.logger.info('Stop')


class serial_trader_t(Thread):
    def __init__(self, trader, q_out):
        super(serial_trader_t, self).__init__()
        self.q_out = q_out
        self.q_in = queue.Queue()
        self.trader = trader
        self.logger = logging.getLogger(f'serial_trader({self.trader.name})')

    def stop(self):
        self.stop_flag = True

    def run(self):
        self.logger.info('Start')
        self.stop_flag = False
        while not self.stop_flag:
            try:
                request = self.q_in.get(block=True, timeout=5.0)
                self.logger.info(f'Starting execution: {request.req}')
                try:
                    self.process_request(request)
                except Exception as e:
                    self.logger.exception('')
                    request.res['status'] = str(e)
                self.q_out.put(request)
                self.logger.info(f'Finish execution: {request.req_id}')
            except queue.Empty:
                # В каждый свободный момент запрашиваем баланс (не чаще, чем раз в timeout секунд)
                # 1. Поддерживаем TCP соединение
                # 2. В момент простоя уточняем баланс
                try:
                    self.trader.refresh_balance()
                except:
                    self.logger.exception('')
            except Exception:
                # TODO: не должны сюда попасть. Возможно, нужно убрать обработчик
                self.logger.exception('')
                pass
        self.logger.info('Stop')

    def process_request(self, request):
        if request.action == 'sell':
            pair = request.req['pair']
            rate = request.req['rate']
            amount = request.req['amount']
            self.trader.sell(pair, rate, amount)
        elif request.action == 'buy':
            pair = request.req['pair']
            rate = request.req['rate']
            amount = request.req['amount']
            self.trader.buy(pair, rate, amount)
        elif request.action == 'balance':
            request.res['balance'] = self.trader.return_balance()
        else:
            # NOTE: небезопасная подстановка пользовательского ввода
            raise ValueError(f'Unknown command {request.action}')
        request.res['status'] = 'ok'