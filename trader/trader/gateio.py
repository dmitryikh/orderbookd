import datetime
import urllib.parse
import logging
import requests
from threading import Lock
import time
import hmac, hashlib
import json

from .balance import balance_t


class BadResponse(RuntimeError):
    pass


class gateio_trader_t(object):
    def __init__(self, config, name=None):
        self.config = config
        self.api_key = config['gateio']['api_key']
        self.secret = bytes(open(config['gateio']['secret_file'], 'r').read().strip(), encoding='ascii')
        self.trading_url = config['gateio']['trading_api_url']

        self.session = requests.Session()
        self.lock = Lock()
        self.name = name if name else self.__class__.__name__
        self.logger = logging.getLogger(self.name)

        self.balance = balance_t(self.get_balance, config)
        self.logger.info(f'{self.balance}')

    @staticmethod
    def _create_sign(params, bSecretKey):
        sign = ''
        for key in params.keys():
            value = str(params[key])
            sign += key + '=' + value + '&'
        bSign = bytes(sign[:-1], encoding='utf8')

        mySign = hmac.new(bSecretKey, bSign, hashlib.sha512).hexdigest()
        return mySign

    def _http_post_request(self, resource, params):
        headers = {
                "Content-type" : "application/x-www-form-urlencoded",
                "KEY": self.api_key,
                "SIGN": self._create_sign(params, self.secret)
        }

        tempParams = urllib.parse.urlencode(params) if params else ''
        with self.lock:
            response = self.session.post(self.trading_url + resource, tempParams, headers=headers, timeout=10)
            try:
                if response.status_code == 200:
                    return response.json()
                else:
                    return
            except BaseException as e:
                print("httpPost failed, detail is:%s,%s" %(response.text,e))
                raise BadResponse(str(e))

    def amount(self, cur):
        return self.balance.amount(cur)

    def refresh_balance(self):
        self.balance.refresh()

    def return_balance(self, refresh=True):
        self.refresh_balance()
        return self.balance.balances

    @staticmethod
    def _check_true(ret):
        if isinstance(ret['result'], str):
            return ret['result'].lower() == 'true'
        else:
            return ret['result']

    def get_balance(self):
        URL = "/api2/1/private/balances"
        param = {}
        ret = self._http_post_request(URL, param)
        if ret['result'] != 'true' or 'available' not in ret:
            raise BadResponse(str(ret))

        return {k: float(v) for k,v in ret['available'].items()}

    def buy(self, pair, rate, amount):
        URL = "/api2/1/private/buy"
        symbol = pair.lower()
        params = {'currencyPair': symbol,'rate': rate, 'amount': amount}
        self.logger.info('send buy {}, rate = {}, amount = {}'.format(pair, rate, amount))
        ret = self._http_post_request(URL, params)
        self.logger.debug(f'get response: {ret}')
        if not self._check_true(ret):
            raise BadResponse(str(ret))
        # NOTE: в случае успеха приходит сообщение:
        # {'result': 'true', 'message': 'Success', 'code': 0, 'orderNumber':
        # 1185027065, 'rate': '6310', 'leftAmount': '0.00000000',
        # 'filledAmount': '0.0002', 'filledRate': 6308.7, 'feePercentage':
        # 0.002, 'feeValue': '0.0000004', 'feeCurrency': 'BTC', 'fee':
        # '0.0000004 BTC'}
        order_id = ret['orderNumber']
        if float(ret['leftAmount']) != 0:
            # не полностью закрыли ордер, закрываем его насильно и возвращаем ошибку
            self.cancel_order(order_id, pair)
            raise BadResponse(str(self.get_order(order_id, pair)))

    def sell(self, pair, rate, amount):
        URL = "/api2/1/private/sell"
        symbol = pair.lower()
        params = {'currencyPair': symbol, 'rate': rate, 'amount': amount}
        self.logger.info('send sell {}, rate = {}, amount = {}'.format(pair, rate, amount))
        ret = self._http_post_request(URL, params)
        # NOTE: может придти:
        #   {'result': 'false', 'message': 'Your order size is too small. The minimum is 1 USDT', 'code': 20}
        # или:
        #   {'result': 'true', 'message': 'Success', 'code': 0, 'orderNumber':
        #   1184872126, 'rate': '6327.9', 'leftAmount': '0.00000000',
        #   'filledAmount': '0.0002', 'filledRate': 6333.08, 'feePercentage':
        #   0.002, 'feeValue': '0.002533232', 'feeCurrency': 'USDT', 'fee':
        #   '0.002533232 USDT'}
        self.logger.debug(f'get response: {ret}')
        if not self._check_true(ret):
            raise BadResponse(str(ret))
        order_id = ret['orderNumber']
        if float(ret['leftAmount']) != 0:
            # не полностью закрыли ордер, закрываем его насильно и возвращаем ошибку
            self.cancel_order(order_id, pair)
            raise BadResponse(str(self.get_order(order_id, pair)))
        # return self.get_order(order_id, pair)

    def cancel_order(self, orderNumber, pair):
        URL = "/api2/1/private/cancelOrder"
        symbol = pair.lower()
        params = {'orderNumber': orderNumber, 'currencyPair': symbol}
        self.logger.debug(f'cancel order {orderNumber} {pair}')
        ret = self._http_post_request(URL, params)
        self.logger.debug(f'get response: {ret}')
        if not self._check_true(ret):
            raise BadResponse(str(ret))


    # {'result': 'true', 'order': {'orderNumber': '1184984309', 'status':
    # 'cancelled', 'currencyPair': 'btc_usdt', 'type': 'sell', 'rate':
    # '7000.9', 'left': 0.0002, 'amount': '0.00000000', 'initialRate':
    # '7000.9', 'initialAmount': '0.0002', 'filledAmount': '0', 'filledRate':
    # '7000.9', 'feePercentage': 0.2, 'feeValue': '0', 'feeCurrency': 'USDT',
    # 'fee': '0 USDT', 'timestamp': 1534103625}, 'message': 'Success', 'code':
    # 0, 'elapsed': '6.26087ms'}
    def get_order(self, orderNumber, pair):
        URL = "/api2/1/private/getOrder"
        symbol = pair.lower()
        params = {'orderNumber': orderNumber, 'currencyPair': symbol}
        self.logger.debug(f'get order status {orderNumber} {pair}')
        ret = self._http_post_request(URL, params)
        self.logger.debug(f'get response: {ret}')
        if not self._check_true(ret) or 'order' not in ret:
            raise BadResponse(str(ret))
        return ret['order']