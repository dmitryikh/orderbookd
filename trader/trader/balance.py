import math
from threading import Lock
from orderbookd.util import is_dicts_equal, int2strnum


class balance_t(object):
    """
    Класс, отслеживающий текущий баланс на бирже в разных валютах.
    Потокобезопасный
    """
    def __init__(self, get_balance_func, fee):
        self.fee = fee
        self.get_balance_func = get_balance_func
        self.balances = self._fetch_balance()
        self.lock = Lock()

    def _fetch_balance(self):
        return {k: v for k, v in self.get_balance_func().items() if v != 0}

    def refresh(self):
        with self.lock:
            self.balances = self._fetch_balance()

    def add(self, cur, amount, with_fee=False):
        with self.lock:
            if with_fee:
                # NOTE: надо подумать об округлении и расчете без потери точности
                amount = amount * (1.0 - self.fee)
            self.balances[cur] = self.balances.get(cur, 0) + amount

    def sub(self, cur, amount, with_fee=False):
        with self.lock:
            if with_fee:
                amount = amount * (1.0 - self.fee)
            # NOTE: нужна ли проверка на то, что уйдем в минус?
            self.balances[cur] = self.balances.get(cur, 0) - amount

    def amount(self, cur):
        return self.balances.get(cur, 0)

    def _compare_assert(self):
        """
        Сравниваем баланс с тем, что вернется от биржи. Нужно для отладки, что мы локально правильно
        ведем подсчет баланса.
        """
        balances = self._fetch_balance()
        for cur in balances.keys():
            if abs(balances[cur] - self.balances[cur]) > 2e-8:
                raise RuntimeError("Different balances for '{}': got {}, expected {}".format(cur, balances[cur], self.balances[cur]))

    def __str__(self):
        ret = 'Balances: '
        ret += ', '.join( map(lambda kv: '{} = {}'.format( kv[0], kv[1]),
                          self.balances.items())
                        )
        return ret
