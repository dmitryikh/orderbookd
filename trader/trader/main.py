import argparse
import logging
import time
import yaml

from .http import daemon_http_server_thread_t
from .huobi import huobi_trader_t
from .gateio import gateio_trader_t
from .util import answer_sender_t, serial_trader_t

logging.basicConfig( format="%(asctime)s - %(name)s - %(levelname)s - %(message)s",
                     level=logging.DEBUG
                   )
logger = logging.getLogger('main')


def parse_args():
    parser = argparse.ArgumentParser(description='bot for traiding operations',
                                     formatter_class=argparse.ArgumentDefaultsHelpFormatter)
    parser.add_argument('-c', '--config', type=str, required=True,
                        help='config file')
    return parser.parse_args()


def main():
    settings = parse_args()
    with open(settings.config, 'r') as fin:
        config = yaml.load(fin)

    logger.info('Start trader')
    answer_sender = answer_sender_t()
    traders = { 'huobi': huobi_trader_t(config),
                'gateio': gateio_trader_t(config),
              }
    for name, trader in traders.items():
        logger.info(f'{name}: {trader.balance}')

    serial_traders = {name: serial_trader_t(trader, answer_sender.get_queue()) for name, trader in traders.items()}
    exchanges_queues = {name: serial_trader.q_in for name, serial_trader in serial_traders.items()}

    http = daemon_http_server_thread_t(exchanges_queues, answer_sender.get_queue(), config)

    answer_sender.start()
    for serial_trader in serial_traders.values():
        serial_trader.start()
    http.start()

    try:
        while True:
            time.sleep(1)
    except:
        pass

    for serial_trader in serial_traders.values():
        serial_trader.stop()
    answer_sender.stop()
    http.stop()

    answer_sender.join()
    for serial_trader in serial_traders.values():
        serial_trader.join()
    http.join()

    logger.info('Stop trader')
