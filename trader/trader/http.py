# encoding: utf-8
# import asyncio
import logging
from threading import Thread
import json

from tornado.ioloop import IOLoop
from tornado.web import Application, RequestHandler, url
from tornado.websocket import WebSocketHandler

# import orderbookd.db as db
# from .util import datetime_str_to_ts, ts_to_datetime_str, timestamp_now, strnum2int, int2strnum, supported_exchanges, DATETIME_REGEXP
from .util import request_t


class http_server_thread_t(Thread):
    def __init__(self, port):
        super(http_server_thread_t, self).__init__()
        self.port = port
        self.app = Application()
        self.logger = logging.getLogger('trader.http')

    def add_handlers(self, urls):
        self.app.add_handlers(r'.*', urls)

    def run(self):
        self.logger.info('Start')
        self.app.listen(self.port)
        self.ioloop = IOLoop.current()
        self.ioloop.start()
        self.logger.info('Stop')

    def stop(self):
        if self.is_alive():
            if not hasattr(self, 'ioloop'):
                raise RuntimeError("Can't stop thread: no event loop")
            self.ioloop.add_callback(self.ioloop.stop)


class daemon_handler_t(RequestHandler):
        def initialize(self, http):
            self.http = http

        def set_default_headers(self):
            self.set_header('Content-Type', 'text/plain; charset=UTF-8')

        def if_debug(self):
            if self.get_argument('debug', '0') in  ['1', 'true', 'True']:
                return True
            return False

        # def parse_exchange(self):
        #     exchange = self.get_argument('exchange', '')
        #     if not exchange:
        #         raise ValueError('`exchange` parameter is not provided')
        #     if exchange not in supported_exchanges:
        #         raise ValueError('supported exhange: {}'.format(','.join(supported_exchanges)))
        #     return exchange


class daemon_ws_handler_t(WebSocketHandler):
        def initialize(self, http):
            self.http = http


class daemon_http_server_thread_t(http_server_thread_t):
    class prices_updates(daemon_ws_handler_t):
        def open(self):
            self.set_nodelay(True)
            self.http.logger.info("Get new websocket connection from: '{}'".format(repr(self.request)))

        def on_message(self, message):
            request = None
            try:
                request = request_t(message)
                request.send_action = lambda msg: self.write_message('{}'.format(msg))
                if request.action in ('buy', 'sell', 'balance'):
                    exchange = request.req['exchange']
                    self.http.exchange_queues[exchange].put(request)
                else:
                    raise ValueError(f'Unknown action: {request.action}')
            except Exception as e:
                # Стараемся кое-как заполнить структуру request и отправляем в очередь на отправку
                # Тут сами ничего не пишем, что бы не создавать гонки данных
                if request is None:
                    request = request_t.default()
                if not hasattr(request, 'res'):
                    request.res = {'status': str(e), 'id': 0}
                else:
                    request.res['status'] = str(e)
                request.send_action = lambda msg: self.write_message('{}'.format(msg))
                self.http.q_out.put(request)

        def on_close(self):
            self.http.logger.info("Close websocket connection from: '{}'".format(repr(self.request)))

    def __init__(self, exchange_queues, q_out, config):
        super(daemon_http_server_thread_t, self).__init__(config['http']['port'])
        self.config = config
        self.exchange_queues = exchange_queues
        self.q_out = q_out
        self.add_handlers([ url(r'/trader/ws', self.prices_updates, dict(http=self)),
                          ])