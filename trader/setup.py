from setuptools import setup, find_packages
from os.path import join, dirname
import trader

setup(
    name='trader',
    version=trader.__version__,
    packages=find_packages(),
    install_requires=[
        'tornado==4.4.2',
        'PyYAML==3.12',
    ]
)
