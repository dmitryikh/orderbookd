import argparse
import websocket
import time
import json
import logging
import random
import queue
import yaml

logging.basicConfig( format="%(asctime)s - %(name)s - %(levelname)s - %(message)s",
                     level=logging.DEBUG
                   )
logger = logging.getLogger('main')

def ws_on_open(ws):
    logger.info('ws_on_open')
    ws.send(json.dumps({'id': 1, 'exchange': 'huobi', 'action': 'balance'}))
    ws.send(json.dumps({'id': 2, 'exchange': 'gateio', 'action': 'balance'}))
    # ws.send(json.dumps({'id': 2, 'exchange': 'huobi', 'action': 'sell', 'pair': 'BTC_USDT', 'amount': 0.0001, 'rate': 6378}))

def ws_on_error(ws, error):
    logger.error('ERROR: {}'.format(error))
    ws.close()

def ws_on_close(ws):
    logger.info('ws_on_close')

def ws_on_message(ws, message):
    try:
        print(f"'{message}'")
    except Exception:
        logger.exception('Exception while processing message:')
        ws.close()


ws = websocket.WebSocketApp( "ws://localhost:8008/trader/ws",
                                  on_message = ws_on_message,
                                  on_error = ws_on_error,
                                  on_close = ws_on_close
                                )
ws.on_open = ws_on_open
ws.run_forever()
