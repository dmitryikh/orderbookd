import argparse
import logging
import time
import yaml

from trader.gateio import gateio_trader_t

logging.basicConfig( format="%(asctime)s - %(name)s - %(levelname)s - %(message)s",
                     level=logging.DEBUG
                   )
logger = logging.getLogger('main')


def parse_args():
    parser = argparse.ArgumentParser(description='bot for traiding operations',
                                     formatter_class=argparse.ArgumentDefaultsHelpFormatter)
    parser.add_argument('-c', '--config', type=str, required=True,
                        help='config file')
    return parser.parse_args()


def main():
    settings = parse_args()
    with open(settings.config, 'r') as fin:
        config = yaml.load(fin)

    trader = gateio_trader_t(config)

    # trader.sell(pair='BTC_USDT', rate=7000.90000000 , amount=0.0002)
    # trader.buy(pair='BTC_USDT', rate=6310, amount=0.0002)
    # logger.info(f'{ret}')
    # _id = ret['data']
    # ret = trader.order_info(_id)
    # logger.info(f'{ret}')
    trader.refresh_balance()
    logger.info(f'{trader.balance}')

if __name__ == '__main__':
    main()