import argparse
import logging
import time
import yaml

from trader.huobi import huobi_trader_t

logging.basicConfig( format="%(asctime)s - %(name)s - %(levelname)s - %(message)s",
                     level=logging.DEBUG
                   )
logger = logging.getLogger('main')


def parse_args():
    parser = argparse.ArgumentParser(description='bot for traiding operations',
                                     formatter_class=argparse.ArgumentDefaultsHelpFormatter)
    parser.add_argument('-c', '--config', type=str, required=True,
                        help='config file')
    return parser.parse_args()


def main():
    settings = parse_args()
    with open(settings.config, 'r') as fin:
        config = yaml.load(fin)

    trader = huobi_trader_t(config)

    # trader.sell(pair='BTC_USDT', rate=7000.40000000, amount=0.0001)
    trader.buy(pair='BTC_USDT', rate=6390, amount=0.0003)
    # logger.info(f'{ret}')
    # _id = ret['data']
    # ret = trader.order_info(_id)
    # logger.info(f'{ret}')
    trader.refresh_balance()
    logger.info(f'{trader.balance}')

if __name__ == '__main__':
    main()