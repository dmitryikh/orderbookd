from setuptools import setup, find_packages
from os.path import join, dirname
import orderbookd

setup(
    name='orderbookd',
    version=orderbookd.__version__,
    packages=find_packages(),
    long_description=open(join(dirname(__file__), 'README.md'), encoding='utf-8').read(),
    install_requires=[
        'tornado==4.4.2',
        'mysql-connector==2.1.6',
        'PyYAML==3.12',
        'websocket-client==0.47.0',
	'pytz==2016.10',
    ]
)
