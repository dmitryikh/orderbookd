import logging
import random
import json
import requests
import websocket

import orderbookd.db as db
from .common_loader import common_loader_t
from .util import strnum2int, timestamp_now
from .order_book import inc_t, orderbook_t

def supported_pairs():
    w_btc = [ 'TRX', 'ETH', 'EOS', 'BNB', 'PPT', 'ELF', 'LUN', 'NEO', 'CLOAK', 'CMT', 'XRP', 'ADA', 'ZIL', 'ZEC', 'BCN',
              'LTC', 'TUSD', 'ICX', 'ONT', 'IOST', 'XVG', 'NANO', 'STORM', 'XMR', 'WAVES', 'BCC', 'MTL', 'ARN', 'NAV',
              'WAN', 'STRAT', 'GVT', 'VEN', 'BRD', 'DASH', 'EDO', 'NEBL', 'SALT', 'GTO', 'SUB', 'OMG', 'QLC', 'SNT', 'BQX',
              'IOTA', 'MCO', 'XLM', 'ZRX', 'VIB', 'WINGS', 'POWR', 'NULS', 'LOOM', 'NCASH', 'RCN', 'ENG', 'STEEM', 'QTUM',
              'BCPT', 'POA', 'BTG', 'SYS', 'LSK', 'ETC', 'WTC', 'QSP', 'SNM', 'RLC', 'AE', 'AION', 'INS', 'BTS', 'XEM', 'AST',
              'DGD', 'ENJ', 'POE', 'YOYO', 'OST', 'FUN', 'GRS', 'RPX', 'KMD', 'HSR', 'LINK', 'WPR', 'XZC', 'ADX', 'KNC', 'BAT',
              'REQ', 'TNB', 'VIBE', 'BLZ', 'LEND', 'TRIG', 'ARK', 'AMB', 'GNT', 'MDA', 'CND', 'MANA', 'DNT', 'FUEL', 'APPC',
              'GAS', 'LRC', 'REP', 'SNGLS', 'CDT', 'RDN', 'DLT', 'STORJ', 'BNT', 'GXS', 'MOD', 'VIA', 'PIVX', 'OAX', 'WABI',
              'CHAT', 'TNT', 'MTH', 'EVX', 'BCD', 'ICN'
            ]
    w_bnb = [ 'TUSD', 'BCC', 'ZIL', 'NEO', 'ONT', 'LTC', 'ICX', 'CMT', 'ADA', 'XLM', 'BCN', 'VEN', 'IOTA', 'NANO', 'LOOM',
              'NULS', 'STEEM', 'WAN', 'XEM', 'AE', 'WAVES', 'LSK', 'SYS', 'QTUM', 'STORM', 'NCASH', 'POA', 'WTC', 'GTO',
              'BAT', 'YOYO', 'QSP', 'XZC', 'BLZ', 'OST', 'AION', 'RLC', 'BTS', 'ADX', 'TRIG', 'POWR', 'AMB', 'NEBL', 'MCO',
              'BRD', 'RDN', 'RPX', 'QLC', 'CND', 'GNT', 'DLT', 'VIA', 'RCN', 'APPC', 'REP', 'BCPT', 'WABI', 'NAV', 'PIVX'
            ]

    w_eth = [ 'EOS', 'TRX', 'CLOAK', 'CMT', 'BNB', 'STORM', 'ZIL', 'NEO', 'XRP', 'ICX', 'ONT', 'ZEC', 'WAN', 'ADA', 'VEN',
              'IOST', 'XVG', 'BCC', 'TUSD', 'NANO', 'ARN', 'LOOM', 'LTC', 'IOTA', 'LUN', 'ZRX', 'BCN', 'XLM', 'SALT', 'POA',
              'OMG', 'QTUM', 'VIB', 'ELF', 'NCASH', 'AE', 'NULS', 'QSP', 'ETC', 'XMR', 'SYS', 'AION', 'WTC', 'STEEM', 'BTS',
              'GTO', 'RLC', 'STRAT', 'LRC', 'DGD', 'WAVES', 'REQ', 'KNC', 'AMB', 'ENJ', 'BLZ', 'LSK', 'DASH', 'BTG', 'GXS',
              'CDT', 'AST', 'BNT', 'BAT', 'FUN', 'PPT', 'WPR', 'SNM', 'POE', 'MANA', 'LEND', 'XEM', 'LINK', 'ENG', 'OST', 'SUB',
              'WINGS', 'REP', 'NEBL', 'GNT', 'SNT', 'BQX', 'GRS', 'YOYO', 'TNB', 'HSR', 'ADX', 'SNGLS', 'KMD', 'POWR', 'FUEL',
              'INS', 'MCO', 'BRD', 'GVT', 'ARK', 'RDN', 'VIBE', 'QLC', 'RCN', 'XZC', 'NAV', 'CND', 'WABI', 'DLT', 'RPX', 'MTL',
              'DNT', 'APPC', 'CHAT', 'TRIG', 'MOD', 'BCPT', 'OAX', 'STORJ', 'PIVX', 'MTH', 'EVX', 'TNT', 'MDA', 'VIA', 'ICN',
              'EDO', 'BCD'
            ]
    
    w_usdt = [ 'BTC', 'ETH', 'BNB', 'BCC', 'NEO', 'ADA', 'LTC', 'XRP', 'QTUM']

    pairs = []
    for base, array in zip(['BTC', 'BNB', 'ETH', 'USDT'], [w_btc, w_bnb, w_eth, w_usdt]):
        pairs.extend([cur + '_' + base for cur in array])

    return pairs

def subset_of_supported_pairs(pairs):
    all_supported_pairs = set([p.upper() for p in supported_pairs()])
    subset_supported_pairs = []
    for pair in pairs:
        if pair in all_supported_pairs:
            subset_supported_pairs.append(pair)
        else:
            reverse_pair = '_'.join(pair.split('_')[::-1])
            if reverse_pair in all_supported_pairs:
                subset_supported_pairs.append(reverse_pair)

    return subset_supported_pairs

def parse_orderbook(data, orderbook, ts, precision=8):
    """
    Формат данных:
    ```
        {"lastUpdateId":271871263,
         "bids":[["0.06187800","4.58500000",[]],["0.06187700","0.58200000",[]],..],
         "asks":[["0.06190400","7.49400000",[]],...]
        }
    ```
    """
    orderbook.asks = {strnum2int(x[0], precision=precision): strnum2int(x[1], precision=precision) for x in data['asks']}
    orderbook.bids = {strnum2int(x[0], precision=precision): strnum2int(x[1], precision=precision) for x in data['bids']}
    orderbook.ts = ts
    orderbook.seq = 0  # т.к. binance имеет слолжны механизм seq'ок, то мы используем свой


class loader_t(common_loader_t):
    def __init__(self, dumper_queue, orderbooks_holder, prices_holder, ws_queue, config):
        super(loader_t, self).__init__( exchange='binance',
                                        dumper_queue=dumper_queue,
                                        orderbooks_holder=orderbooks_holder,
                                        prices_holder=prices_holder,
                                        ws_queue=ws_queue,
                                        config=config,
                                        logger=logging.getLogger('binance_loader'),
                                      )
        self.pairs = subset_of_supported_pairs(config[self.exchange]['pairs'])
        if len(self.pairs) != len(config[self.exchange]['pairs']):
            self.logger.error('Supported {} of {} pairs'.format(len(self.pairs), len(config[self.exchange]['pairs'])))
            self.logger.error('{}'.format(','.join(self.pairs)))
            raise RuntimeError('Unsupported pairs')
        self.level = self.config[self.exchange]['level']
        self.seq_counters = self.get_last_seqs()
        self.stream2pair = {}

    def get_last_seqs(self):
        """
        Т.к. gateio не поддерживает seq, то нам нужно поддерживать его исскуственно. Для этого нам нужно
        получать из базы последний записанный seq
        """
        if self.config['db']['use']:
            conn = db.get_connection(self.config['db'])
            ret = db.select_last_seq_increments(conn, self.exchange, self.pairs)
            conn.close()
        else:
            self.logger.warning('db.use = False, last seqs set to 0')
            ret = {p: 0 for p in self.pairs}
        return ret


    def serve_connetion(self):
        stream_list = list(map(lambda x: '{}@depth{}'.format(x.replace('_', '').lower(), self.level), self.pairs))
        self.stream2pair = {stream: pair for stream, pair in zip(stream_list, self.pairs)}
        streams = '/'.join(stream_list)
        self.logger.debug('URL is: {}'.format(f"wss://stream.binance.com:9443/stream?streams={streams}"))
        self.ws = websocket.WebSocketApp( f"wss://stream.binance.com:9443/stream?streams={streams}",
                                          on_message = self.ws_on_message,
                                          on_error = self.ws_on_error,
                                          on_close = self.ws_on_close
                                        )
        self.ws.on_open = self.ws_on_open
        if 'proxies' in self.config and len(self.config['proxies']) > 0:
            # TODO: сейчас прокси выбирается случайно, но нужен более продвинутый алгоритм,
            # который бы учитывал сбои на прокси и их нагруженность
            proxy = random.choice(self.config['proxies'])
            self.ws.run_forever( http_proxy_host=proxy['host'],
                                 http_proxy_port=proxy['port'],
                                 http_proxy_auth=(proxy['login'], proxy['password']),
                                )
        else:
            self.ws.run_forever()

    def stop_connection(self):
        if hasattr(self, 'ws'):
            self.ws.close()

    def ws_on_open(self, ws):
        self.logger.info('ws_on_open')

    def ws_on_error(self, ws, error):
        self.logger.error('ERROR: {}'.format(error))
        self.ws.close()

    def ws_on_close(self, ws):
        self.logger.info('ws_on_close')

    def ws_on_message(self, ws, message):
        try:
            self.process_message(message)
        except Exception:
            self.logger.exception('Exception while processing message:')
            self.ws.close()

    def process_message(self, message):
        ts = timestamp_now()
        msg = json.loads(message)

        stream = msg['stream']
        if  stream not in self.stream2pair:
            raise RuntimeError(f'Unknown stream {stream}')
        pair = self.stream2pair[stream]
        def extract(ob):
            parse_orderbook(msg['data'], ob, ts, precision=self.config['precision'])
            ob.seq = self.seq_counters[pair]
        if pair not in self.last_dump_period:
            # пришел полный ордербук в первый раз (с учетом лимита, конечно)
            # в случае первой загруги ордербука предполагаем, что мы пропустили ряд обновлений,
            # поэтому пропускаем 2 seq (символически, чтобы потом найти дыру в данных)
            self.seq_counters[pair] += 2
            self.process_intial_orderbook(pair, extract)
        else:
            # промежуточная загрузка полного дампа, делаем из него инкремент и обрабатываем как
            # обычно
            self.seq_counters[pair] += 1
            ob_new = orderbook_t()
            extract(ob_new)
            with self.orderbooks_holder.get((self.exchange, pair)) as orderbook:
                increment = orderbook.diff(ob_new, check_seq=True)
            if increment is None:
                # Бинанс присылает данные каждую секунду. Даже если там не было изменений.
                # Мы просто делаем вид, что ничего не приходило
                self.seq_counters[pair] -= 1
                # self.logger.warning(f'Got intermidiate full orderbook for {pair}, but increment is empty')
                return
            else:
                self.process_increment(pair, increment)