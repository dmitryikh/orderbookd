import argparse
import time
import json
import logging
from threading import Thread
import queue
import yaml

from .http import daemon_http_server_thread_t
import orderbookd.db as db
from .util import ts2minute, timestamp_now, queue_with_action_t, concurrent_map_t, supported_exchanges
from .order_book import inc_t, orderbook_t
from .price import price_processor_t
import orderbookd.poloniex as poloniex
import orderbookd.gateio as gateio
import orderbookd.binance as binance
import orderbookd.livecoin as lc
import orderbookd.huobi as huobi

logging.basicConfig( format="%(asctime)s - %(name)s - %(levelname)s - %(message)s",
                     level=logging.DEBUG
                   )

def parse_args():
    parser = argparse.ArgumentParser(description='collect pushes for order books',
                                     formatter_class=argparse.ArgumentDefaultsHelpFormatter)
    parser.add_argument('-c', '--config', type=str, required=True,
                        help='config file')
    return parser.parse_args()


class dumper_t(Thread):
    def __init__(self, config):
        super(dumper_t, self).__init__()
        self.config = config
        self.use = config['db']['use']
        self.db_con = None
        if self.use:
            self.db_con = db.get_connection(config['db'])
        self.q_in = queue.Queue()
        self.logger = logging.getLogger('app.inc_dumper')
        self.stop_flag = False

    def get_queue(self):
        return self.q_in

    def stop(self):
        self.stop_flag = True

    def run(self):
        self.logger.info('Start')
        self.stop_flag = False
        while True:
            try:
                exchange, pair, el = self.q_in.get(True, 1.0)
            except queue.Empty:
                if self.stop_flag == True:
                    break
                else:
                    continue
            try:
                if isinstance(el, inc_t):
                    if self.use:
                        db.insert_increment(self.db_con, exchange, pair, el, self.config['precision'])
                elif isinstance(el, orderbook_t):
                    if self.use:
                        db.insert_orderbook(self.db_con, exchange, pair, el, self.config['period'], self.config['precision'])
                else:
                    raise RuntimeError('Get unknown element')
            except:
                self.logger.exception('Error while dumping to database:')
        self.logger.info('Stop')


def create_tables(config):
    """
    Для собираемых пар и бирж создаем таблицы в БД
    """
    conn = db.get_connection(config['db'])
    for exchange in supported_exchanges:
        if config[exchange]['collect']:
            for pair in config[exchange]['pairs']:
                if not db.is_tables_for_pair_exist(conn, exchange, pair, config['db']['database']):
                    db.create_tables_for_pair(conn, exchange, pair)

def main():
    settings = parse_args()
    with open(settings.config, 'r') as fin:
        config = yaml.load(fin)
    if config['db']['use']:
        create_tables(config)
    poloniex_pair_loaders = {}
    gateio_loader = None
    binance_loader = None
    livecoin_loader = None
    huobi_loader = None
    # Регистрируем хранилища для ордербуков и лучших цен
    orderbooks_holder = concurrent_map_t()
    prices_holder = concurrent_map_t()

    for exchange in supported_exchanges:
        if config[exchange]['collect']:
            for pair in config[exchange]['pairs']:
                prices_holder.reg((exchange, pair), price_processor_t(exchange, pair, orderbooks_holder, amount_threshold=0))
                orderbooks_holder.reg((exchange, pair), orderbook_t())

    dumper = dumper_t(config)
    # очередь событий для рассылки по WebSocket (см. http.py)
    ws_queue = queue_with_action_t('ws_queue')
    http = daemon_http_server_thread_t(orderbooks_holder, prices_holder, ws_queue, config)
    try:
        dumper.start()
        ws_queue.start()

        if config['poloniex']['collect']:
            for pair in config['poloniex']['pairs']:
                loader = poloniex.pair_loader_t(pair, dumper.get_queue(), orderbooks_holder, prices_holder, ws_queue, config)
                loader.start()
                poloniex_pair_loaders[pair] = loader

        if config['gateio']['collect']:
            gateio_loader = gateio.loader_t(dumper.get_queue(), orderbooks_holder, prices_holder, ws_queue, config)
            gateio_loader.start()


        if config['binance']['collect']:
            binance_loader = binance.loader_t(dumper.get_queue(), orderbooks_holder, prices_holder, ws_queue, config)
            binance_loader.start()

        if config['livecoin']['collect']:
            livecoin_loader = lc.loader_t(dumper.get_queue(), orderbooks_holder, prices_holder, ws_queue, config)
            livecoin_loader.start()

        if config['huobi']['collect']:
            huobi_loader = huobi.loader_t(dumper.get_queue(), orderbooks_holder, prices_holder, ws_queue, config)
            huobi_loader.start()

        http.start()

        while True:
            time.sleep(1)
    except (KeyboardInterrupt, SystemExit):
        pass

    ws_queue.stop()
    http.stop()
    dumper.stop()
    for loader in poloniex_pair_loaders.values():
        loader.stop()

    if gateio_loader:
        gateio_loader.stop()
    if binance_loader:
        binance_loader.stop()
    if livecoin_loader:
        livecoin_loader.stop()
    if huobi_loader:
        huobi_loader.stop()

    ws_queue.join()
    http.join()
    dumper.join()
    for loader in poloniex_pair_loaders.values():
        loader.join()
    if gateio_loader:
        gateio_loader.join()
    if binance_loader:
        binance_loader.join()
    if livecoin_loader:
        livecoin_loader.join()
    if huobi_loader:
        huobi_loader.join()