# encoding: utf-8
import bisect
import json
from threading import Lock

from .util import ts_to_datetime_str, strnum2int,\
                  varint_encode, varint_decode,\
                  is_dicts_equal


class inc_t(object):
    def __init__(self, seq, ts):
        self.seq = seq
        self.ts = ts

        self.incs = []

    def is_empty(self):
        return not bool(self.incs)

    def add_inc(self, action, rate, amount):
        assert action in ['a', 'b']
        self.incs.append([action, rate, amount])

    def __str__(self):
        return str(self.incs)


class orderbook_t(object):
    """
    Значения для rate и amount - целый числа (см. strnum2int)
    """
    def __init__(self, seq=0, ts=0):
        self.asks = {}
        self.bids = {}
        self.seq = seq
        self.ts = ts

    def apply(self, increment, check_seq=True):
        if check_seq and self.seq > 0 and self.seq + 1 != increment.seq:
            raise RuntimeError('Invalid seq sequence (expected {}, got {})'.format(self.seq + 1, increment.seq))

        for inc in increment.incs:
            if inc[0] == 'a':
                d = self.asks
            elif inc[0] == 'b':
                d = self.bids
            else:
                raise RuntimeError('Got unknown type: inc[0] = {}'.format(inc[0]))

            if inc[2] == 0:
                del d[inc[1]]
            else:
                d[inc[1]] = inc[2]
        self.seq = increment.seq
        self.ts = increment.ts

    def clone(self):
        ret = orderbook_t()
        ret.asks = self.asks.copy()
        ret.bids = self.bids.copy()
        ret.seq = self.seq
        ret.ts = self.ts
        return ret


    def diff(self, new, check_seq=True):
        """
        Считает разницу между двумя оредрбуками. Возвращает inc_t если разница есть, иначе None
        """
        if check_seq and self.seq > 0 and self.seq + 1 != new.seq:
            raise RuntimeError('Invalid seq sequence (expected {}, got {})'.format(self.seq + 1, new.seq))

        inc = inc_t(seq=new.seq, ts=new.ts)
        for action in ['asks', 'bids']:
            diff = {}
            _was = set(getattr(self, action).items())
            _new = set(getattr(new, action).items())

            diff.update({rate: 0 for rate, _ in (_was - _new)})
            diff.update({rate: amount for rate, amount in (_new - _was)})

            for rate, amount in diff.items():
                inc.incs.append([action[0], rate, amount])

        if inc.is_empty():
            return None
        else:
            return inc


    def to_dict(self, topn=None, cum=False):
        """
        Возвращает копию данных ордер бука в виде словаря, asks и bids отсортированы, от них взят topn.
            cum - если True, то возвращаются словари asks и bids, в которых amount каждого rate
                  равен всем предыдущим amount'ам.
        """
        ret = { 'seq': self.seq,
                'ts': self.ts,
              }
        # TODO: можно применить partial sort
        asks = sorted(self.asks.items(), key=lambda x: x[0])[:topn]
        bids = sorted(self.bids.items(), key=lambda x: x[0], reverse=True)[:topn]
        if cum:
            for i in range(1, len(asks)):
                asks[i] = (asks[i][0], asks[i][1] + asks[i-1][1])
            for i in range(1, len(bids)):
                bids[i] = (bids[i][0], bids[i][1] + bids[i-1][1])
        ret['asks'] = asks
        ret['bids'] = bids
        return ret

    @staticmethod
    def from_json(data, precision=8):
        d = json.loads(data)
        ob = orderbook_t()
        ob.ts = d.get('ts', ob.ts)
        ob.seq = d.get('seq', ob.seq)
        ob.asks = {strnum2int(x[0], precision=precision): strnum2int(x[1], precision=precision) for x in d['asks']}
        ob.bids = {strnum2int(x[0], precision=precision): strnum2int(x[1], precision=precision) for x in d['bids']}
        return ob

    def __str__(self):
        return '{} asks rates, '\
               '{} bids rates, seq = {}, ts = {}, datetime = {}'\
               .format(len(self.asks), len(self.bids), self.seq, self.ts, ts_to_datetime_str(self.ts))

    def is_empty(self):
        return not (len(self.asks) or len(self.bids))

    def compress(self, precision=8):
        """
        Кодирует asks и bids словари в бинарный формат для сжатого хранения.
        1. Сортируем rate по возрастанию.
        2. Пишем в бинарном формате разницу между предыдущим и следующим rate'ом в формате varint
        3. Пишем amount в varint формате

        Данный подход дает сжатие ~4-5 раз по сравнению с json форматом.
        """
        compression_version = 0
        encoded_bytes = varint_encode(compression_version)
        encoded_bytes += varint_encode(precision) # precision_rate
        encoded_bytes += varint_encode(precision) # precision_amount
        for action in ['asks', 'bids']:
            a = self.__dict__[action]
            encoded_bytes += varint_encode(len(a))
            last_rate = 0
            for rate, amount in sorted(a.items(), key=lambda x: x[0]):
                encoded_bytes += varint_encode(rate - last_rate)
                encoded_bytes += varint_encode(amount)
                last_rate = rate
        return encoded_bytes

    def decompress(self, buf, pos=0):
        compression_version, pos = varint_decode(buf, pos)
        if compression_version == 0:
            asks = {}
            bids = {}
            precision_rate, pos = varint_decode(buf, pos)
            precision_amount, pos = varint_decode(buf, pos)
            for d in [asks, bids]:
                number_of_elements, pos = varint_decode(buf, pos)
                last_rate = 0
                for _ in range(number_of_elements):
                    val, pos = varint_decode(buf, pos)
                    rate = last_rate + val
                    last_rate = rate
                    amount, pos = varint_decode(buf, pos)
                    d[rate] = amount
            self.asks = asks
            self.bids = bids
        else:
            raise RuntimeError('Unknown version to decompress {}'.format(compression_version))

    def is_equal(self, ob2):
        """
        Сравнивает ордербуки между собой. Возвращает True, если они равны
        """
        is_equal = True
        is_equal |= self.ts == ob2.ts
        is_equal |= self.seq == ob2.seq
        is_equal |= is_dicts_equal(self.asks, ob2.asks)
        is_equal |= is_dicts_equal(self.bids, ob2.bids)
        return is_equal
