import logging
import random
import json
import requests
import websocket
import gzip

import orderbookd.db as db
from .common_loader import common_loader_t
from .util import timestamp_now, float2int
from .order_book import inc_t, orderbook_t


def supported_pairs(precision=None):

    """
    Получить через API биржи все поддерживаемые валютные пары
    """
    r = requests.get('https://api.huobi.pro/v1/common/symbols')
    pairs = []
    for pair_data in r.json()['data']:
        base_currency = pair_data['base-currency']
        quote_currency = pair_data['quote-currency']
        symbol = pair_data['symbol']
        assert symbol == base_currency + quote_currency
        pairs.append(f'{base_currency.upper()}_{quote_currency.upper()}')
        
        if precision:
            # если определен precision, то проверяем, что все пары имеет дочность, вписывающуюся в precision
            assert pair_data['price-precision'] <= precision
            assert pair_data['amount-precision'] <= precision

    return pairs


def subset_of_supported_pairs(pairs):
    all_supported_pairs = set([p.upper() for p in supported_pairs()])
    subset_supported_pairs = []
    for pair in pairs:
        if pair in all_supported_pairs:
            subset_supported_pairs.append(pair)
        else:
            reverse_pair = '_'.join(pair.split('_')[::-1])
            if reverse_pair in all_supported_pairs:
                subset_supported_pairs.append(reverse_pair)

    return subset_supported_pairs


def parse_orderbook(data, orderbook, ts, precision=8):
    """
    Формат данных:
    ```
        {"lastUpdateId":271871263,
         "bids":[["0.06187800","4.58500000",[]],["0.06187700","0.58200000",[]],..],
         "asks":[["0.06190400","7.49400000",[]],...]
        }
    ```
    {"ch":"market.btcusdt.depth.step5",
     "ts":1532931344100,
     "tick":{"bids":[[8182.000000000000000000,0.090500000000000000], ...],
     "asks":[[8182.500000000000000000,0.050000000000000000], ...],
     "ts":1532931344030,
     "version":14220521319}
    }
    """
    orderbook.asks = {float2int(x[0], precision=precision): float2int(x[1], precision=precision) for x in data['tick']['asks']}
    orderbook.bids = {float2int(x[0], precision=precision): float2int(x[1], precision=precision) for x in data['tick']['bids']}
    orderbook.ts = ts
    orderbook.seq = 0  # т.к. binance имеет слолжны механизм seq'ок, то мы используем свой


class loader_t(common_loader_t):
    def __init__(self, dumper_queue, orderbooks_holder, prices_holder, ws_queue, config):
        super(loader_t, self).__init__( exchange='huobi',
                                        dumper_queue=dumper_queue,
                                        orderbooks_holder=orderbooks_holder,
                                        prices_holder=prices_holder,
                                        ws_queue=ws_queue,
                                        config=config,
                                        logger=logging.getLogger('huobi_loader'),
                                      )
        self.pairs = subset_of_supported_pairs(config[self.exchange]['pairs'])
        if len(self.pairs) != len(config[self.exchange]['pairs']):
            self.logger.error('Supported {} of {} pairs'.format(len(self.pairs), len(config[self.exchange]['pairs'])))
            self.logger.error('{}'.format(','.join(self.pairs)))
            raise RuntimeError('Unsupported pairs')
        self.seq_counters = self.get_last_seqs()
        self.stream2pair = {}

    def get_last_seqs(self):
        """
        Т.к. gateio не поддерживает seq, то нам нужно поддерживать его исскуственно. Для этого нам нужно
        получать из базы последний записанный seq
        """
        if self.config['db']['use']:
            conn = db.get_connection(self.config['db'])
            ret = db.select_last_seq_increments(conn, self.exchange, self.pairs)
            conn.close()
        else:
            self.logger.warning('db.use = False, last seqs set to 0')
            ret = {p: 0 for p in self.pairs}
        return ret

    def serve_connetion(self):
        self.stream2pair = {}
        self.ws = websocket.WebSocketApp( "wss://api.huobipro.com/ws",
                                          on_message = self.ws_on_message,
                                          on_error = self.ws_on_error,
                                          on_close = self.ws_on_close
                                        )
        self.ws.on_open = self.ws_on_open
        if 'proxies' in self.config and len(self.config['proxies']) > 0:
            # TODO: сейчас прокси выбирается случайно, но нужен более продвинутый алгоритм,
            # который бы учитывал сбои на прокси и их нагруженность
            proxy = random.choice(self.config['proxies'])
            self.ws.run_forever( http_proxy_host=proxy['host'],
                                    http_proxy_port=proxy['port'],
                                    http_proxy_auth=(proxy['login'], proxy['password']),
                                )
        else:
            self.ws.run_forever()

    def stop_connection(self):
        if hasattr(self, 'ws'):
            self.ws.close()

    def ws_on_open(self, ws):
        self.logger.info('ws_on_open')
        step ='step0'
        self.logger.info(f'Subscribe for {self.pairs} with step={step}')
        for pair in self.pairs:
            pair_lower = pair.replace('_', '').lower()
            d = dict( sub=f'market.{pair_lower}.depth.{step}',
                      id='id1',
                    )
            self.stream2pair[d['sub']] = pair
            self.ws.send(json.dumps(d))

    def ws_on_error(self, ws, error):
        self.logger.error('ERROR: {}'.format(error))
        self.ws.close()

    def ws_on_close(self, ws):
        self.logger.info('ws_on_close')

    def ws_on_message(self, ws, message):
        try:
            self.process_message(message)
        except Exception:
            self.logger.exception('Exception while processing message:')
            self.ws.close()

    def process_message(self, message):
        ts = timestamp_now()
        msg = json.loads(gzip.decompress(message).decode('utf-8'))
        # self.logger.info(f'got: {msg}')
        if 'subbed' in msg:
            # пришло сообщение об удачной подписке, пропускаем
            return
        if 'ping' in msg:
            # Иногда будут прилетатть сообщения вида:
            # 2018-07-30 09:13:46,469 - huobi.io - INFO - {"ping":1532931226318}
            self.ws.send(json.dumps({'pong': msg['ping']}))
            return

        stream = msg['ch']
        if stream not in self.stream2pair:
            raise RuntimeError(f'Unknown stream {stream}')
        pair = self.stream2pair[stream]
        def extract(ob):
            parse_orderbook(msg, ob, ts, precision=self.config['precision'])
            ob.seq = self.seq_counters[pair]
        if pair not in self.last_dump_period:
            # пришел полный ордербук в первый раз (с учетом лимита, конечно)
            # в случае первой загруги ордербука предполагаем, что мы пропустили ряд обновлений,
            # поэтому пропускаем 2 seq (символически, чтобы потом найти дыру в данных)
            self.seq_counters[pair] += 2
            self.process_intial_orderbook(pair, extract)
        else:
            # промежуточная загрузка полного дампа, делаем из него инкремент и обрабатываем как
            # обычно
            self.seq_counters[pair] += 1
            ob_new = orderbook_t()
            extract(ob_new)
            with self.orderbooks_holder.get((self.exchange, pair)) as orderbook:
                increment = orderbook.diff(ob_new, check_seq=True)
            if increment is None:
                # Бинанс присылает данные каждую секунду. Даже если там не было изменений.
                # Мы просто делаем вид, что ничего не приходило
                self.seq_counters[pair] -= 1
                # self.logger.warning(f'Got intermidiate full orderbook for {pair}, but increment is empty')
                return
            else:
                self.process_increment(pair, increment)