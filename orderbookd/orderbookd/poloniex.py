import logging
import random
import json
import websocket

from .common_loader import common_loader_t
from .order_book import inc_t
from .util import strnum2int, timestamp_now

def parse_orderbook(data, orderbook, seq, ts, precision=8):
    orderbook.asks = {strnum2int(x[0], precision=precision): strnum2int(x[1], precision=precision) for x in data[0].items()}
    orderbook.bids = {strnum2int(x[0], precision=precision): strnum2int(x[1], precision=precision) for x in data[1].items()}
    orderbook.ts = ts
    orderbook.seq = seq

def parse_increment(data, seq, ts, precision=8):
    increment = inc_t(seq, ts)
    for event in (event for event in data if event[0] == 'o'):
        if event[-3] == 0:
            action = 'a'
        elif event[-3] == 1:
            action = 'b'
        increment.incs.append([ action,
                                strnum2int(event[-2], precision=precision),
                                strnum2int(event[-1], precision=precision)
                              ])
    return increment


class pair_loader_t(common_loader_t):
    def __init__(self, pair, dumper_queue, orderbooks_holder, prices_holder, ws_queue, config):
        super(pair_loader_t, self).__init__( exchange='poloniex',
                                             dumper_queue=dumper_queue,
                                             orderbooks_holder=orderbooks_holder,
                                             prices_holder=prices_holder,
                                             ws_queue=ws_queue,
                                             config=config,
                                             logger=logging.getLogger('poloniex_loader.{}'.format(pair)),
                                           )
        self.pair = pair

    def serve_connetion(self):
        self.ws = websocket.WebSocketApp( "wss://api2.poloniex.com/",
                                            on_message = self.ws_on_message,
                                            on_error = self.ws_on_error,
                                            on_close = self.ws_on_close
                                        )
        self.ws.on_open = self.ws_on_open
        if 'proxies' in self.config and len(self.config['proxies']) > 0:
            # TODO: сейчас прокси выбирается случайно, но нужен более продвинутый алгоритм,
            # который бы учитывал сбои на прокси и их нагруженность
            proxy = random.choice(self.config['proxies'])
            self.ws.run_forever( http_proxy_host=proxy['host'],
                                 http_proxy_port=proxy['port'],
                                 http_proxy_auth=(proxy['login'], proxy['password']),
                               )
        else:
            self.ws.run_forever()

    def stop_connection(self):
        if hasattr(self, 'ws'):
            self.ws.close()

    def ws_on_open(self, ws):
        self.logger.info('ws_on_open')
        self.ws.send(json.dumps({'command': 'subscribe', 'channel': self.pair}))

    def ws_on_error(self, ws, error):
        self.logger.error('ERROR: {}'.format(error))
        self.ws.close()

    def ws_on_close(self, ws):
        self.logger.info('ws_on_close')

    def ws_on_message(self, ws, message):
        try:
            self.process_message(message)
        except Exception:
            self.logger.exception('Exception while processing message:')
            self.ws.close()

    def process_message(self, message):
        ts = timestamp_now()
        msg = json.loads(message)
        msg_code = int(msg[0])
        if msg_code == 1010:
            return
        seq = int(msg[1])
        action = msg[2][0][0]
        if action == 'i':
            def extractor(ob):
                parse_orderbook(msg[2][0][1]['orderBook'], ob, seq, ts, precision=self.config['precision'])

            self.process_intial_orderbook(self.pair, extractor)
        else:
            increment = parse_increment(msg[2], seq, ts)
            if increment.is_empty():
                return
            self.process_increment(self.pair, increment)