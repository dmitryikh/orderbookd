# encoding: utf-8
# import asyncio
import logging
from threading import Thread
import json

from tornado.ioloop import IOLoop
from tornado.web import Application, RequestHandler, url
from tornado.websocket import WebSocketHandler

import orderbookd.db as db
from .util import datetime_str_to_ts, ts_to_datetime_str, timestamp_now, strnum2int, int2strnum, supported_exchanges, DATETIME_REGEXP
from .order_book import orderbook_t


class http_server_thread_t(Thread):
    def __init__(self, port):
        super(http_server_thread_t, self).__init__()
        self.port = port
        self.app = Application()
        self.logger = logging.getLogger('app.http')

    def add_handlers(self, urls):
        self.app.add_handlers(r'.*', urls)

    def run(self):
        self.logger.info('Start')
        # asyncio.set_event_loop(asyncio.new_event_loop())
        self.app.listen(self.port)
        self.ioloop = IOLoop.current()
        self.ioloop.start()
        self.logger.info('Stop')

    def stop(self):
        if self.is_alive():
            if not hasattr(self, 'ioloop'):
                raise RuntimeError("Can't stop thread: no event loop")
            self.ioloop.add_callback(self.ioloop.stop)


class daemon_handler_t(RequestHandler):
        def initialize(self, http):
            self.http = http

        def set_default_headers(self):
            self.set_header('Content-Type', 'text/plain; charset=UTF-8')

        def if_debug(self):
            if self.get_argument('debug', '0') in  ['1', 'true', 'True']:
                return True
            return False

        def parse_fmt(self):
            return self.get_argument('fmt', '')

        def parse_ts(self):
            ts = self.get_argument('ts', '')
            if not ts:
                raise ValueError('`ts` parameter is not provided')
            is_now = (ts == 'now')
            if not is_now:
                try:
                    ts = float(ts)
                except ValueError:
                    try:
                        ts = datetime_str_to_ts(ts)
                    except ValueError:
                        raise ValueError('`ts` parameter should be "now" or timestamp ("1527360539.79") or datetime ("2018-05-25_17:21:00.0")')
            return ts, is_now

        def int2strnum(self, v):
            return int2strnum(v, precision_decode=self.http.config['precision'], precision=self.http.config['precision'])

        def parse_exchange(self):
            exchange = self.get_argument('exchange', '')
            if not exchange:
                raise ValueError('`exchange` parameter is not provided')
            if exchange not in supported_exchanges:
                raise ValueError('supported exhange: {}'.format(','.join(supported_exchanges)))
            return exchange


class daemon_ws_handler_t(WebSocketHandler):
        def initialize(self, http):
            self.http = http


class daemon_http_server_thread_t(http_server_thread_t):
    class time_handler(daemon_handler_t):
        def get(self):
            ret = {'error': '', 'result': ts_to_datetime_str(timestamp_now())}
            self.write(ret)

    class orderbook_handler(daemon_handler_t):
        def get_orderbook(self, exchange, pair, ts):
            if ts == 'now':
                with self.http.orderbooks_holder.get((exchange, pair)) as orderbook:
                    ob = orderbook.clone()
            else:
                conn = self.http.get_db_connection()
                ob = db.select_orderbook( conn,
                                          exchange,
                                          pair,
                                          ts,
                                          self.http.config['period'],
                                          self.http.config['precision']
                                        )
            return ob

        def process(self):
            exchange = self.parse_exchange()
            pair = self.get_argument('pair', '')
            if not pair:
                raise ValueError('pair parameter is not provided')
            if not self.http.orderbooks_holder.is_exist((exchange, pair)):
                raise RuntimeError(f"Unknown pair '{pair}' (exchange = {exchange})")
            topn = self.get_argument('topn', '')
            topn = int(topn) if topn else None

            ts, is_now = self.parse_ts()
            ob = self.get_orderbook(exchange, pair, ts)

            order_book_dict = ob.to_dict(topn=topn)
            order_book_dict['datetime'] = ts_to_datetime_str(order_book_dict['ts'])
            if not self.if_debug():
                if self.parse_fmt() == 'num':
                    order_book_dict['asks'] = [(x[0], x[1]) for x in order_book_dict['asks']]
                    order_book_dict['bids'] = [(x[0], x[1]) for x in order_book_dict['bids']]
                else:
                    order_book_dict['asks'] = [(self.int2strnum(x[0]), self.int2strnum(x[1])) for x in order_book_dict['asks']]
                    order_book_dict['bids'] = [(self.int2strnum(x[0]), self.int2strnum(x[1])) for x in order_book_dict['bids']]
            else:
                # В debug режиме выводим только кол-во rates, без значений
                order_book_dict['asks_len'] = len(order_book_dict['asks'])
                del order_book_dict['asks']
                order_book_dict['bids_len'] = len(order_book_dict['bids'])
                del order_book_dict['bids']
            return order_book_dict

        def get(self):
            ret = {}
            try:
                ret['result'] = self.process()
                ret['error'] = ''
            except Exception as e:
                ret['error'] = str(e)
                self.http.logger.exception('')
                self.set_status(500)
            self.write(ret)

    class stat_handler(daemon_handler_t):
        def get_first_minute(self, exchange, pair):
            cur = self.http.get_db_connection().cursor()
            cur.execute(f'SELECT minute FROM {exchange}_dump_{pair} ORDER BY minute LIMIT 1')
            ret = cur.fetchone()
            if ret:
                first_minute = ret[0]
            else:
                first_minute = None

            cur.close()
            return first_minute

        def get(self):
            ret = ''
            for exchange, pair in self.http.orderbooks_holder.keys():
                ts = self.get_first_minute(exchange, pair)
                with self.http.orderbooks_holder.get((exchange, pair)) as orderbook:
                    ret += f'OrderBook {exchange}-{pair}: {orderbook}'
                if ts:
                    ret += ' first_ts = {}, first_datetime = {}\n'.format(ts, ts_to_datetime_str(ts))
                else:
                    ret += ' DATA NOT AVAILABLE\n'

            self.write(ret)

    class prices_handler(daemon_handler_t):
        def parse_currencies(self, exchange):
            """
            currencies=BTC,ETH,XMR - валюты, для которых отдаем информацию
            """
            all_pairs = [pair for exch, pair in self.http.orderbooks_holder.keys() if exch == exchange]
            all_currencies = set((cur for pair_cur in all_pairs for cur in pair_cur.split('_')))
            currencies = self.get_argument('currencies', '')
            if currencies:
                currencies = currencies.split(',')
                for currency in currencies:
                    if currency not in all_currencies:
                        raise ValueError("Unknown currency '{currency}' (exchange = {exchange})")
            else:
                currencies = all_currencies
            currencies_set = set(currencies)
            needed_pairs = []
            for pair in all_pairs:
                c1, c2 = pair.split('_')
                if c1 in currencies_set and c2 in currencies_set:
                    # в случаей, когда обе пара представлена валютами, которые нас интересуют
                    needed_pairs.append(pair)
            if not needed_pairs:
                raise RuntimeError('No pairs for currencies: {} (exchange = {})'.format(','.join(currencies), exchange))

            return currencies, needed_pairs

        def parse_currencies_amounts(self, needed_pairs):
            currencies_amounts = {}
            for pair in needed_pairs:
                # Рассматриваем только лимиты на валюту, которой торгуем (вторая часть пары)
                currency = pair.split('_')[1]
                amount = self.get_argument(currency + '_amount', '')
                if amount:
                    try:
                        amount = strnum2int(amount, precision=self.http.config['precision'])
                    except ValueError:
                        raise ValueError('{}_amount should be float number'.format(currency))
                    currencies_amounts[currency] = amount
            return currencies_amounts

        def load_order_books(self, exchange, pairs, ts, job):
            """
            Параллельно загружаем order book'и для пар из списка `pairs`, актуальные на момент
            времени `ts`. Если `ts` == "now", то берутся order books, актуальные на данный момент
            """
            def order_book_from_db_thread(pair, ts):
                con = None
                try:
                    con = db.get_connection(self.http.config['db'])
                    orderbook = db.select_orderbook( con,
                                                     exchange,
                                                     pair,
                                                     ts,
                                                     self.http.config['period'],
                                                     self.http.config['precision']
                                                   )
                except Exception as e:
                    orderbook = str(e)
                finally:
                    if con:
                        con.close()
                job(pair, orderbook)

            if ts == 'now':
                for pair in pairs:
                    try:
                        with self.http.orderbooks_holder.get((exchange, pair)) as orderbook:
                            if orderbook.is_empty():
                                raise ValueError()
                            try:
                                job(pair, orderbook)
                            except Exception:
                                pass
                    except (KeyError, ValueError):
                        job(pair, 'No data available yet')
            else:
                threads = []
                for pair in pairs:
                    th = Thread(target=order_book_from_db_thread, args=(pair, ts))
                    th.start()
                    threads.append(th)

                for th in threads:
                    th.join()

        def get(self):
            ret = {}
            result = {}
            try:
                ts, is_now = self.parse_ts()
                exchange = self.parse_exchange()
                currencies, needed_pairs = self.parse_currencies(exchange)
                currencies_amounts = self.parse_currencies_amounts(needed_pairs)
                if len(currencies_amounts) == 0 and is_now:
                    # пользуемся prices_holder'ом для запроса без фильтра и на текущий момент
                    for pair in needed_pairs:
                        result[pair] = {}
                        highest_bid = {}
                        lowest_ask = {}
                        with self.http.prices_holder.get((exchange, pair)) as price_processor:
                            price = price_processor.get_price()
                            result[pair]['highestBid'] = {'rate': price.bid_rate, 'amount': price.bid_amount}
                            result[pair]['lowestAsk'] = {'rate': price.ask_rate, 'amount': price.ask_amount}
                            result[pair]['seq'] = price.seq
                            result[pair]['ts'] = price.ts
                            result[pair]['datetime'] = ts_to_datetime_str(price.ts)
                            result[pair]['status'] = 'ok'
                else:
                    def job(pair, orderbook):
                        result[pair] = {}
                        if isinstance(orderbook, orderbook_t):
                            d = orderbook.to_dict(cum=True)
                            highest_bid = {}
                            lowest_ask = {}
                            currency = pair.split('_')[1]
                            amount_threshold = 0
                            if currency in currencies_amounts:
                                # есть лимит на минимальное кол-во
                                amount_threshold = currencies_amounts[currency]

                            for dest, action_name in [(highest_bid, 'bids'), (lowest_ask, 'asks')]:
                                item = next((item for item in d[action_name] if item[1] >= amount_threshold), None)
                                if item:
                                    dest['rate'] = self.int2strnum(item[0])
                                    dest['amount'] = self.int2strnum(item[1])
                                else:
                                    dest['rate'] = self.int2strnum(0)
                                    dest['amount'] = self.int2strnum(0)

                            result[pair]['highestBid'] = highest_bid
                            result[pair]['lowestAsk'] = lowest_ask
                            result[pair]['seq'] = d['seq']
                            result[pair]['ts'] = d['ts']
                            result[pair]['datetime'] = ts_to_datetime_str(d['ts'])
                            result[pair]['status'] = 'ok'
                        else:
                            result[pair]['status'] = orderbook

                    self.load_order_books(exchange, needed_pairs, ts, job)
                ret['result'] = result
                ret['error'] = ''
            except Exception as e:
                ret['error'] = str(e)
                self.http.logger.exception('')
                self.set_status(500)
            self.write(ret)

    class prices_updates(daemon_ws_handler_t):
        def open(self):
            self.set_nodelay(True)
            self.http.logger.info("Get new websocket connection from: '{}'".format(repr(self.request)))
            self.reg()

        def on_message(self, message):
            pass
            # self.write_message("Just listen!:)")

        def reg(self):
            msg = []
            for exchange, pair in self.http.prices_holder.keys():
                with self.http.prices_holder.get((exchange, pair)) as price:
                    msg.append(['price', price.get_price().to_dict(exchange, pair)])
            self.write_message('{}'.format(json.dumps(msg)))
            self.id = self.http.ws_queue.add_action(lambda msg: self.write_message('{}'.format(msg)))

        def unreg(self):
            self.http.ws_queue.del_action(self.id)

        def on_close(self):
            self.unreg()
            self.http.logger.info("Close websocket connection from: '{}'".format(repr(self.request)))

    def __init__(self, orderbooks_holder, prices_holder, ws_queue, config):
        super(daemon_http_server_thread_t, self).__init__(config['http']['port'])
        self.config = config
        self.db_conn = None
        self.orderbooks_holder = orderbooks_holder
        self.prices_holder = prices_holder
        self.ws_queue = ws_queue
        self.add_handlers([ url(r'/admin/time', self.time_handler, dict(http=self)),
                            url(r'/orderbook', self.orderbook_handler, dict(http=self)),
                            url(r'/prices', self.prices_handler, dict(http=self)),
                            url(r'/admin/stat', self.stat_handler, dict(http=self)),
                            url(r'/prices/ws', self.prices_updates, dict(http=self)),
                          ])

    def get_db_connection(self):
        if not self.config['db']['use']:
            return None
        try:
            # Если удалось заполучить курсор, значит соедение есть
            _ = self.db_conn.cursor()
        except (db.OperationalError, AttributeError) as e:
            self.logger.warning('Got db.OperationalError: {}'.format(str(e)))
            self.logger.info('Trying to reconnect')
            self.db_conn = db.get_connection(self.config['db'])
        return self.db_conn
