# encoding: utf-8
from datetime import datetime
import pytz
import six
from threading import Thread, Lock
import queue
import logging

DATETIME_FORMAT = '%Y-%m-%d_%H:%M:%S.%f'
DATETIME_REGEXP = r'\d{4}-\d{2}-\d{2}_\d{2}:\d{2}:[0-9.]+'

supported_exchanges = ['poloniex', 'gateio', 'binance', 'livecoin', 'huobi']

def ts2minute(ts):
    return int(ts / 60) * 60

def ts2period(ts, period, next=False):
    return int(ts / period) * period + (period if next else 0)

def timestamp_now():
    return datetime_to_ts(datetime.utcnow().replace(tzinfo=pytz.utc))

def datetime_to_ts(dt):
    return (dt - datetime(1970, 1, 1, tzinfo=pytz.utc)).total_seconds()

def datetime_str_to_ts(dt_str):
    dt = datetime.strptime(dt_str, DATETIME_FORMAT).replace(tzinfo=pytz.utc)
    return datetime_to_ts(dt)

def ts_to_datetime_str(ts):
    return datetime.utcfromtimestamp(ts).strftime(DATETIME_FORMAT)

def cur_format(f):
    """
    Преобразует float в формат с 8-ю знаками после запятой
    """
    return '{:.8f}'.format(f)

def accumu(array):
    """
    Считает куммулятивную сумму массива array, результат в виде генератора
    """
    total = 0
    for x in array:
        total += x
        yield total

def strnum2int(s, precision=8):
    """
    Преобразует строкове представление rate или amount в целочисленное значение, сдвигая дробную
    часть на precision разрядов вправо. Таким образом получаем точное (int) представление float числа.
    """
    tokens = s.split('.')
    assert len(tokens) == 1 or len(tokens) == 2
    ret = int(tokens[0]) * 10**precision
    if len(tokens) == 2:
        # есть десятичная часть
        if len(tokens[1]) > precision:
            raise ValueError('{} has precision more than {}'.format(s, precision))
        ret += int(tokens[1]) * 10**(precision - len(tokens[1]))
    return ret


def strnum_truncate(s, precision):
    """
    Обрезаем десятичную часть до <= `precision` знаков после запятой
    """
    tokens = s.split('.')
    if len(tokens) == 2 and len(tokens[1]) > precision:
        return tokens[0] + '.' + tokens[1][:precision]
    else:
        return s


def int2strnum(v, precision_decode, precision=8):
    """
    Преобразуем целочисленное значение в вещественное число, сдвигая int на precision_decode
    разрядов влево за запятую. Число в текстовом виде будет иметь точно precision знаков после
    запятой.
    """
    assert precision > 0
    shift = 10**precision_decode
    n = v // shift
    d = str(v % shift)
    d = '0'*max(0, precision_decode - len(d)) + d
    d = d + '0'*max(0, precision - len(d))
    return str(n) + '.' + d[:precision]


def int2float(v, precision_decode):
    return v / 10**precision_decode


def float2int(v, precision):
    return int(v * 10**precision)


def strnum_dec_len(s):
    """
    Для строкового представления числа возвращает кол-во значящих разрядов после запятой.
    """
    tokens = s.split('.')
    if len(tokens) == 1:
        return 0
    else:
        return len(tokens[1].rstrip('0'))


def varint_encode(value):
    """
    Кодирует int в varint бинарный формат
    """
    if value < 0:
        raise ValueError('Int should be >= 0')
    buf = b''
    bits = value & 0x7f
    value >>= 7
    while value:
        buf += six.int2byte(0x80|bits)
        bits = value & 0x7f
        value >>= 7
    buf += six.int2byte(bits)
    return buf


def varint_decode(buffer, pos):
    """
    Декодирует бинарное представление в int
    """
    result = 0
    shift = 0
    while 1:
        b = six.indexbytes(buffer, pos)
        result |= ((b & 0x7f) << shift)
        pos += 1
        if not (b & 0x80):
          result = int(result)
          return (result, pos)
        shift += 7

def is_dicts_equal(d1, d2):
    return len(set(d1.items()) & set(d2.items())) == len(d1)


class concurrent_map_t(object):
    """
    Мапка, которая отдает хранимые значения только под мьютексом. Само наполнение мапки, метод
    `reg`, не потокобезопасный
    """
    class entity_context_t(object):
        def __init__(self, obj, lock):
            self.lock = lock
            self.obj = obj

        def __enter__(self):
            self.lock.acquire()
            return self.obj

        def __exit__(self, *args):
            self.lock.release()

    def __init__(self):
        self._objects = {}  # значения - пара obj и lock

    def reg(self, key, obj):
        """
        obj должен быть mutable
        """
        self._objects[key] = (obj, Lock())

    def update(self, key, obj):
        old, lock = self._objects[key]
        with lock:
            self._objects[key] = (obj, lock)

    def reg_or_update(self, key, obj):
        if key in self._objects:
            self.update(key, obj)
        else:
            self.reg(key, obj)

    def get(self, key):
        ret = self._objects.get(key)
        if ret is None:
            raise KeyError("No object for key '{}'".format(key))
        return self.entity_context_t(*ret)

    def is_exist(self, key):
        return key in self._objects

    def keys(self):
        return self._objects.keys()


class queue_with_action_t(Thread):
    """
    Очередь, которая для каждого полученного сообщения вызывает функцию из self.actions
    """
    def __init__(self, name=None):
        super(queue_with_action_t, self).__init__()
        self.actions = {}
        self.data_lock = Lock()  # лок на операции с self.actions
        self.exe_lock = Lock()  # лок на выполнение actions
        self.q = queue.Queue()
        self.logger = logging.getLogger('app.' + (name if name else self.__class__.__name__))
        self.stop_flag = False

    def get_queue(self):
        return self.q

    def stop(self):
        self.stop_flag = True

    def put(self, *args, **kwargs):
        self.q.put(*args, **kwargs)

    def run(self):
        self.logger.info('Start')
        self.stop_flag = False
        while not self.stop_flag:
            try:
                val = self.q.get(True, 1.0)
            except queue.Empty:
                    continue
            with self.exe_lock:
                with self.data_lock:
                    actions_copy = list(self.actions.values())
                for action in actions_copy:
                    try:
                        action(val)
                    except Exception:
                        self.logger.exception('Error while applying function {} on value {}'.format(action, val))
        self.logger.info('Stop')

    def has_actions(self):
        with self.data_lock:
            return bool(self.actions)

    def add_action(self, action):
        with self.data_lock:
            next_id = (max(self.actions.keys()) + 1) if self.actions else 0
            self.actions[next_id] = action
        return next_id


    def del_action(self, id):
        """
        Должны блокироваться при удалении action, если он выполняется
        """
        is_found = False
        with self.exe_lock:
            with self.data_lock:
                if id in self.actions:
                    is_found = True
                    del self.actions[id]
        if not is_found:
            self.logger.error("Can't find action with id = {}".format(id))
