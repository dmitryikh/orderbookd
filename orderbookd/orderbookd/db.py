import mysql.connector as mysql
from mysql.connector.errors import OperationalError
import json
from .util import int2strnum, strnum2int, ts2period
from .order_book import orderbook_t, inc_t


def get_connection(config):
    con = mysql.connect( user=config['user'],
                         password=config['password'],
                         host=config['host'],
                         port=config['port'],
                         database=config['database'],
                       )
    con.autocommit = True
    return con


def iter_rows_batch(cursor, size=10):
    while True:
        rows = cursor.fetchmany(size)
        if not rows:
            break
        for row in rows:
            yield row


def insert_increment(conn, exchange, pair, increment, precision=8):
    cur = conn.cursor()
    query = f'INSERT INTO {exchange}_inc_{pair} (`seq`, `ts`, `inc`) VALUES (%s, %s, %s)'

    vals = [increment.seq, str(increment.ts), json.dumps([[ 0 if inc[0] == 'a' else 1,
                                                            int2strnum(inc[1], precision_decode=precision, precision=precision),
                                                            int2strnum(inc[2], precision_decode=precision, precision=precision),
                                                          ] for inc in increment.incs])]
    cur.execute(query, vals)

    conn.commit()


def select_increments(conn, exchange, pair, seq_start, seq_end=None, precision=8):
    cur = conn.cursor()
    query = f'SELECT seq, ts, inc FROM {exchange}_inc_{pair} WHERE seq > {seq_start}'
    if seq_end:
        query += f' AND seq <= {seq_end}'
    query += ' ORDER BY seq'
    increments = []
    cur.execute(query)
    for row in cur.fetchall():
        increment = inc_t(row[0], row[1])
        increment.incs = [[ 'a' if inc[0] == 0 else 'b',
                            strnum2int(inc[1], precision=8),
                            strnum2int(inc[2], precision=8),
                          ] for inc in json.loads(row[2])]
        increments.append(increment)
    return increments


def insert_orderbook(conn, exchange, pair, orderbook, period, precision=8):
    cur = conn.cursor()
    query = f'INSERT INTO {exchange}_dump_{pair} (`minute`,`ts`,`seq`, `dump`) VALUES (%s, %s, %s, %s)'
    vals = [ ts2period(orderbook.ts, period, next=True),
             str(orderbook.ts),
             orderbook.seq,
             orderbook.compress(precision)
           ]
    cur.execute(query, vals)
    conn.commit()


def select_orderbook(conn, exchange, pair, ts, period, precision=8):
    cur = conn.cursor()
    base_period = ts2period(ts, period)
    if ts == base_period:
        # запрошен orderbook, полный дамп которого есть в базе
        query = f'SELECT `ts`,`seq`, `dump` FROM {exchange}_dump_{pair} WHERE minute = %s'
        cur.execute(query, (base_period, ))
        row = cur.fetchone()
        if not row:
            raise RuntimeError('No data for ts = {}'.format(ts))

        orderbook = orderbook_t()
        orderbook.ts = row[0]
        orderbook.seq = row[1]
        orderbook.decompress(row[2])

        return orderbook

    else:
        next_period = ts2period(ts, period, next=True)
        # self.logger.info('Request for ts = {}, datetime = {}'.format(ts, ts_to_datetime_str(ts)))
        cur.execute(f'SELECT minute, ts, seq FROM {exchange}_dump_{pair} WHERE minute IN '
                    f'({base_period}, {next_period}) ORDER BY minute')
        rows = cur.fetchall()
        # self.logger.info('Got {} results'.format(len(rows)))
        if len(rows) == 0:
            raise RuntimeError('No data for ts = {}'.format(ts))
        base_period_from_db, base_ts, base_seq = rows[0]
        assert base_period_from_db == base_period, 'Internal Error'

        seq_start = base_seq
        seq_end = None

        if len(rows) == 2:
            next_period_from_db, next_ts, next_seq = rows[1]
            assert next_period_from_db == next_period, 'Internal Error'
            seq_end = next_seq
        elif len(rows) > 2:
            raise RuntimeError('Expected <= 2 rows')
        else:
            # Возможно, это последние собранные данные
            cur.execute(f'SELECT max(minute) FROM {exchange}_dump_{pair}')
            max_period, = cur.fetchone()
            if max_period - base_period <= 2 * period:
                # запрашиваемые данные близки к последним собранным данным
                # поэтому пробуем загрузить все инкременты
                pass
            else:
                raise RuntimeError('No full data for minute {} (requested ts {})'.format(base_period, ts))

        orderbook = select_orderbook(conn, exchange, pair, base_period, period, precision)
        for inc in select_increments(conn, exchange, pair, seq_start, seq_end, precision):
            if inc.ts > ts:
                break
            orderbook.apply(inc, True)
        return orderbook


def select_last_seq_increments(conn, exchange, pairs):
    cur = conn.cursor()
    res = {}
    for pair in pairs:
        query = f'SELECT max(seq) FROM {exchange}_inc_{pair}'
        cur.execute(query)
        val = cur.fetchone()[0]
        res[pair] = val if val else 0

    return res


def is_tables_for_pair_exist(conn, exchange, pair, db):
    """
    Проверяет, есть ли нужные таблицы для валютной пары `pair` на бирже `exchange`
    """
    cur = conn.cursor()
    query = f"SELECT COUNT(*) FROM information_schema.tables "\
            f"WHERE table_schema = '{db}' AND (table_name = '{exchange}_inc_{pair}' "\
            f"OR table_name = '{exchange}_dump_{pair}')"
    cur.execute(query)
    res = cur.fetchone()[0]
    if res == 0:
        return False
    elif res == 2:
        return True
    else:
        # ожидаем, что либо все таблицы для пары есть, либо ни одной
        raise RuntimeError("Wrong result")


def create_tables_for_pair(conn, exchange, pair):
    cur = conn.cursor()
    query = """
CREATE TABLE {exch}_inc_{pair} (
    seq INT NOT NULL,
    ts DOUBLE NOT NULL,
    inc JSON NOT NULL,
    PRIMARY KEY(seq)
)""".format(exch=exchange, pair=pair)
    cur.execute(query)

    query = """
CREATE TABLE {exch}_dump_{pair} (
    minute INT NOT NULL,
    ts DOUBLE NOT NULL,
    seq INT NOT NULL,
    dump MEDIUMBLOB NOT NULL,
    PRIMARY KEY(minute)
)
ROW_FORMAT=COMPRESSED""".format(exch=exchange, pair=pair)
    cur.execute(query)
