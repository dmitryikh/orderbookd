from threading import Thread
import time
import json
import queue

from .util import ts2minute

class common_loader_t(Thread):
    """
    Класс, обобщающий чтение ордербуков и его дельт с разных бирж
    """
    def __init__(self, exchange, dumper_queue, orderbooks_holder, prices_holder, ws_queue, config, logger):
        super(common_loader_t, self).__init__()
        self.exchange = exchange
        self.orderbooks_holder = orderbooks_holder
        self.prices_holder = prices_holder
        self.config = config
        self.ws_queue = ws_queue
        self.logger = logger
        self.dumper_queue = dumper_queue
        self.last_dump_period = {}

    def run(self):
        self.logger.info('Start')
        self.stop_flag = False
        self.fail_counter = 0
        while not self.stop_flag:
            if self.fail_counter > 0:
                time.sleep(1.0)
            self.logger.info('WebSoket Start')
            self.last_dump_period = {}
            self.serve_connetion()
            self.logger.info('WebSoket Stop')
            self.fail_counter += 1
        self.logger.info('Stop')

    def serve_connetion(self):
        raise NotImplementedError

    def stop_connection(self):
        raise NotImplementedError

    def stop(self):
        self.stop_flag = True
        self.stop_connection()

    def process_intial_orderbook(self, pair, extractor):
        with self.orderbooks_holder.get((self.exchange, pair)) as orderbook:
            extractor(orderbook)
            seq = orderbook.seq
            ts = orderbook.ts
        self.last_dump_period[pair] = ts2minute(ts)
        with self.prices_holder.get((self.exchange, pair)) as price_processor:
            # TODO: надо бы запихнуть это в метод в price_processor_t
            price_processor.full_scan(action='asks')
            price_processor.full_scan(action='bids')
            price_processor._update_price()
            price = price_processor.get_price()
            price.ts = ts
            price.seq = seq
            if self.ws_queue.has_actions():
                msg = [['price', price.to_dict(self.exchange, pair)]]
                try:
                    self.ws_queue.put(json.dumps(msg), True, 0.1)
                except queue.Full:
                    self.logger.warn("Timeout on putting message to ws_queue")


    def process_increment(self, pair, increment):
        if increment.is_empty():
            raise RuntimeError('Inrement is empty!')
        if pair not in self.last_dump_period:
            raise RuntimeError(f'Got incement for {pair} but no initial orderbook was processed')

        try:
            self.dumper_queue.put((self.exchange, pair, increment), True, 0.1)
        except queue.Full:
            self.logger.warn('Timeout on putting increment to dumper_queue')
        with self.orderbooks_holder.get((self.exchange, pair)) as orderbook:
            if self.last_dump_period[pair] != 0 and self.last_dump_period[pair] != ts2minute(increment.ts):
                try:
                    self.logger.debug('Dumping {}: {}'.format(pair, orderbook))
                    self.dumper_queue.put((self.exchange, pair, orderbook.clone()), True, 0.1)
                except queue.Full:
                    self.logger.warn('Timeout on putting dump to dumper_queue')
            try:
                orderbook.apply(increment)
                self.last_dump_period[pair] = ts2minute(orderbook.ts)
            except Exception:
                self.logger.exception(f'While applying increment for {pair}:')
                raise

        with self.prices_holder.get((self.exchange, pair)) as price_processor:
            price_inc = price_processor.apply(increment)
        if price_inc:
            if self.ws_queue.has_actions():
                msg = [['price_inc', price_inc.to_dict(self.exchange, pair)]]
                try:
                    self.ws_queue.put(json.dumps(msg), True, 0.1)
                except queue.Full:
                    self.logger.warn("Timeout on putting message to ws_queue")