# encoding: utf-8
import bisect


class price_inc_t(object):
    """
    Класс, хранящий изменение состояния лучшей цены
    """
    def __init__(self, new_ask=None, new_bid=None, ts=0, seq=0):
        self.new_ask = new_ask  # tuple (rate, amount)
        self.new_bid = new_bid  # tuple (rate, amount)
        self.ts = ts
        self.seq = seq

    def to_dict(self, exchange, pair):
        d = { 'exchange': exchange,
              'pair': pair,
              'ts': self.ts,
              'seq': self.seq,
            }
        for action, vals in zip(['ask', 'bid'], [self.new_ask, self.new_bid]):
            if vals:
                d[action] = { 'rate': vals[0],
                              'amount': vals[1],
                            }
        return d

    @staticmethod
    def from_dict(d):
        new_ask = None
        new_bid = None
        if 'ask' in d:
            new_ask = (d['ask']['rate'], d['ask']['amount'])
        if 'bid' in d:
            new_bid = (d['bid']['rate'], d['bid']['amount'])

        return d['exchange'], d['pair'], price_inc_t(new_ask, new_bid, ts=d['ts'], seq=d['seq'])

    def __str__(self):
        return 'new_ask = {}, new_bid = {}, ts = {}, seq = {}'.format( self.new_ask,
                                                                       self.new_bid,
                                                                       self.ts,
                                                                       self.seq,
                                                                     )


class price_t(object):
    def __init__(self, ask_rate=0, ask_amount=0, bid_rate=0, bid_amount=0, ts=0, seq=0):
        self.ask_rate = ask_rate
        self.ask_amount = ask_amount
        self.bid_rate = bid_rate
        self.bid_amount = bid_amount
        self.ts = ts
        self.seq = seq

    def diff(self, new):
        new_ask = None
        new_bid = None
        if self.ask_rate != new.ask_rate or self.ask_amount != new.ask_amount:
            new_ask = (new.ask_rate, new.ask_amount)

        if self.bid_rate != new.bid_rate or self.bid_amount != new.bid_amount:
            new_bid = (new.bid_rate, new.bid_amount)

        if new_ask or new_bid:
            return price_inc_t( new_ask=new_ask,
                                new_bid=new_bid,
                                ts=new.ts,
                                seq=new.seq
                              )
        else:
            return None

    def __str__(self):
        return 'ask_rate = {}, ask_amount = {}, bid_rate = {}, '\
        'bid_amount = {}, ts = {}, seq = {}'.format( self.ask_rate,
                                                     self.ask_amount,
                                                     self.bid_rate,
                                                     self.bid_amount,
                                                     self.ts,
                                                     self.seq,
                                                   )

    def __eq__(self, lhs):
        return self.ask_rate == lhs.ask_rate and self.ask_amount == lhs.ask_amount\
           and self.bid_rate == lhs.bid_rate and self.bid_amount == lhs.bid_amount

    def apply(self, price_inc):
        """
        Применить икремент к ценам. Инкременты содержат абсолютные значения (не дельту)
        """
        if price_inc.new_ask:
            self.ask_rate, self.ask_amount = price_inc.new_ask

        if price_inc.new_bid:
            self.bid_rate, self.bid_amount = price_inc.new_bid

        if price_inc.new_ask or price_inc.new_bid:
            self.ts = price_inc.ts
            self.seq = price_inc.seq

    def to_dict(self, exchange, pair):
        d = { 'exchange': exchange,
              'pair': pair,
              'ts': self.ts,
              'seq': self.seq,
              'ask_rate': self.ask_rate,
              'ask_amount': self.ask_amount,
              'bid_rate': self.bid_rate,
              'bid_amount': self.bid_amount,
            }
        return d

    @staticmethod
    def from_dict(d):
        return d['exchange'], d['pair'], price_t( ask_rate=d['ask_rate'],
                                                  ask_amount=d['ask_amount'],
                                                  bid_rate=d['bid_rate'],
                                                  bid_amount=d['bid_amount'],
                                                  ts=d['ts'],
                                                  seq=d['seq'],
                                                )

    def __str__(self):
        return 'ask_rate = {}, ask_amount = {}, '\
               'bid_rate = {}, bid_amount = {}, '\
               'ts = {}, seq = {}'.format( self.ask_rate,
                                           self.ask_amount,
                                           self.bid_rate,
                                           self.bid_amount,
                                           self.ts,
                                           self.seq,
                                         )


class price_processor_t(object):
    """
    Объект, который отслеживает лучшую цену на ask и bid с учетом поступающих инкрементов.
    Поддерживается фильтр на минимальный amount
    """
    def __init__(self, exchange, pair, orderbooks_holder, amount_threshold=0):
        self.exchange = exchange
        self.pair = pair
        self.orderbooks_holder = orderbooks_holder
        self.amount_threshold = amount_threshold
        self.top_records = dict(asks=[], bids=[])
        self.cum_amount = dict(asks=0, bids=0)
        self.price = price_t()

    def apply(self, increment):
        """
        Мы отслеживаем лучшую цену по приходящим инкрементам полного orderbook'а. Многие инкременты
        не имеют влияния лучшую цену
        """
        for action in ['asks', 'bids']:
            if not self.top_records[action]:
                # еще не отслеживали лучшую цену
                self.full_scan(action)
                continue
            for a, rate, amount in increment.incs:
                if a != action[0]:
                    continue
                top_records = self.top_records[action]
                cum_amount = self.cum_amount[action]
                # случай для asks:
                # 1. если обновление рейта вне top_records (меньше), то игнорим
                # 2. если обновление рейта в top_records то:
                # 2.1. если удаление строчки, то смотрим, проходит ли cum - amount, который был.
                # Если да, то зануляем запись и все. Если нет - то пересобираем из полного
                # orderbook'а.
                # 2.2 Если уменьшение - то тоже самое
                # 2.3 Если увеличение, то не в самой нижней записи, то пересобираем orderbook из
                # top_records

                # для bids переводим rate в -rate и следуем логике выше
                if a == 'b':
                    rate = rate * -1
                if rate > top_records[-1][0]:
                    continue
                else:
                    if amount == 0:
                        idx = bisect.bisect_left([x[0] for x in top_records], rate)
                        if top_records[idx][0] == rate:
                            # нашли совпадение
                            if cum_amount - top_records[idx][1] >= self.amount_threshold and len(top_records) > 1:
                                # уменьшился объем, но все еще вписываемся в threshold
                                self.cum_amount[action] -= top_records[idx][1]
                                top_records.pop(idx)
                            else:
                                # к сожалению, нужно искать по полному orderbook'у
                                self.full_scan(action)
                        else:
                            # запись с rate могла исчезнуть из полного ордербука, т.к. в одном
                            # инкременте может быть несколько апдейтов, и они уже все были применены
                            # к полному ордербуку
                            pass
                    elif amount > 0:
                        idx = bisect.bisect_left([x[0] for x in top_records], rate)
                        if top_records[idx][0] == rate:
                            # нашли такую ще запись
                            if amount - top_records[idx][1] >= 0:
                                # увеличили, если это не крайняя запись, то нужно перестраивать partial_scan
                                self.cum_amount[action] += amount - top_records[idx][1]
                                top_records[idx] = (top_records[idx][0], amount)
                                if idx == len(top_records) - 1:
                                    continue
                                else:
                                    self.partial_scan(action)
                            else:
                                # уменьшили, если все еще вписываемся в amount_threshold, то делаем
                                # partial_scan, иначе - full_scan
                                if cum_amount - top_records[idx][1] + amount >= self.amount_threshold:
                                    self.cum_amount[action] += amount - top_records[idx][1]
                                    top_records[idx] = (top_records[idx][0], amount)
                                    if idx == len(top_records) - 1:
                                        continue
                                    else:
                                        self.partial_scan(action)
                                else:
                                    self.full_scan(action)
                        else:
                            # не нашли такую запись, делаем вставку и partial_scan
                            top_records.insert(idx, (rate, amount))
                            self.partial_scan(action)
        self.price.seq = increment.seq
        self.price.ts = increment.ts

        return self._update_price()

    def full_scan(self, action='asks'):
        """
        Восстанавливаем лучшие цены из полных orderbook'ов
        """
        with self.orderbooks_holder.get((self.exchange, self.pair)) as orderbook:
            values = [(x[0] * (-1 if action == 'bids' else 1), x[1]) for x in getattr(orderbook, action).items()]
            values = sorted(values)
        cum = 0
        reached = False
        for i in range(len(values)):
            cum += values[i][1]
            if cum >= self.amount_threshold:
                reached = True
                break
        if not reached:
            self.top_records[action] = values
            self.cum_amount[action] = cum
            return

        self.cum_amount[action] = cum
        self.top_records[action] = values[:i+1]

    def partial_scan(self, action):
        cum = 0
        reached = False
        top_records = self.top_records[action]
        for i in range(len(top_records)):
            cum += top_records[i][1]
            if cum >= self.amount_threshold:
                reached = True
                break
        if not reached:
            raise RuntimeError('partial_scan internall error')

        self.cum_amount[action] = cum
        self.top_records[action] = top_records[:i+1]

    def get_price(self):
        return self.price

    def _update_price(self):
        if not self.top_records['asks'] or self.cum_amount['asks'] < self.amount_threshold:
            ask_rate, ask_amount = 0, 0
        else:
            ask_rate, ask_amount = self.top_records['asks'][-1][0], self.cum_amount['asks']

        if not self.top_records['bids'] or self.cum_amount['bids'] < self.amount_threshold:
            bid_rate, bid_amount = 0, 0
        else:
            bid_rate, bid_amount = self.top_records['bids'][-1][0] * -1, self.cum_amount['bids']


        if  self.price.ask_rate == ask_rate and self.price.ask_amount == ask_amount\
        and self.price.bid_rate == bid_rate and self.price.bid_amount == bid_amount:
            return None
        else:
            new_price = price_t( ask_rate=ask_rate,
                                 ask_amount=ask_amount,
                                 bid_rate=bid_rate,
                                 bid_amount=bid_amount,
                                 ts=self.price.ts,
                                 seq=self.price.seq,
                               )
            inc = self.price.diff(new_price)
            self.price = new_price
            return inc
