import logging
import random
import json
import requests
import time

import orderbookd.db as db
from .common_loader import common_loader_t
from .util import strnum2int, timestamp_now, strnum_truncate
from .order_book import inc_t, orderbook_t

logging.getLogger('urllib3.connectionpool').setLevel(logging.INFO)

def supported_pairs():
    return [\
    "BTC_USD","BTC_EUR","BTC_RUR","LTC_BTC","LTC_USD","EMC_BTC","EMC_USD",
    "EMC_RUR","EMC_ETH","EMC_DASH","EMC_XMR","DASH_BTC","DASH_USD","ETH_BTC",
    "ETH_USD","ETH_RUR","DOGE_BTC","DOGE_USD","CURE_BTC","SIB_BTC","SIB_RUR",
    "TX_BTC","RBIES_BTC","ADZ_BTC","ADZ_USD","BSD_BTC","SXC_BTC","BTA_BTC",
    "VOX_BTC","MOJO_BTC","CRBIT_BTC","CRBIT_LEO","CRBIT_ETH","SHIFT_BTC",
    "SHIFT_USD","SHIFT_ETH","YOC_BTC","YOC_USD","YOC_RUR","YOC_ETH",
    "CREVA_BTC","LSK_BTC","LSK_USD","EL_BTC","EL_USD","EL_RUR","HNC_BTC",
    "HNC_ETH","HNC_USD","HNC_EUR","CLOAK_BTC","CLOAK_USD","MOIN_BTC",
    "BLU_BTC","BLU_USD","LEO_BTC","LEO_USD","LEO_RUR","LEO_ETH","PPC_BTC",
    "PPC_USD","NMC_BTC","MONA_BTC","GAME_BTC","BLK_BTC","SYS_BTC","DGB_BTC",
    "DGB_ETH","DGB_USD","DGB_EUR","DGB_RUR","THS_BTC","THS_USD","THS_RUR",
    "THS_ETH","VRC_BTC","SLR_BTC","DBIX_BTC","XMR_BTC","XMR_USD","BTS_BTC",
    "GB_BTC","VRM_BTC","ATX_BTC","ENT_BTC","BURST_BTC","NXT_BTC","POST_BTC",
    "POST_ETH","EDR_BTC","EDR_USD","EDR_RUR","KRB_BTC","KRB_USD","KRB_RUR",
    "ARC_BTC","ARC_USD","ARC_RUR","ARC_ETH","ARC_BCH","GYC_BTC","DMC_BTC",
    "DMC_USD","VRS_BTC","VRS_USD","XRC_BTC","XRC_USD","BIT_BTC","DOLLAR_BTC",
    "OD_BTC","XAUR_BTC","GOLOS_BTC","UNC_BTC","VLTC_BTC","CCRB_BTC",
    "CCRB_ETH","BPC_BTC","BPC_ETH","EUR_USD","USD_RUR","PRES_BTC","DIME_BTC",
    "DIME_USD","DIME_EUR","DIME_RUR","DIME_ETH","DIBC_BTC","DIBC_USD",
    "DIBC_ETH","VSL_BTC","ICN_BTC","NVC_BTC","NVC_USD","XSPEC_BTC","LUNA_BTC",
    "ACN_BTC","LDC_BTC","MSCN_BTC","MSCN_USD","MSCN_EUR","MSCN_RUR",
    "MSCN_ETH","OBITS_BTC","OBITS_USD","OBITS_ETH","TIME_BTC","TIME_ETH",
    "TIME_USD","WAVES_BTC","DANC_BTC","DANC_USD","INCNT_BTC","TAAS_BTC",
    "TAAS_USD","XMS_BTC","SOAR_BTC","SOAR_ETH","PIVX_BTC","PIVX_USD",
    "PIVX_EUR","PIVX_RUR","PIVX_ETH","FUNC_BTC","FUNC_USD","FUNC_ETH",
    "ITI_BTC","ITI_EUR","ITI_RUR","ITI_ETH","PUT_BTC","PUT_USD","PUT_RUR",
    "PUT_ETH","GUP_BTC","GUP_ETH","MNE_BTC","MNE_ETH","WINGS_BTC","WINGS_ETH",
    "UNRC_BTC","UNRC_USD","RLT_BTC","RLT_USD","RLT_RUR","RLT_ETH",
    "FORTYTWO_BTC","FORTYTWO_USD","FORTYTWO_ETH","STRAT_BTC","STRAT_USD",
    "STRAT_ETH","INSN_BTC","INSN_USD","INSN_ETH","TRUMP_BTC","TRUMP_ETH",
    "FNC_BTC","FNC_ETH","FNC_USD","MCO_BTC","MCO_USD","MCO_ETH","VOISE_BTC",
    "VOISE_USD","VOISE_EUR","VOISE_ETH","PPY_BTC","PPY_USD","PPY_ETH",
    "ASAFE2_BTC","ASAFE2_USD","PLBT_BTC","PLBT_EMC","PLBT_ETH","PLBT_USD",
    "KPL_BTC","KPL_USD","KPL_ETH","BCH_BTC","BCH_USD","BCH_RUR","BCH_ETH",
    "MCR_BTC","MCR_ETH","PIPL_BTC","PIPL_ETH","HVN_BTC","HVN_USD","HVN_ETH",
    "XRL_BTC","XRL_ETH","MGO_BTC","MGO_ETH","FU_BTC","FU_ETH","WIC_BTC",
    "WIC_ETH","GRS_BTC","GRS_USD","GRS_ETH","PRO_BTC","PRO_USD","PRO_ETH",
    "XEM_BTC","XEM_USD","XEM_ETH","CPC_BTC","CPC_USD","CPC_ETH","wETT_BTC",
    "wETT_USD","wETT_ETH","eETT_BTC","eETT_USD","eETT_ETH","SUMO_BTC",
    "SUMO_ETH","QTUM_BTC","QTUM_USD","QTUM_ETH","OMG_BTC","OMG_USD","OMG_ETH",
    "PAY_BTC","PAY_USD","PAY_ETH","KNC_BTC","KNC_USD","KNC_ETH","GNT_BTC",
    "GNT_USD","GNT_ETH","EOS_BTC","EOS_USD","EOS_ETH","BAT_BTC","BAT_USD",
    "BAT_ETH","REP_BTC","REP_USD","REP_ETH","MTL_BTC","MTL_USD","MTL_ETH",
    "DGD_BTC","DGD_USD","DGD_ETH","CVC_BTC","CVC_USD","CVC_ETH","SNGLS_BTC",
    "SNGLS_USD","SNGLS_ETH","SNT_BTC","SNT_USD","SNT_ETH","GNO_BTC","GNO_USD",
    "GNO_ETH","ZRX_BTC","ZRX_USD","ZRX_ETH","BNT_BTC","BNT_USD","BNT_ETH",
    "FUN_BTC","FUN_USD","FUN_ETH","EDG_BTC","EDG_USD","EDG_ETH","ANT_BTC",
    "ANT_USD","ANT_ETH","ETHOS_BTC","ETHOS_USD","ETHOS_ETH","STORJ_BTC",
    "STORJ_USD","STORJ_ETH","RLC_BTC","RLC_USD","RLC_ETH","TKN_BTC","TKN_USD",
    "TKN_ETH","MLN_BTC","MLN_USD","MLN_ETH","TRST_BTC","TRST_USD","TRST_ETH",
    "FirstBlood_BTC","FirstBlood_USD","FirstBlood_ETH","VIB_BTC","VIB_USD",
    "VIB_ETH","BIO_BTC","BIO_ETH","BIO_RUR","BIO_USD","NEO_BTC","NEO_USD",
    "NEO_ETH","OTN_BTC","OTN_USD","OTN_ETH","MNX_BTC","MNX_USD","MNX_ETH",
    "ENJ_BTC","ENJ_ETH","DAY_BTC","DAY_ETH","ETHP_BTC","ETHP_ETH","ATM_BTC",
    "ATM_ETH","DMD_BTC","DMD_USD","DMD_ETH","OXY_BTC","OXY_USD","OXY_ETH",
    "CLD_BTC","CLD_ETH","ARTE_BTC","ARTE_ETH","CDX_BTC","CDX_ETH","CLPC_BTC",
    "CLPC_USD","CLPC_ETH","ESP_BTC","ESP_ETH","BTB_BTC","BTB_ETH","ESC_BTC",
    "ESC_ETH","PRG_BTC","PRG_USD","PRG_ETH","AMM_BTC","AMM_USD","AMM_ETH",
    "HST_BTC","HST_USD","HST_ETH","ERO_BTC","ERO_ETH","KICK_BTC","KICK_USD",
    "KICK_RUR","KICK_ETH","UQC_BTC","UQC_USD","UQC_ETH","GRX_BTC","GRX_ETH",
    "INS_BTC","INS_ETH","ICOS_BTC","ICOS_USD","ICOS_ETH","TER_BTC","TER_ETH",
    "FLP_BTC","FLP_ETH","RBM_BTC","RBM_USD","RBM_EUR","RBM_ETH","RBM_LTC",
    "FLIXX_BTC","FLIXX_ETH","DTR_BTC","DTR_ETH","EVC_BTC","EVC_ETH","SPF_BTC",
    "SPF_ETH","B2B_BTC","B2B_ETH","TFL_BTC","TFL_ETH","CHSB_BTC","CHSB_ETH",
    "PIN_BTC","PIN_ETH","GOAL_BTC","GOAL_ETH","GOAL_USD","COV_BTC","COV_ETH",
    "IFAN_BTC","IFAN_ETH","IPL_BTC","IPL_ETH","NOX_BTC","NOX_ETH","TWC_BTC",
    "TWC_TER","TWC_ETH","KAPU_BTC","KAPU_ETH","TRX_BTC","TRX_ETH","TRX_USD",
    "CRC_BTC","CRC_ETH","CRC_USD","CRC_EUR","ESR_BTC","ESR_ETH","ESR_BCH",
    "ESR_DASH","TUBE_BTC","TUBE_ETH","TUBE_USD","TUBE_XMR","SPA_BTC",
    "SPA_ETH","LTT_BTC","LTT_ETH","AMB_BTC","AMB_ETH","AMB_USD","AMB_RUR",
    "ECHO_BTC","ECHO_ETH","ECHO_USD","HNR_BTC","HNR_ETH","HNR_USD","NAM_BTC",
    "NAM_ETH","NAM_USD","ORE_BTC","ORE_ETH","ORE_USD","PPT_BTC","PPT_ETH",
    "PPT_USD","DIG_BTC","DIG_ETH","DIG_USD","DIG_LTC","ARK_BTC","ARK_ETH",
    "ARK_USD","ECIO_BTC","ECIO_ETH","XSN_BTC","XSN_ETH","XSN_LTC","BPTN_BTC",
    "BPTN_ETH","IDH_BTC","IDH_ETH","XBT_BTC","XBT_ETH","USC_BTC","USC_ETH",
    "FXT_BTC","FXT_ETH","CBR_BTC","CBR_ETH","CBR_USD","CBR_EUR","VIEW_BTC",
    "VIEW_ETH","ORME_BTC","ORME_ETH","ORME_USD","JOY_BTC","JOY_ETH",
    "NOKU_BTC","NOKU_ETH","NOKU_EURN","EURN_BTC","BTC_EURN","ETH_EURN",
    "USD_EURN","EUR_EURN","XMR_EURN","WGR_BTC","WGR_ETH","CNB_BTC","CNB_ETH",
    "ADL_BTC","ADL_ETH","ADL_USD","LATX_BTC","LATX_ETH","MTRc_BTC","MTRc_ETH",
    "MTRc_USD","PHI_BTC","PHI_ETH","PHI_USD","NOAH_BTC","HB_BTC","HB_USD",
    "HB_ETH","SKCH_BTC","SKCH_ETH","FLOT_BTC","FLOT_ETH","SPD_BTC","SPD_ETH",
    "TNS_BTC","TNS_ETH","RTB_BTC","RTB_ETH","TPI_BTC","TPI_ETH","VIU_BTC",
    "VIU_ETH","EOSDAC_BTC","EOSDAC_ETH","ELI_BTC","ELI_ETH","ELI_BCH",
    "SHPING_BTC","SHPING_ETH","CARB_BTC","CARB_ETH","PELO_BTC","PELO_ETH",
    "PELO_USD","UTNP_BTC","UTNP_ETH","MORPH_BTC","MORPH_ETH","MRP_BTC",
    "MRP_ETH","PLN_BTC","PLN_ETH","SIG_BTC","SIG_ETH","ONL_BTC","ONL_ETH",
    "BEZ_BTC","BEZ_ETH","BEZ_USD","CASH_BTC","CASH_ETH","BTCH_BTC","BTCH_ETH",
    "MNTP_BTC","MNTP_ETH","SCT_BTC","SCT_ETH","ONT_BTC","ONT_ETH","ONT_USD",
    "LEDU_BTC","LEDU_ETH","RST_BTC","RST_ETH","TGAME_BTC","TGAME_ETH",
    "PISH_BTC","PISH_ETH","HXX_BTC","GIFT_BTC","GIFT_ETH","ECO_BTC","ECO_ETH",
    "W3C_BTC","W3C_ETH","SNBL_BTC","SNBL_ETH","ZBC_BTC","ZBC_ETH"]
    r = requests.get('https://data.gate.io/api2/1/marketinfo')
    return [next(iter(d.keys())) for d in r.json()['pairs']]


def subset_of_supported_pairs(pairs):
    all_supported_pairs = set([p.upper() for p in supported_pairs()])
    subset_supported_pairs = []
    for pair in pairs:
        if pair in all_supported_pairs:
            subset_supported_pairs.append(pair)
        else:
            reverse_pair = '_'.join(pair.split('_')[::-1])
            if reverse_pair in all_supported_pairs:
                subset_supported_pairs.append(reverse_pair)

    return subset_supported_pairs


def parse_orderbook(data, orderbook, ts, precision=8):
    """
    Формат данных:
    ```
    {"timestamp":1532927309622,
     "asks":[["82.69206","0.00546692",1532923705711],["83.09","0.33548",1532926255071],...],
     "bids":[["82.69203","0.33233102",1532924920160],["82.37222","23.328381",1532927281313],...]
    }
    ```
    Может прилететь вида: '16E+3'
    """
    orderbook.asks = {strnum2int(strfloat_to_strnum(x[0]), precision=precision): strnum2int(strfloat_to_strnum(x[1]), precision=precision) for x in data['asks']}
    orderbook.bids = {strnum2int(strfloat_to_strnum(x[0]), precision=precision): strnum2int(strfloat_to_strnum(x[1]), precision=precision) for x in data['bids']}
    orderbook.ts = ts
    orderbook.seq = 0  # livecoine http API не поддерживает механиз seq

def strfloat_to_strnum(s):
    if 'E+' in s:
        return '{:0.8f}'.format(float(s))
    return s


class loader_t(common_loader_t):
    def __init__(self, dumper_queue, orderbooks_holder, prices_holder, ws_queue, config):
        super(loader_t, self).__init__( exchange='livecoin',
                                        dumper_queue=dumper_queue,
                                        orderbooks_holder=orderbooks_holder,
                                        prices_holder=prices_holder,
                                        ws_queue=ws_queue,
                                        config=config,
                                        logger=logging.getLogger('livecoin_loader'),
                                      )
        self.level = config[self.exchange]['level']
        self.period = config[self.exchange]['period']
        self.precision = config['precision']
        self.pairs = subset_of_supported_pairs(config[self.exchange]['pairs'])
        if len(self.pairs) != len(config[self.exchange]['pairs']):
            self.logger.error('Supported {} of {} pairs'.format(len(self.pairs), len(config[self.exchange]['pairs'])))
            self.logger.error('{}'.format(','.join(self.pairs)))
            raise RuntimeError('Unsupported pairs')
        self.seq_counters = self.get_last_seqs()

        self.session = requests.Session()

    def gen_url(self, pair):
        pair = pair.replace('_', '/')
        return f'https://api.livecoin.net/exchange/order_book?currencyPair={pair}&groupByPrice=True&depth={self.level}'

    def get_last_seqs(self):
        """
        Т.к. gateio не поддерживает seq, то нам нужно поддерживать его исскуственно. Для этого нам нужно
        получать из базы последний записанный seq
        """
        if self.config['db']['use']:
            conn = db.get_connection(self.config['db'])
            ret = db.select_last_seq_increments(conn, self.exchange, self.pairs)
            conn.close()
        else:
            self.logger.warning('db.use = False, last seqs set to 0')
            ret = {p: 0 for p in self.pairs}
        return ret

    def serve_connetion(self):
        last_update = 0
        self.stop_connection_flag = False
        while not self.stop_connection_flag:
            ts = time.time()
            if last_update + self.period < time.time():
                last_update = ts
                for pair in self.pairs:
                    if self.stop_connection_flag:
                        break
                    try:
                        res = self.session.get(self.gen_url(pair))
                        self._process_message(pair, res.json())
                    except:
                        self.logger.exception('')
                        self.stop_connection_flag = True
            else:
                time.sleep(0.1)

    def _process_message(self, pair, msg):
        # self.logger.debug(f'process message: {msg}')
        ts = timestamp_now()

        def extract(ob):
            parse_orderbook(msg, ob, ts, precision=self.precision)
            ob.seq = self.seq_counters[pair]
        if pair not in self.last_dump_period:
            # пришел полный ордербук в первый раз (с учетом лимита, конечно)
            # в случае первой загруги ордербука предполагаем, что мы пропустили ряд обновлений,
            # поэтому пропускаем 2 seq (символически, чтобы потом найти дыру в данных)
            self.seq_counters[pair] += 2
            self.process_intial_orderbook(pair, extract)
        else:
            # промежуточная загрузка полного дампа, делаем из него инкремент и обрабатываем как
            # обычно
            self.seq_counters[pair] += 1
            ob_new = orderbook_t()
            extract(ob_new)
            with self.orderbooks_holder.get((self.exchange, pair)) as orderbook:
                increment = orderbook.diff(ob_new, check_seq=True)
            if increment is None:
                # Мы можем получать данные, даже если там не было изменений.
                # Мы просто делаем вид, что ничего не приходило
                self.seq_counters[pair] -= 1
                # self.logger.warning(f'Got intermidiate full orderbook for {pair}, but increment is empty')
                return
            else:
                self.process_increment(pair, increment)

    def stop_connection(self):
        if hasattr(self, 'stop_connection_flag'):
            self.stop_connection_flag = True