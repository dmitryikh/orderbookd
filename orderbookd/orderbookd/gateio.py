import logging
import random
import json
import requests
import websocket

import orderbookd.db as db
from .common_loader import common_loader_t
from .util import strnum2int, timestamp_now, strnum_truncate
from .order_book import inc_t, orderbook_t

def supported_pairs():
    """
    Получить через API биржи все поддерживаемые валютные пары
    """
    r = requests.get('https://data.gate.io/api2/1/marketinfo')
    return [next(iter(d.keys())) for d in r.json()['pairs']]


def subset_of_supported_pairs(pairs):
    all_supported_pairs = set([p.upper() for p in supported_pairs()])
    subset_supported_pairs = []
    for pair in pairs:
        if pair in all_supported_pairs:
            subset_supported_pairs.append(pair)
        else:
            reverse_pair = '_'.join(pair.split('_')[::-1])
            if reverse_pair in all_supported_pairs:
                subset_supported_pairs.append(reverse_pair)

    return subset_supported_pairs


def parse_orderbook(data, orderbook, ts, precision=8):
    """
    Формат данных:
    ```
        {"asks": [["0.01956859", "0.603"], ["0.0195686", "4.9529536"], ...],
         "bids": [["0.0192261", "0.79802581"], ["0.01922609", "91.56541524"], ...]
        }
    ```
    Размер десятичной части для amount может быть больше 8!
    """
    orderbook.asks = {strnum2int(x[0], precision=precision): strnum2int(strnum_truncate(x[1], precision=precision), precision=precision) for x in data['asks']}
    orderbook.bids = {strnum2int(x[0], precision=precision): strnum2int(strnum_truncate(x[1], precision=precision), precision=precision) for x in data['bids']}
    orderbook.ts = ts
    orderbook.seq = 0  # gateio не поддерживает механиз seq


def parse_increment(data, ts, precision=8):
    """
    Формат данных:
    ```
        {"bids": [["0.036818", "0"], ["0.023601", "93.1888"]]}
    ```
    Размер десятичной части для amount может быть больше 8!
    """
    increment = inc_t(0, ts)  # gateio не поддерживает seq
    for action, values in data.items():
        assert action in ['asks', 'bids']
        for val in values:
            increment.incs.append([ action[0],
                                    strnum2int(val[0], precision=precision),
                                    strnum2int(strnum_truncate(val[1], precision=precision), precision=precision),
                                  ])
    return increment


class loader_t(common_loader_t):
    def __init__(self, dumper_queue, orderbooks_holder, prices_holder, ws_queue, config):
        super(loader_t, self).__init__( exchange='gateio',
                                        dumper_queue=dumper_queue,
                                        orderbooks_holder=orderbooks_holder,
                                        prices_holder=prices_holder,
                                        ws_queue=ws_queue,
                                        config=config,
                                        logger=logging.getLogger('gateio_loader'),
                                      )
        self.pairs = subset_of_supported_pairs(config[self.exchange]['pairs'])
        if len(self.pairs) != len(config[self.exchange]['pairs']):
            self.logger.error('Supported {} of {} pairs'.format(len(self.pairs), len(config[self.exchange]['pairs'])))
            self.logger.error('{}'.format(','.join(self.pairs)))
            raise RuntimeError('Unsupported pairs')
        self.seq_counters = self.get_last_seqs()

    def get_last_seqs(self):
        """
        Т.к. gateio не поддерживает seq, то нам нужно поддерживать его исскуственно. Для этого нам нужно
        получать из базы последний записанный seq
        """
        if self.config['db']['use']:
            conn = db.get_connection(self.config['db'])
            ret = db.select_last_seq_increments(conn, self.exchange, self.pairs)
            conn.close()
        else:
            self.logger.warning('db.use = False, last seqs set to 0')
            ret = {p: 0 for p in self.pairs}
        return ret


    def serve_connetion(self):
        self.ws = websocket.WebSocketApp( "wss://ws.gate.io/v3/",
                                            on_message = self.ws_on_message,
                                            on_error = self.ws_on_error,
                                            on_close = self.ws_on_close
                                        )
        self.ws.on_open = self.ws_on_open
        if 'proxies' in self.config and len(self.config['proxies']) > 0:
            # TODO: сейчас прокси выбирается случайно, но нужен более продвинутый алгоритм,
            # который бы учитывал сбои на прокси и их нагруженность
            proxy = random.choice(self.config['proxies'])
            self.ws.run_forever( http_proxy_host=proxy['host'],
                                    http_proxy_port=proxy['port'],
                                    http_proxy_auth=(proxy['login'], proxy['password']),
                                )
        else:
            self.ws.run_forever()

    def stop_connection(self):
        if hasattr(self, 'ws'):
            self.ws.close()

    def ws_on_open(self, ws):
        self.logger.info('ws_on_open')
        # NOTE: согласно документации, максимальная глубина ордербуков, поддерживаемая gateio: 30
        # https://gate.io/docs/websocket/index.html
        params = [[pair, 30, '0.00000001'] for pair in self.pairs]
        d = dict( id=123,
                  method='depth.subscribe',
                  params=params,
                )
        self.ws.send(json.dumps(d))

    def ws_on_error(self, ws, error):
        self.logger.error('ERROR: {}'.format(error))
        self.ws.close()

    def ws_on_close(self, ws):
        self.logger.info('ws_on_close')

    def ws_on_message(self, ws, message):
        try:
            self.process_message(message)
        except Exception:
            self.logger.exception('Exception while processing message:')
            self.ws.close()

    def process_message(self, message):
        ts = timestamp_now()
        msg = json.loads(message)
        if 'method' not in msg or msg['method'] != 'depth.update':
            self.logger.error(f'Got unknown message: {message}')
            return

        data = msg['params']
        clean, depth, pair = data

        if clean:
            # пришел полный ордербук (с учетом лимита, конечно)
            # Это может быть либо первоначальная загрузка, либо промежуточная (gateio иногда
            # присылает полный ордербук вместо только апдейта)
            def extract(ob):
                parse_orderbook(depth, ob, ts, precision=self.config['precision'])
                ob.seq = self.seq_counters[pair]

            if pair not in self.last_dump_period:
                # в случае первой загруги ордербука предполагаем, что мы пропустили ряд обновлений,
                # поэтому пропускаем 2 seq (символически, чтобы потом найти дыру в данных)
                self.seq_counters[pair] += 2
                self.process_intial_orderbook(pair, extract)
            else:
                # промежуточная загрузка полного дампа, делаем из него инкремент и обрабатываем как
                # обычно
                self.seq_counters[pair] += 1
                ob_new = orderbook_t()
                extract(ob_new)
                with self.orderbooks_holder.get((self.exchange, pair)) as orderbook:
                    increment = orderbook.diff(ob_new, check_seq=True)
                if increment is None:
                    raise RuntimeError(f'Got intermidiate full orderbook for {pair}, but inrement is empty')
                self.process_increment(pair, increment)
        else:
            increment = parse_increment(depth, ts)
            self.seq_counters[pair] += 1
            increment.seq = self.seq_counters[pair]
            self.process_increment(pair, increment)