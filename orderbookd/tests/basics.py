import unittest
import json
import yaml

from orderbookd.util import strnum2int, int2strnum,\
                            varint_encode, varint_decode,\
                            is_dicts_equal

from orderbookd.util import concurrent_map_t
from orderbookd.order_book import orderbook_t, inc_t
from orderbookd.price import price_processor_t, price_t
from orderbookd.arbitrage import arbitrage_processor_t


class TestStrnum(unittest.TestCase):
    def test_strnum2int(self):
        self.assertEqual(strnum2int('0.00000001', precision=8), 1)
        with self.assertRaises(ValueError):
            self.assertEqual(strnum2int('0.00000001', precision=7), 0)
        self.assertEqual(strnum2int('0.00000001', precision=9), 10)

        self.assertEqual(strnum2int('10.00000001', precision=8), 1000000001)
        with self.assertRaises(ValueError):
            self.assertEqual(strnum2int('10.00000001', precision=7), 100000000)
        self.assertEqual(strnum2int('10.00000001', precision=9), 10000000010)

        self.assertEqual(strnum2int('1234567890.12345678', precision=8), 123456789012345678)
        self.assertEqual(strnum2int('9999999999.99999999', precision=8), 999999999999999999)

    def test_int2strnum(self):
        self.assertEqual(int2strnum(1, precision_decode=8, precision=8), '0.00000001')
        self.assertEqual(int2strnum(0, precision_decode=7, precision=8), '0.00000000')
        self.assertEqual(int2strnum(10, precision_decode=9, precision=8), '0.00000001')

        self.assertEqual(int2strnum(1000000001, precision_decode=8, precision=8), '10.00000001')
        self.assertEqual(int2strnum(100000000, precision_decode=7, precision=8), '10.00000000')
        self.assertEqual(int2strnum(10000000010, precision_decode=9, precision=8), '10.00000001')

        self.assertEqual(int2strnum(123456789012345678, precision_decode=8, precision=8), '1234567890.12345678')
        self.assertEqual(int2strnum(999999999999999999, precision_decode=8, precision=8), '9999999999.99999999')

        self.assertEqual(int2strnum(123456789012345678, precision_decode=8, precision=9), '1234567890.123456780')
        self.assertEqual(int2strnum(999999999999999999, precision_decode=8, precision=10), '9999999999.9999999900')

        self.assertEqual(int2strnum(123456789012345678, precision_decode=8, precision=7), '1234567890.1234567')
        self.assertEqual(int2strnum(999999999999999999, precision_decode=8, precision=6), '9999999999.999999')
        self.assertEqual(int2strnum(123456789012345678, precision_decode=8, precision=1), '1234567890.1')
        self.assertEqual(int2strnum(999999999999999999, precision_decode=8, precision=1), '9999999999.9')


class TestVarint(unittest.TestCase):
    def test_varint_encode(self):
        with self.assertRaises(ValueError):
            varint_encode(-1)
        self.assertEqual(varint_encode(0), b'\x00')
        self.assertEqual(varint_encode(1), b'\x01')
        self.assertEqual(varint_encode(10), b'\x0A')
        self.assertEqual(varint_encode(150), b'\x96\x01')
        self.assertEqual(varint_encode(1000000001), b'\x81\x94\xeb\xdc\x03')
        self.assertEqual(varint_encode(123456789012345678), b'\xce\xe6\xc3\xb1\xba\xe9\xa6\xdb\x01')
        self.assertEqual(varint_encode(999999999999999999), b'\xff\xff\x8f\xbb\xba\xd6\xad\xf0\r')
        self.assertEqual(varint_encode(999999999999999999999999999999999999), b'\xff\xff\xff\xff\xff\xe1\xe7\xa5\xb3\xab\x9c\xc8\xbc\xcf\xf3\xcb\xc0\x01')

    def test_varint_decode(self):
        def read_one(buffer):
            res, pos = varint_decode(buffer, 0)
            self.assertEqual(pos, len(buffer))
            return res
        self.assertEqual(read_one(b'\x00'), 0)
        self.assertEqual(read_one(b'\x01'), 1)
        self.assertEqual(read_one(b'\x0A'), 10)
        self.assertEqual(read_one(b'\x96\x01'), 150)
        self.assertEqual(read_one(b'\x81\x94\xeb\xdc\x03'), 1000000001)
        self.assertEqual(read_one(b'\xce\xe6\xc3\xb1\xba\xe9\xa6\xdb\x01'), 123456789012345678)
        self.assertEqual(read_one(b'\xff\xff\x8f\xbb\xba\xd6\xad\xf0\r'), 999999999999999999)
        self.assertEqual(read_one(b'\xff\xff\xff\xff\xff\xe1\xe7\xa5\xb3\xab\x9c\xc8\xbc\xcf\xf3\xcb\xc0\x01'), 999999999999999999999999999999999999)

    def test_encode_decode(self):
        ints = [ 0,
                 1,
                 10,
                 100,
                 125,
                 50,
                 10000001,
                 20000002,
                 30003,
                 999999999999999999999999999999999999,
                 999999999999999999,
                 123456789012345678,
              ]
        buf = b''
        for i in ints:
            buf += varint_encode(i)

        pos = 0
        for i in ints:
            i_dec, pos = varint_decode(buf, pos)
            self.assertEqual(i_dec, i)
        self.assertEqual(pos, len(buf))


class TestOrderbookCompression(unittest.TestCase):
    def check_compress_decompress(self, ob):
        ob_r = orderbook_t()
        ob_r.decompress(ob.compress())
        self.assertTrue(is_dicts_equal(ob.asks, ob_r.asks))
        self.assertTrue(is_dicts_equal(ob.bids, ob_r.bids))

    def test_orderbook_compression(self):
        self.check_compress_decompress(orderbook_t())
        with open('ob1.json') as fin:
            self.check_compress_decompress(orderbook_t.from_json(fin.read()))
        with open('ob2.json') as fin:
            self.check_compress_decompress(orderbook_t.from_json(fin.read()))


class TestIncrementApply(unittest.TestCase):
    def test_increment(self):
        # Проверяем, что можем применить пустой инкремент
        inc = inc_t(seq=0, ts=0)
        ob = orderbook_t()
        ob2 = ob.clone()
        ob2.apply(inc)
        self.assertTrue(ob.is_equal(ob2))

        # Проверяем, что работает проверка на seq
        with self.assertRaises(RuntimeError):
            inc = inc_t(seq=10, ts=0)
            orderbook_t(seq=8).apply(inc)

        orderbook_t(seq=9).apply(inc)

        # Проверяем, что aply выполняет свою функцию
        inc = inc_t(seq=10, ts=1234)
        inc.add_inc('a', 1, 123456781234)
        ob = orderbook_t(seq=9)
        ob2 = ob.clone()
        ob2.asks[1] = 123456781234
        ob2.ts = 1234
        self.assertTrue(ob.is_equal(ob2))

        # проверяем на реальном ордербуке
        with open('ob1.json') as fin:
            ob = orderbook_t.from_json(fin.read())
        ob.seq = 1234
        ob.ts = 1000
        ob_rate = list(ob.asks.items())[0][0]

        inc = inc_t(seq=1235, ts = 2000)
        inc.add_inc('a', 110, 220)
        inc.add_inc('a', ob_rate, 0)
        inc.add_inc('b', 330, 1)

        ob2 = ob.clone()
        ob.apply(inc)
        ob2.asks[110] = 220
        del ob2.asks[ob_rate]
        ob2.bids[330] = 1

        self.assertTrue(ob.is_equal(ob2))


class TestPricesIncrementApply(unittest.TestCase):
    def check_price(self, price, *args):
        self.assertEqual(price, price_t(*args))

    def test_increment(self):
        obh = concurrent_map_t()
        obh.reg('pair', orderbook_t())
        p = price_processor_t('pair', obh, amount_threshold=0)

        def apply(inc):
            with obh.get('pair') as ob:
                ob.apply(inc)
            p.apply(inc)

        inc1 = inc_t(seq=1, ts=1)
        apply(inc1)
        self.check_price(p.get_price(), 0, 0, 0, 0)

        inc2 = inc_t(seq=2, ts=2)
        inc2.add_inc('a', 10, 20)
        inc2.add_inc('a', 20, 40)
        apply(inc2)
        self.check_price(p.get_price(), 10, 20, 0, 0)

        inc3 = inc_t(seq=3, ts=3)
        inc3.add_inc('a', 10, 2)
        apply(inc3)
        self.check_price(p.get_price(), 10, 2, 0, 0)

        inc4 = inc_t(seq=4, ts=4)
        inc4.add_inc('a', 5, 1)
        inc4.add_inc('a', 80, 40)
        apply(inc4)
        self.check_price(p.get_price(), 5, 1, 0, 0)

        inc5 = inc_t(seq=5, ts=5)
        inc5.add_inc('a', 5, 5)
        apply(inc5)
        self.check_price(p.get_price(), 5, 5, 0, 0)

        inc6 = inc_t(seq=6, ts=6)
        inc6.add_inc('a', 80, 0)
        apply(inc6)
        self.check_price(p.get_price(), 5, 5, 0, 0)

        inc7 = inc_t(seq=7, ts=7)
        inc7.add_inc('a', 5, 0)
        apply(inc7)
        self.check_price(p.get_price(), 10, 2, 0, 0)

        inc8 = inc_t(seq=8, ts=8)
        inc8.add_inc('a', 7, 1)
        apply(inc8)
        self.check_price(p.get_price(), 7, 1, 0, 0)

        inc9 = inc_t(seq=9, ts=9)
        inc9.add_inc('a', 11, 11)
        apply(inc9)
        self.check_price(p.get_price(), 7, 1, 0, 0)

    def test_increment_w_threshold(self):
        obh = concurrent_map_t()
        obh.reg('pair', orderbook_t())
        p = price_processor_t('pair', obh, amount_threshold=10)

        def apply(inc):
            with obh.get('pair') as ob:
                ob.apply(inc)
            p.apply(inc)

        inc1 = inc_t(seq=1, ts=1)
        apply(inc1)
        self.check_price(p.get_price(), 0, 0, 0, 0)

        inc2 = inc_t(seq=2, ts=2)
        inc2.add_inc('a', 10, 20)
        inc2.add_inc('a', 20, 40)
        inc2.add_inc('b', 5, 5)
        apply(inc2)
        # a: (10, 20), (20, 40)
        # b: (5, 5)
        self.check_price(p.get_price(), 10, 20, 0, 0)

        inc3 = inc_t(seq=3, ts=3)
        inc3.add_inc('a', 10, 2)
        inc3.add_inc('b', 10, 5)
        apply(inc3)
        # a: (10, 2), (20, 40)
        # b: (5, 5), (10, 5)
        self.check_price(p.get_price(), 20, 42, 5, 10)

        inc4 = inc_t(seq=4, ts=4)
        inc4.add_inc('a', 5, 1)
        inc4.add_inc('a', 80, 40)
        inc4.add_inc('b', 7, 2)
        inc4.add_inc('b', 10, 10)
        apply(inc4)
        # a: (5, 1), (10, 2), (20, 40), (80, 40)
        # b: (5, 5), (7, 2), (10, 10)
        self.check_price(p.get_price(), 20, 43, 10, 10)

        inc5 = inc_t(seq=5, ts=5)
        inc5.add_inc('a', 5, 5)
        inc5.add_inc('b', 12, 5)
        apply(inc5)
        # a: (5, 5), (10, 2), (20, 40), (80, 40)
        # b: (5, 5), (7, 2), (10, 10), (12, 5)
        self.check_price(p.get_price(), 20, 47, 10, 15)

        inc6 = inc_t(seq=6, ts=6)
        inc6.add_inc('a', 20, 0)
        inc6.add_inc('b', 13, 5)
        apply(inc6)
        # a: (5, 5), (10, 2), (80, 40)
        # b: (5, 5), (7, 2), (10, 10), (12, 5), (13, 5)
        self.check_price(p.get_price(), 80, 47, 12, 10)

        inc7 = inc_t(seq=7, ts=7)
        inc7.add_inc('a', 5, 0)
        inc7.add_inc('b', 12, 3)
        inc7.add_inc('b', 5, 20)
        apply(inc7)
        # a: (10, 2), (80, 40)
        # b: (5, 20), (7, 2), (10, 10), (12, 3), (13, 5)
        self.check_price(p.get_price(), 80, 42, 10, 18)

        inc8 = inc_t(seq=8, ts=8)
        inc8.add_inc('a', 7, 1)
        inc8.add_inc('b', 12, 4)
        inc8.add_inc('b', 5, 0)
        inc8.add_inc('b', 12, 6)
        inc8.add_inc('b', 12, 5)
        apply(inc8)
        # a: (7, 1), (10, 2), (80, 40)
        # b: (7, 2), (10, 10), (12, 5), (13, 5)
        self.check_price(p.get_price(), 80, 43, 12, 10)

        inc9 = inc_t(seq=9, ts=9)
        inc9.add_inc('a', 11, 11)
        inc9.add_inc('b', 13, 12)
        inc9.add_inc('b', 12, 0)
        apply(inc9)
        # a: (7, 1), (10, 2), (11, 11), (80, 40)
        # b: (7, 2), (10, 10), (13, 12)
        self.check_price(p.get_price(), 11, 14, 13, 12)

        inc10 = inc_t(seq=10, ts=10)
        inc10.add_inc('a', 11, 7)
        inc10.add_inc('b', 14, 7)
        inc10.add_inc('b', 14, 12)
        apply(inc10)
        # a: (7, 1), (10, 2), (11, 7), (80, 40)
        # b: (7, 2), (10, 10), (13, 12), (14, 12)
        self.check_price(p.get_price(), 11, 10, 14, 12)

        inc11 = inc_t(seq=11, ts=11)
        inc11.add_inc('a', 8, 2)
        inc11.add_inc('a', 10, 0)
        inc11.add_inc('b', 13, 2)
        apply(inc11)
        # a: (7, 1), (8, 2), (11, 7), (80, 40)
        # b: (7, 2), (10, 10), (13, 2), (14, 12)
        self.check_price(p.get_price(), 11, 10, 14, 12)

        inc12 = inc_t(seq=12, ts=12)
        inc12.add_inc('a', 11, 6)
        inc12.add_inc('a', 80, 0)
        inc12.add_inc('b', 14, 7)
        apply(inc12)
        # a: (7, 1), (8, 2), (11, 6)
        # b: (7, 2), (10, 10), (13, 2), (14, 7)
        self.check_price(p.get_price(), 0, 0, 10, 19)

        inc13 = inc_t(seq=13, ts=13)
        inc13.add_inc('a', 11, 8)
        inc13.add_inc('b', 10, 0)
        apply(inc13)
        # a: (7, 1), (8, 2), (11, 8)
        # b: (7, 2), (13, 2), (14, 7)
        self.check_price(p.get_price(), 11, 11, 7, 11)

    def test_bug1(self):
        obh = concurrent_map_t()
        obh.reg('pair', orderbook_t())
        p = price_processor_t('pair', obh, amount_threshold=0)

        def apply(inc):
            with obh.get('pair') as ob:
                ob.apply(inc)
            p.apply(inc)

        incs_txt = """| 251153430 | 1529957089.420503 | [["b", "6235.40641500", "0.00000000"]]                                        |
| 251153431 | 1529957089.542245 | [["a", "6260.83384452", "0.60000000"]]  |
| 251153432 | 1529957089.542756 | [["b", "6226.08430969", "0.00000000"], ["b", "6218.98062075", "0.41856089"]]  |
| 251153433 | 1529957089.747416 | [["a", "6260.84922960", "7.99614900"]]                                        |
| 251153434 | 1529957089.940546 | [["b", "6231.40350000", "0.01500000"]]                                        |
| 251153435 | 1529957090.040913 | [["b", "6248.05760591", "0.00000000"]]                                        |
| 251153436 | 1529957090.634677 | [["a", "6260.84922960", "0.00000000"], ["a", "6260.83384442", "8.38806400"]]  |
| 251153437 | 1529957090.952949 | [["b", "6248.05760592", "10.00000000"]]                                       |
| 251153438 | 1529957091.558215 | [["b", "6248.05760593", "6.48000000"]]                                        |
| 251153439 | 1529957091.558479 | [["a", "6260.83384442", "0.00000000"]]                                        |
| 251153440 | 1529957092.351325 | [["b", "6226.48314612", "1.60700000"]]                                        |
| 251153441 | 1529957092.465331 | [["a", "6260.83384438", "7.99614900"]]                                        |
| 251153442 | 1529957092.465694 | [["a", "6252.93000479", "0.21522232"]]                                        |
| 251153443 | 1529957092.893135 | [["a", "6260.83384438", "0.00000000"]]                                        |
| 251153444 | 1529957093.056166 | [["b", "6248.05760593", "6.47976003"]]                                        |
| 251153445 | 1529957093.252566 | [["a", "6260.83384434", "7.99614900"]]                                        |
| 251153446 | 1529957093.678167 | [["b", "6248.05760592", "0.00000000"], ["b", "6248.05760603", "9.83228600"]]  |
| 251153447 | 1529957094.460429 | [["a", "6260.83384434", "0.00000000"]]                                        |
| 251153448 | 1529957094.876163 | [["b", "6248.05760603", "0.00000000"], ["b", "6248.05760622", "9.79058800"]]  |
| 251153449 | 1529957095.162064 | [["a", "6260.83384430", "7.99614900"]]                                        |
| 251153450 | 1529957095.856814 | [["a", "6260.83384430", "0.00000000"]]                                        |
| 251153451 | 1529957096.791123 | [["a", "6260.83384448", "7.99614900"]]                                        |
| 251153452 | 1529957096.791596 | [["b", "6248.05760622", "0.00000000"], ["b", "6248.05760614", "10.13768100"]] |
| 251154121 | 1529957221.266745 | [["a", "6256.01680969", "5.78663200"]]  |
| 251154122 | 1529957221.266981 | [["a", "6256.01680978", "4.00000000"]]  |
| 251154123 | 1529957221.267149 | [["b", "6234.21910000", "0.00000000"]]                                        |
| 251154124 | 1529957221.267302 | [["a", "6281.58832284", "1.88452661"]]                                        |
| 251154125 | 1529957221.267504 | [["a", "6244.03620110", "0.01881049"]]                                        |
| 251154126 | 1529957221.268248 | [["b", "6234.30000001", "6.66000000"]]                                        |
| 251154127 | 1529957221.268941 | [["a", "6256.00680000", "0.47050000"]]                                        |
| 251154128 | 1529957221.270089 | [["b", "6199.26488610", "0.00000000"], ["b", "6179.18179026", "40.00000000"]] |
| 251154129 | 1529957221.418965 | [["a", "6256.01680969", "0.00000000"], ["a", "6244.03620100", "5.66409900"]]  |
| 251154130 | 1529957221.419318 | []                                        |
| 251154131 | 1529957221.591556 | [["b", "6244.02000000", "0.00059960"]]                                        |
| 251154132 | 1529957222.100408 | [["b", "6227.20907398", "0.00000000"], ["b", "6211.50305345", "0.41906476"]]  |
| 251154133 |  1529957222.23827 | [["a", "6244.03620100", "0.00000000"], ["a", "6244.03620099", "5.68886900"]]  |
| 251154134 | 1529957222.302335 | [["b", "6238.57890101", "0.00000000"]]                                        |
| 251154135 | 1529957222.302687 | [["a", "6256.01680978", "0.00000000"], ["a", "6256.01680968", "4.00000000"]]  |
| 251154136 | 1529957222.641582 | [["b", "6234.30000001", "0.00000000"]]                                        |
| 251154137 |   1529957222.7099 | [["b", "6227.20907399", "0.00000000"], ["b", "6226.48314613", "3.08107494"]]  |
| 251154138 | 1529957222.895174 | [["a", "6256.01680968", "0.00000000"], ["a", "6256.00679999", "4.00000000"]]  |
| 251154139 | 1529957223.025153 | [["a", "6250.26402000", "0.01032330"]]                                        |
| 251154140 | 1529957223.206423 | [["a", "6244.03620099", "0.00000000"], ["a", "6244.03620083", "5.53406400"]]  |
| 251154141 | 1529957223.876912 | [["a", "6244.03620083", "0.00000000"]]                                        |
| 251154142 | 1529957224.589023 | [["b", "6234.30000004", "10.00000000"]]                                       |
| 251154143 | 1529957224.589406 | [["b", "6234.30000101", "0.38286998"]]                                        |
| 251154144 | 1529957224.712469 | [["b", "6234.30000004", "0.00000000"]]                                        |
| 251154145 | 1529957224.749543 | [["b", "6226.48314613", "0.00000000"], ["b", "6206.00000002", "3.04819332"]]  |
| 251154146 | 1529957224.749885 | [["a", "6256.00680000", "0.00000000"]]                                        |
| 251154147 | 1529957225.136029 | [["b", "6206.00000002", "0.00000000"], ["b", "6226.48314614", "3.17988240"]]  |
| 251154148 | 1529957225.136557 | [["a", "6255.99680000", "0.38250000"]]                                        |
| 251154149 | 1529957225.347068 | [["b", "6234.30000008", "10.00000000"]]                                       |
| 251154150 |  1529957225.54198 | [["b", "6226.48314614", "0.00000000"], ["b", "6226.48314613", "3.10796084"]]  |
| 251154151 | 1529957225.643354 | [["a", "6250.26402000", "0.00441553"]]                                        |
| 251154152 |  1529957225.84357 | [["a", "6256.00679999", "0.00000000"], ["a", "6255.99679999", "4.00000000"]]  |
| 251154153 | 1529957225.948509 | [["b", "6234.30000008", "0.00000000"]]                                        |
| 251154154 | 1529957226.088569 | [["a", "6244.03620110", "0.01856977"]]                                        |
| 251154155 | 1529957226.243784 | [["a", "6250.24863488", "0.60000000"]]  |
| 251154156 | 1529957226.244058 | [["a", "6244.03620109", "5.59766200"]]                                        |
| 251154157 | 1529957226.462676 | []                                        |
| 251154158 | 1529957226.463156 | [["b", "6226.48314613", "0.00000000"], ["b", "6206.00000002", "3.07944847"]]  |
| 251154159 | 1529957226.639997 | [["b", "6234.30000105", "10.00000000"]]                                       |
| 251154160 | 1529957226.865282 | [["b", "6206.00000002", "0.00000000"], ["b", "6226.48314614", "3.11635126"]]  |
| 251154161 | 1529957227.352005 | [["b", "6244.02000000", "0.00035936"]]                                        |
| 251154162 | 1529957227.352418 | [["a", "6244.03620109", "0.00000000"], ["a", "6244.03620097", "5.77111800"]]  |
| 251154163 | 1529957227.551912 | [["b", "6234.30000105", "10.02000000"]]                                       |
| 251154164 | 1529957227.764566 | [["a", "6274.46751992", "3.62330146"]]                                        |
| 251154165 | 1529957227.765055 | [["b", "6226.48314614", "0.00000000"], ["b", "6180.00000001", "3.10688667"]]  |
| 251154166 |  1529957227.76539 | [["b", "6234.30000105", "0.02000000"]]                                        |
| 251154167 | 1529957227.837909 | [["a", "6250.26402000", "0.00000000"], ["a", "6250.26401999", "0.00441553"]]  |
| 251154168 | 1529957228.268914 | [["a", "6244.03620097", "0.00000000"], ["a", "6244.03620098", "5.34837900"]]  |
| 251154169 | 1529957228.273489 | [["a", "6244.03620098", "5.17634700"]]                                        |
| 251154170 | 1529957228.569094 | [["b", "6180.00000001", "0.00000000"], ["b", "6226.48314615", "3.08126660"]]  |
| 251154171 | 1529957228.981486 | [["a", "6244.03620098", "0.00000000"], ["a", "6244.03620078", "5.42814200"]]  |
| 251154172 | 1529957229.259239 | [["b", "6234.30000105", "0.00000000"]]                                        |
| 251154173 | 1529957229.366978 | [["a", "6244.03620078", "0.00000000"], ["a", "6244.03620084", "5.78019800"]]  |
| 251154174 | 1529957229.659424 | [["a", "6244.03620084", "5.29781665"]]                                        |
| 251154175 | 1529957229.755361 | [["a", "6244.03620084", "0.00000000"], ["a", "6244.03620062", "5.70641900"]]  |
| 251154176 | 1529957229.966486 | []                                        |
| 251154177 | 1529957230.056753 | []                                        |
| 251154178 | 1529957230.193981 | [["b", "6234.30000105", "10.00000000"]]                                       |
| 251154179 | 1529957230.567842 | [["a", "6244.03620062", "0.00000000"], ["a", "6244.03620071", "5.78305900"]]  |
| 251154180 | 1529957230.572766 | [["b", "6234.30000105", "10.02000000"]]                                       |
| 251154181 | 1529957230.573053 | [["a", "6250.26401999", "0.00000000"]]                                        |
| 251154182 | 1529957230.707724 | [["b", "6226.48314615", "0.00000000"], ["b", "6206.00000002", "3.07404663"]]  |
| 251154183 | 1529957230.866922 | [["a", "6250.26402000", "0.00509075"]]                                        |
| 251154184 | 1529957230.867358 | [["b", "6234.30000105", "0.02000000"]]                                        |
| 251154185 | 1529957231.294485 | [["b", "6234.30000105", "0.00000000"]]                                        |
| 251154186 | 1529957231.294821 | [["a", "6250.26402000", "0.00000000"]]                                        |
| 251154187 | 1529957231.356202 | [["b", "6206.00000002", "0.00000000"], ["b", "6226.48314613", "3.05909635"]]  |
| 251154188 | 1529957231.682201 | [["a", "6244.03620071", "0.00000000"], ["a", "6244.03620051", "6.11943300"]]  |
| 251154189 | 1529957231.770892 | [["a", "6244.03620109", "0.06688034"]]                                        |
| 251154190 |  1529957231.77134 | [["b", "6234.30000102", "5.36000000"]]                                        |
| 251154191 | 1529957232.185002 | [["a", "6244.03620051", "0.00000000"], ["a", "6244.03620056", "6.06291400"]]  |
| 251154192 | 1529957232.882048 | []                                        |
| 251154193 | 1529957232.882469 | [["a", "6244.03620056", "0.00000000"]]                                        |
| 251154194 | 1529957232.886004 | [["a", "6244.03620109", "0.06655938"]]                                        |
| 251154195 |  1529957233.01859 | [["b", "6234.30000102", "5.38000000"]]                                        |
| 251154196 | 1529957233.175621 | [["b", "6244.02000000", "0.00011912"]]                                        |
| 251154197 | 1529957233.176027 | [["a", "6244.03620109", "0.00000000"], ["a", "6244.03620110", "0.00000000"]]  |"""
        seq = 1
        for line in incs_txt.split('\n'):
            d = json.loads(line.split('|')[3].strip())
            inc = inc_t(seq=seq, ts=0)
            seq += 1
            for el in d:
                if el[0] != "a":
                    continue
                inc.add_inc(el[0], strnum2int(el[1], precision=8), strnum2int(el[2], precision=8))
            apply(inc)


class TestArbitrage(unittest.TestCase):
    def test_arbitrage_real(self):
        with open('arbitrage.yaml') as fin:
            config = yaml.load(fin.read())

        fee = config['arbitrage']['fee']

        def fee_func(amount):
            return amount * (1 - fee)

        with open('price1.json') as fin:
            price = json.load(fin)
        # заполняем storage для поиска арбитражей
        storage = {}
        for pair, data in price['result'].items():
            if data['status'] != 'ok':
                continue
            price_dict = dict( ts=data['ts'],
                               seq=data['seq'],
                               ask_rate=float(data['lowestAsk']['rate']),
                               ask_amount=float(data['lowestAsk']['amount']),
                               bid_rate=float(data['highestBid']['rate']),
                               bid_amount=float(data['highestBid']['amount']),
                             )
            storage[pair] = price_dict

        e_rate = {}
        e_rate['BTC->ETH->ZEC->BTC'] = 1.00129259
        e_rate['BTC->ETC->ETH->ZEC->BTC'] = 1.00008176
        e_amount = {}
        e_amount['BTC->ETH->ZEC->BTC'] = 1.2991673795955232e-06
        e_amount['BTC->ETC->ETH->ZEC->BTC'] = 1.2991673795955232e-06

        for chain in config['arbitrage']['chains']:
            rate, amount, ts, success = arbitrage_processor_t.calc_profit(chain.split('->'), storage, fee_func)
            self.assertTrue(success)
            if rate > 1.0:
                self.assertTrue(chain in e_rate and chain in e_amount)
                self.assertAlmostEqual(rate, e_rate[chain], places=7)
                self.assertAlmostEqual(amount, e_amount[chain], places=7)

    def test_arbitrage_simple(self):
        # Тест простого случая
        storage=dict(A_B=dict(ts=1.0, ask_rate=1.0, ask_amount=1.0, bid_rate=0.0, bid_amount=0.0),
                     B_C=dict(ts=1.1, ask_rate=1.0, ask_amount=1.0, bid_rate=0.0, bid_amount=0.0),
                     C_A=dict(ts=1.0, ask_rate=0.8, ask_amount=0.5, bid_rate=0.0, bid_amount=0.0)
                    )
        def fee_func(amount):
            return amount
        rate, amount, ts, success = arbitrage_processor_t.calc_profit('A->B->C->A'.split('->'), storage, fee_func)
        self.assertTrue(success)
        self.assertEqual(ts, 1.1)
        self.assertEqual(rate, 1.25)
        self.assertEqual(amount, 0.5)

        # Простой случай, но с комиссией
        fee = 0.002
        def fee_func(amount):
            return amount * (1.0 - fee)
        rate, amount, ts, success = arbitrage_processor_t.calc_profit('A->B->C->A'.split('->'), storage, fee_func)
        self.assertTrue(success)
        self.assertEqual(ts, 1.1)
        self.assertAlmostEqual(rate, 1.25 * fee_func(1.0)**3, places=7)
        self.assertAlmostEqual(amount, 0.5* fee_func(1.0)**1, places=7)

        # цепочка с обратной парой, у которых нулевые рейты
        rate, amount, ts, success = arbitrage_processor_t.calc_profit('A->B->C->B->A'.split('->'), storage, fee_func)
        self.assertFalse(success)
        self.assertEqual(ts, 0)
        self.assertEqual(rate, 0)
        self.assertEqual(amount, 0)

        # теперь рейты есть, вычисляем профит
        storage['A_B']['bid_rate'] = 0.9
        storage['A_B']['bid_amount'] = 0.1
        storage['B_C']['bid_rate'] = 1.2
        storage['B_C']['bid_amount'] = 1.0
        rate, amount, ts, success = arbitrage_processor_t.calc_profit('A->B->C->B->A'.split('->'), storage, fee_func)
        expected_rate = 1.0  /1.0*0.998  /1.0*0.998  *1.2*0.998  *0.9*0.998
        expected_amount = 0.1 * 0.9 * 0.998
        self.assertAlmostEqual(rate, expected_rate, places=7)
        self.assertAlmostEqual(amount, expected_amount, places=7)


if __name__ == '__main__':
    unittest.main()
