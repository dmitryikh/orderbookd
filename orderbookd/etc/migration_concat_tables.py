import argparse
import logging
import yaml

from orderbookd.db import get_mysql_connection

logging.basicConfig( format="%(asctime)s - %(name)s - %(levelname)s - %(message)s",
                     level=logging.DEBUG
                   )
logger = logging.getLogger('main')


def parse_args():
    parser = argparse.ArgumentParser(description='concantenate dump_{pair} tables',
                                     formatter_class=argparse.ArgumentDefaultsHelpFormatter)
    parser.add_argument('-c', '--config', type=str, required=True,
                        help='config file')
    return parser.parse_args()


def concat_tables(conn, pairs):
    cur = conn.cursor()
    for pair in pairs:
        query = 'INSERT INTO dump_{}_c (minute, ts, seq, dump) SELECT minute, ts, seq, dump FROM dump_{}'.format(pair, pair)
        cur.execute(query)

        query = 'RENAME TABLE dump_{} to dump_{}_backup'.format(pair, pair)
        cur.execute(query)

        query = 'RENAME TABLE dump_{}_c to dump_{}'.format(pair, pair)
        cur.execute(query)
        # Дропать будем в ручную
        # DROP TABLE dump_XMR_ZEC_backup;


def main():
    settings = parse_args()
    with open(settings.config, 'r') as fin:
        config = yaml.load(fin)
    pairs = config['pairs']
    # _o - original
    # _c - compressed
    conn = get_mysql_connection(config['db'])
    conn.autocommit = True

    concat_tables(conn, pairs)

if __name__ == '__main__':
    main()
