import argparse
import websocket
import time
import json
import logging
import random
import queue
import yaml

import orderbookd.gateio as gio

logging.basicConfig( format="%(asctime)s - %(name)s - %(levelname)s - %(message)s",
                     level=logging.DEBUG
                   )
logger = logging.getLogger('gate.io')


def parse_args():
    parser = argparse.ArgumentParser(description='collect pushes for order books',
                                     formatter_class=argparse.ArgumentDefaultsHelpFormatter)
    parser.add_argument('-c', '--config', type=str, required=True,
                        help='config file')
    return parser.parse_args()


def ws_on_open(ws):
    logger.info('ws_on_open')
    supported_pairs = gio.subset_of_supported_pairs(pairs)
    logger.info('Supported {} of {} pairs'.format(len(supported_pairs), len(pairs)))
    logger.info('{}'.format('\n'.join(supported_pairs)))
    params = [[pair, 30, '0.00000001'] for pair in supported_pairs]
    d = dict( id=123,
              method='depth.subscribe',
              # params=[['ETH_BTC', 30, '0.00000001'], ['GSE_ETH', 30, '0.00000001']],
              params=params,
            )
    # d = dict( id=123,
    #           method='depth.query',
    #           params=['GSE_ETH', 100, '0.00000001'],
    #         )
    print(json.dumps(d))
    ws.send(json.dumps(d))

def ws_on_error(ws, error):
    logger.error('ERROR: {}'.format(error))
    ws.close()

def ws_on_close(ws):
    logger.info('ws_on_close')

def ws_on_message(ws, message):
    try:
        ts = time.time()
        # try:
        #     dt = ts - json.loads(message)[0][1]['ts']
        # except:
        #     dt = 0.0
        logger.info("{}".format(message))
    except Exception:
        logger.exception('Exception while processing message:')
        ws.close()


settings = parse_args()
with open(settings.config, 'r') as fin:
    config = yaml.load(fin)
pairs = config['pairs']

ws = websocket.WebSocketApp( "wss://ws.gate.io/v3/",
                             on_message = ws_on_message,
                             on_error = ws_on_error,
                             on_close = ws_on_close
                           )
ws.on_open = ws_on_open
ws.run_forever()
