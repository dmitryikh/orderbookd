import argparse
import logging
import json
import os
import yaml

import orderbookd.db as db
from orderbookd.order_book import orderbooks_holder_t, inc_t, price_t
from orderbookd.util import strnum2int, ts2period, int2float

logging.basicConfig( format="%(asctime)s - %(name)s - %(levelname)s - %(message)s",
                     level=logging.DEBUG
                   )
logger = logging.getLogger('main')


def parse_args():
    parser = argparse.ArgumentParser(description='dump best prices for particular pair over all time',
                                     formatter_class=argparse.ArgumentDefaultsHelpFormatter)
    parser.add_argument('-c', '--config', type=str, required=True,
                        help='config file')
    parser.add_argument('-p', '--pair', type=str, required=True,
                        help='pair to proceed')
    parser.add_argument('-o', '--output', type=str, required=True,
                        help='output file')
    return parser.parse_args()

def open_output(filename):
    return open(filename, 'w')

def write_gap_output(f):
    f.write('0,0,0.0,0.0,0.0,0.0\n')

def write_output(f, price, precision):
    ask_rate, ask_amount = map(lambda x: int2float(x, precision_decode=precision), price.get_price('asks'))
    bid_rate, bid_amount = map(lambda x: int2float(x, precision_decode=precision), price.get_price('bids'))
    f.write('{},{},{},{},{},{}\n'.format(price.ts, price.seq, ask_rate, ask_amount, bid_rate, bid_amount))

def close_output(f):
    f.close()


# Формат output файла:
# ts, seq, ask_rate, ask_amount, bid_rate, bid_amount

# если был пробел в данных, то 1 строка заполняется нулями

def fetch_best_prices(f, pair, config):
    logger.info('Processing {}'.format(pair))
    precision = config['precision']
    conn_i = db.get_connection(config['db'])  # для апдейтов
    conn_i.autocommit = True
    cur_i = conn_i.cursor()
    conn_o = db.get_connection(config['db'])  # для дампов
    conn_o.autocommit = True
    cur_o = conn_o.cursor()

    obh = orderbooks_holder_t()
    obh.reg(pair)
    price = price_t(pair, obh, amount_threshold=0)

    # Собираем первый полный дамп
    query = 'SELECT minute FROM dump_{} ORDER BY minute LIMIT 1'.format(pair)
    cur_o.execute(query)
    first_minute = cur_o.fetchone()[0]
    ob = db.select_orderbook(conn_o, pair, first_minute, config['period'], precision)
    obh.orderbooks[pair] = (ob, obh.orderbooks[pair][1])

    query = 'SELECT seq, ts, inc FROM inc_{} ORDER BY seq'.format(pair)
    cur_i.execute(query)
    i = 0
    gaps = 0
    gaps_sec = 0
    gaps_seq = 0
    for seq, ts, data in db.iter_rows_batch(cur_i, 100):
        i += 1
        increment = inc_t(seq, ts)
        increment.incs = [[ 'a' if inc[0] == 0 else 'b',
                            strnum2int(inc[1], precision=precision),
                            strnum2int(inc[2], precision=precision),
                          ] for inc in json.loads(data)]

        if seq <= obh.orderbooks[pair][0].seq:
            # старый апдейт, пропускаем
            continue
        elif seq == obh.orderbooks[pair][0].seq + 1:
            # все ок, применяем апдейт и обрабатываем
            obh.orderbooks[pair][0].apply(increment)
            is_changed = price.apply(increment)
            if is_changed:
                # пишем лучшую цену на данный момент
                write_output(f, price, precision)
        else:
            # упс.. пропуск в данных
            # 1. подгружаем следующий полный дамп
            # 2. Пишем "пустую" строчку в данные
            # 3. Если апдейт годится, то применяем к orderbook
            # 3. Заново считаем prices (full_scan)
            dt = increment.ts - obh.orderbooks[pair][0].ts
            dseq = increment.seq - obh.orderbooks[pair][0].seq
            gaps += 1
            gaps_seq += dseq
            gaps_sec += dt
            logger.warning('Got gap: ts: {} <-> {} ({:.2f}) , seq {} <-> {} ({})'.format(obh.orderbooks[pair][0].ts,
                                                                                         increment.ts,
                                                                                         dt,
                                                                                         obh.orderbooks[pair][0].seq,
                                                                                         increment.seq,
                                                                                         dseq
                                                                                        ))
            logger.warning('Total {} gaps: {:.2f} seconds, {} seqs'.format(gaps, gaps_sec, gaps_seq))
            # пишем пустую
            write_gap_output(f)
            query = 'SELECT minute FROM dump_{} WHERE minute > %s ORDER BY minute LIMIT 1'.format(pair)
            cur_o.execute(query, [ts2period(obh.orderbooks[pair][0].ts, config['period'], next=True)])
            next_minute = cur_o.fetchone()[0]
            ob = db.select_orderbook(conn_o, pair, next_minute, config['period'], precision)
            obh.orderbooks[pair] = (ob, obh.orderbooks[pair][1])
            if seq == obh.orderbooks[pair][0].seq + 1:
                # все ок, применяем апдейт и обрабатываем
                obh.orderbooks[pair][0].apply(increment)
            price.full_scan(action='asks')
            price.full_scan(action='bids')
            price.ts = obh.orderbooks[pair][0].ts
            price.seq = obh.orderbooks[pair][0].seq
            #пишем цену на данный момент
            write_output(f, price, precision)
        if i % 100000 == 0:
            logger.info('Processed {} increments, {} gaps'.format(i, gaps))

    logger.info('Finish')


def main():
    settings = parse_args()
    with open(settings.config, 'r') as fin:
        config = yaml.load(fin)
    for pair in config['pairs']:
        f = open_output(os.path.join(settings.output, '{}_history.csv'.format(pair)))
        try:
            fetch_best_prices(f, pair, config)
        finally:
            close_output(f)

if __name__ == '__main__':
    main()
