import argparse
import json
import logging
import yaml

from orderbookd.db import get_mysql_connection, iter_rows_batch
from orderbookd.order_book import order_book_t

logging.basicConfig( format="%(asctime)s - %(name)s - %(levelname)s - %(message)s",
                     level=logging.DEBUG
                   )
logger = logging.getLogger('main')


def parse_args():
    parser = argparse.ArgumentParser(description='migrate dump_{pair} tables to compressed format',
                                     formatter_class=argparse.ArgumentDefaultsHelpFormatter)
    parser.add_argument('-c', '--config', type=str, required=True,
                        help='config file')
    return parser.parse_args()


def rename_tables(conn, pairs):
    cur = conn.cursor()
    for pair in pairs:
        query = 'RENAME TABLE dump_{} TO dump_{}_o'.format(pair, pair)
        cur.execute(query)

        query = """
CREATE TABLE dump_{}_c (
    minute INT NOT NULL,
    ts DOUBLE NOT NULL,
    seq INT NOT NULL,
    dump MEDIUMBLOB NOT NULL,
    PRIMARY KEY(minute)
)
ROW_FORMAT=COMPRESSED""".format(pair)
        cur.execute(query)

        query = """
CREATE TABLE dump_{} (
    minute INT NOT NULL,
    ts DOUBLE NOT NULL,
    seq INT NOT NULL,
    dump MEDIUMBLOB NOT NULL,
    PRIMARY KEY(minute)
)
ROW_FORMAT=COMPRESSED""".format(pair)
        cur.execute(query)

def compress_table(conn_o, conn_c, pair):
    logger.info('Convert from dump_{}_o -> dump_{}_c'.format(pair, pair))
    cur_o = conn_o.cursor()
    cur_c = conn_c.cursor()
    query = 'SELECT minute, ts, seq, dump FROM dump_{}_o'.format(pair)
    cur_o.execute(query)
    i = 0
    for minute, ts, seq, data in iter_rows_batch(cur_o, 10):
        query = 'INSERT INTO dump_{}_c ({}) VALUES ({})'.format(pair, order_book_t.values_columns(), order_book_t.values_mask())
        cur_c.execute(query, (minute, ts, seq, order_book_t._compress(json.loads(data))))
        i += 1
        if i % 100 == 0:
            logger.info('Done {} records'.format(i))
    query = 'DROP TABLE dump_{}_o'.format(pair)
    cur_o.execute(query)
    logger.info('Finish dump_{}_o'.format(pair))


def main():
    settings = parse_args()
    with open(settings.config, 'r') as fin:
        config = yaml.load(fin)
    pairs = config['pairs']
    # _o - original
    # _c - compressed
    conn_o = get_mysql_connection(config['db'])
    conn_o.autocommit = True
    conn_c = get_mysql_connection(config['db'])
    conn_c.autocommit = True

    rename_tables(conn_o, pairs)
    for pair in pairs:
        compress_table(conn_o, conn_c, pair)

if __name__ == '__main__':
    main()
