import argparse
import websocket
import time
import json
import logging
import random
import queue
import yaml

import orderbookd.binance as bnc
from orderbookd.order_book import orderbook_t

logging.basicConfig( format="%(asctime)s - %(name)s - %(levelname)s - %(message)s",
                     level=logging.DEBUG
                   )
logger = logging.getLogger('binance')

ob_global = None

def parse_args():
    parser = argparse.ArgumentParser(description='collect pushes for order books',
                                     formatter_class=argparse.ArgumentDefaultsHelpFormatter)
    parser.add_argument('-c', '--config', type=str, required=True,
                        help='config file')
    return parser.parse_args()


def ws_on_open(ws):
    global ob_global
    logger.info('ws_on_open')
    # params = [[pair, 30, '0.00000001'] for pair in supported_pairs]
    # d = dict( id=123,
    #           method='depth.subscribe',
    #           # params=[['ETH_BTC', 30, '0.00000001'], ['GSE_ETH', 30, '0.00000001']],
    #           params=params,
    #         )
    # print(json.dumps(d))
    # ws.send(json.dumps(d))
    ob_global = None

def ws_on_error(ws, error):
    logger.error('ERROR: {}'.format(error))
    ws.close()

def ws_on_close(ws):
    logger.info('ws_on_close')

def ws_on_message(ws, message):
    global ob_global
    try:
        ts = time.time()
        d = json.loads(message)
        try:
            ob = orderbook_t()
            bnc.parse_orderbook(d['data'], ob, ts, precision=8)
            if ob_global is None:
                ob_global = ob
            else:
                inc = ob_global.diff(ob)
                print(d['stream'], str(inc))
        except:
            logger.exception('')
            pass
        # logger.info("{}".format(message))
    except Exception:
        logger.exception('Exception while processing message:')
        ws.close()


settings = parse_args()
with open(settings.config, 'r') as fin:
    config = yaml.load(fin)
pairs = config['binance']['pairs']
pairs = pairs[:1]
pairs = ['XRP_USDT']
level = 20
streams = '/'.join(map(lambda x: '{}@depth{}'.format(x.replace('_', '').lower(), level), pairs))
print(f"wss://stream.binance.com:9443/stream?streams={streams}")
ws = websocket.WebSocketApp( f"wss://stream.binance.com:9443/stream?streams={streams}",
                             on_message = ws_on_message,
                             on_error = ws_on_error,
                             on_close = ws_on_close
                           )
ws.on_open = ws_on_open
ws.run_forever()
