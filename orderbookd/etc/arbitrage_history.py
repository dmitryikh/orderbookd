import argparse
import logging
import os
import yaml
from collections import defaultdict

from orderbookd.arbitrage import arbitrage_t
from orderbookd.util import ts_to_datetime_str

logging.basicConfig( format="%(asctime)s - %(name)s - %(levelname)s - %(message)s",
                     level=logging.DEBUG
                   )
logger = logging.getLogger('main')
arbitrages_written = 0


def parse_args():
    parser = argparse.ArgumentParser(description='find arbitrages on history data',
                                     formatter_class=argparse.ArgumentDefaultsHelpFormatter)
    parser.add_argument('-c', '--config', type=str, required=True,
                        help='config file')
    parser.add_argument('-d', '--dir', type=str, required=True,
                        help='dir with histories')
    parser.add_argument('-o', '--output', type=str, required=True,
                        help='output file')
    return parser.parse_args()


class pair_history_t(object):
    """
    Класс, читающий построчно историю из файла
    """
    fields = ['ts', 'seq', 'ask_rate', 'ask_amount', 'bid_rate', 'bid_amount']
    types = [float, int, float, float, float, float]

    def __init__(self, filename):
        # Формат файла:
        # ts, seq, ask_rate, ask_amount, bid_rate, bid_amount
        self.f = open(filename, 'r')
        self.f.readline()
        self.fetch_cur()
        self.fetch_next()

    @staticmethod
    def parse_line(line):
        tokens = line.strip().split(',')
        assert len(tokens) == len(pair_history_t.fields)
        return {field: type(token) for
                field, type, token in zip(pair_history_t.fields, pair_history_t.types, tokens)}

    def peek_ts(self):
        return self.cur['ts'] if (self.cur is not None and self.nex is not None) else None

    def next(self):
        if self.nex['seq'] == 0:
            # пропавшие данные дальше
            ret = self.nex
            self.fetch_cur()
            is_gap = True
        else:
            ret = self.cur
            self.cur = self.nex
            is_gap = False
        self.fetch_next()
        return ret, is_gap

    def fetch_cur(self):
        line = self.f.readline()
        if line:
            # последняя строка
            self.cur = pair_history_t.parse_line(line)
        else:
            self.cur = None

    def fetch_next(self):
        line = self.f.readline()
        if line:
            # последняя строка
            self.nex = pair_history_t.parse_line(line)
        else:
            self.nex = None

class storage_holder_t(object):
    def __init__(self, pairs, dir):
        self.pair_histories = {}
        self.storage = {}
        self.ts = 0
        for pair in pairs:
            self.pair_histories[pair] = pair_history_t(os.path.join(dir, '{}_history.csv'.format(pair)))
            price, is_gap = self.pair_histories[pair].next()
            assert not is_gap
            self.storage[pair] = price
            self.ts = max(self.ts, price['ts'])
        # догоняем остальные пары по max:
        for pair in pairs:
            while self.pair_histories[pair].peek_ts() < self.ts:
                self.pair_histories[pair].next()

        self.init = True

    def tick(self):
        next_pair = None
        next_ts = None
        for pair, ph in self.pair_histories.items():
            ts = ph.peek_ts()
            if ts is None:
                # одна из пар достигла конца файла
                logger.info('Pair {} is ended on {} ({})'.format(pair, ts, ts_to_datetime_str(self.ts)))
                return None, None
            assert ts >= self.ts, '{} >= {}'.format(ts, self.ts)
            if next_ts is None or next_ts > ts:
                next_pair = pair
                next_ts = ts
        assert next_ts is not None and next_pair is not None
        price, is_gap = self.pair_histories[next_pair].next()
        self.ts = price['ts']
        is_better = (price['ask_rate'] < self.storage[next_pair]['ask_rate'])\
                 or (price['bid_rate'] > self.storage[next_pair]['bid_rate'])
        self.storage[next_pair] = price
        return next_pair, is_gap, is_better


class arbitrage_processor_t(object):
    def __init__(self, config, storage, f):
        self.config = config
        self.storage = storage
        self.f = f
        self.fee = config['arbitrage']['fee']
        self.pairs = set(config['pairs'])
        self.pair2chains = defaultdict(list)
        self.chain2pairs = defaultdict(list)
        self.chains = set((tuple(ch.split('->')) for ch in config['arbitrage']['chains'] if len(ch.split('->')) < 8))
        self.arbitrages = {}
        for chain in self.chains:
            if len(chain) < 3:
                raise ValueError("Wrong arbitrage chain '{}'".format('->'.join(chain)))
            pairs_tmp = []
            for i in range(1, len(chain)):
                pair1, pair2 = '{}_{}'.format(chain[i - 1], chain[i]), '{}_{}'.format(chain[i], chain[i - 1])
                found = 0
                if pair1 in self.pairs:
                    pairs_tmp.append(pair1)
                    found += 1

                if pair2 in self.pairs:
                    pairs_tmp.append(pair2)
                    found += 1

                if found > 1:
                    raise ValueError("There are equivalent pairs: '{}' and '{}'".format(pair1, pair2))
                if found == 0:
                    # нет пары для цепочки
                    pairs_tmp = []
                    logger.warning("Can't process chain '{}' because there are no information about pair '{}' or '{}'".format('->'.join(chain), pair1, pair2))
                    break
            for pair in pairs_tmp:
                self.pair2chains[pair].append(chain)
                self.chain2pairs[chain].append(pair)

            self.arbitrages[chain] = arbitrage_t(chain)
        logger.info('Process chains ({}): {}'.format(len(self.chain2pairs), ', '.join(map(lambda ch: "'" + '->'.join(ch) + "'", self.chain2pairs.keys()))))

    def check_arbitrages(self, pair, is_better, is_gap):
        chains = self.pair2chains[pair]
        needed_pairs = set()
        for chain in chains:
            needed_pairs.update(self.chain2pairs[chain])

        for chain in chains:
            if chain not in self.chain2pairs:
                continue
            if (not is_better or is_gap) and not self.arbitrages[chain].is_open:
                continue
            rate, amount, ts, success = arbitrage_processor_t.calc_profit(chain, self.storage, fee_func=lambda a: self.sub_fee(a))
            arbitrage = self.arbitrages[chain]
            if not success:
                if self.arbitrages[chain].is_open:
                    # просто закрываем арбитраж, для которого не можем посчитать профит
                    arbitrage.is_open = False
                # logger.warning("Can't calc profit for chain '{}'".format('->'.join(chain)))
                continue
            # logger.debug("calc_profit '{}' (rate = {}, amount = {})".format('->'.join(chain), rate, amount))
            if arbitrage.is_open and rate <= 1.0:
                # закрываем арбитраж
                arbitrage.is_open = False
                arbitrage.ts_end = ts
                logger.debug("Close arbitrage '{}' (rate = {}, amount = {})".format('->'.join(chain), arbitrage.rate_start, arbitrage.amount_start))
                write_arbitrage(self.f, arbitrage)
                # db.insert_arbitrage(conn, arbitrage)
            elif not arbitrage.is_open and rate > 1.0:
                # открываем арбитраж
                arbitrage.is_open = True
                arbitrage.ts_start = ts
                arbitrage.rate_start = rate
                arbitrage.amount_start = amount

    def sub_fee(self, amount, count=1):
        return amount * (1 - self.fee) ** count

    @staticmethod
    def calc_profit(circle, storage, fee_func):
        rate = 1
        # не спортивно.. но пока так. Речь о том, что мы якобы имеем неограниченное кол-во валюты
        # для обмена
        amount = 10**20
        ts = 0
        for i in range(len(circle) - 1):
            cur_val = circle[i]
            next_val = circle[i + 1]

            pair = cur_val + '_' + next_val
            reverse_pair = next_val + '_' + cur_val

            if pair in storage:
                if storage[pair]['ask_amount'] == 0:
                    # нет информации о лучшей цене пары
                    return 0, 0, 0, False
                rate = fee_func(rate / storage[pair]['ask_rate'])
                amount = fee_func(min(amount / storage[pair]['ask_rate'], storage[pair]['ask_amount']))
                ts = max(ts, storage[pair]['ts'])
            elif reverse_pair in storage:
                if storage[reverse_pair]['bid_amount'] == 0:
                    # нет информации о лучшей цене пары
                    return 0, 0, 0, False
                rate = fee_func(rate * storage[reverse_pair]['bid_rate'])
                amount = fee_func(min(amount * storage[reverse_pair]['bid_rate'], storage[reverse_pair]['bid_rate'] * storage[reverse_pair]['bid_amount']))
                ts = max(ts, storage[reverse_pair]['ts'])
            else:
                return 0, 0, 0, False
        return rate, amount, ts, True


def open_output(filename):
    return open(filename, 'w')

# Формат output файла:
# chain, ts_start, ts_end, rate_start, amount_start
def write_arbitrage(f, arbitrage):
    global arbitrages_written
    f.write('{},{},{},{},{}\n'.format('->'.join(arbitrage.chain),
                                      arbitrage.ts_start,
                                      arbitrage.ts_end,
                                      arbitrage.rate_start,
                                      arbitrage.amount_start
                                     ))
    arbitrages_written += 1
    f.flush()

def close_output(f):
    f.close()


def main():
    settings = parse_args()
    with open(settings.config, 'r') as fin:
        config = yaml.load(fin)

    f = open_output(settings.output)
    sh = storage_holder_t(config['pairs'], settings.dir)
    ap = arbitrage_processor_t(config, sh.storage, f)
    i = 0
    while True:
        i += 1
        pair, is_gap, is_better = sh.tick()
        if pair is None:
            return
        ap.check_arbitrages(pair, is_better, is_gap)
        if i % 10000 == 0:
            logger.info('Proceed {} seqs, {} arbitrages found. Datetime: {}'.format(i, arbitrages_written, ts_to_datetime_str(sh.ts)))
    logger.info('Finish')
    close_output(f)

if __name__ == '__main__':
    main()
