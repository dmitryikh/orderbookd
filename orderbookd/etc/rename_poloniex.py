import argparse
import json
import logging
import yaml

from orderbookd.db import get_connection

logging.basicConfig( format="%(asctime)s - %(name)s - %(levelname)s - %(message)s",
                     level=logging.DEBUG
                   )
logger = logging.getLogger('main')


def parse_args():
    parser = argparse.ArgumentParser(description='migrate dump_{pair} tables to compressed format',
                                     formatter_class=argparse.ArgumentDefaultsHelpFormatter)
    parser.add_argument('-c', '--config', type=str, required=True,
                        help='config file')
    return parser.parse_args()


def rename_tables(conn):
    cur = conn.cursor()
    cur.execute("SELECT table_name FROM information_schema.tables "
                "WHERE table_schema='exchange' AND table_name LIKE 'dump_%'")
    tables = [row[0] for row in cur.fetchall()]
    print(tables)
    for table in tables:
        cur.execute(f'RENAME TABLE {table} TO poloniex_{table}')

    cur.execute("SELECT table_name FROM information_schema.tables "
                "WHERE table_schema='exchange' AND table_name LIKE 'inc_%'")
    tables = [row[0] for row in cur.fetchall()]
    print(tables)
    for table in tables:
        cur.execute(f'RENAME TABLE {table} TO poloniex_{table}')


def drop_backup_tables(conn):
    """
    После перехода на сжатый формат хранения ордербуков, остались таблицы вида dump_*_backup.
    Удалим их тут
    """
    cur = conn.cursor()
    cur.execute("SELECT table_name FROM information_schema.tables "
                "WHERE table_schema='exchange' AND table_name LIKE '%_backup'")
    tables = [row[0] for row in cur.fetchall()]
    print(tables)
    for table in tables:
        cur.execute(f'DROP TABLE {table}')

def main():
    settings = parse_args()
    with open(settings.config, 'r') as fin:
        config = yaml.load(fin)
    conn = get_connection(config['db'])
    conn.autocommit = True

    drop_backup_tables(conn)
    rename_tables(conn)

if __name__ == '__main__':
    main()
